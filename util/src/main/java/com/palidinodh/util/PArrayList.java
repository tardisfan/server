package com.palidinodh.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Spliterator;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class PArrayList<T> extends ArrayList<T> {
  public static final long serialVersionUID = 20200127;

  private Lock modLock = new UnsafeLock();
  private List<Consumer<PArrayList<T>>> pending = new ArrayList<>();

  public PArrayList(int initialCapacity) {
    super(initialCapacity);
  }

  public PArrayList(Collection<? extends T> collection) {
    super(collection);
  }

  public PArrayList() {
    super();
  }

  @Override
  public Object clone() {
    checkPending();
    return super.clone();
  }

  @Override
  public boolean contains(Object object) {
    checkPending();
    return super.contains(object);
  }

  @Override
  public boolean containsAll(Collection<?> collection) {
    checkPending();
    return super.containsAll(collection);
  }

  @Override
  public T get(int index) {
    checkPending();
    return super.get(index);
  }

  @Override
  public int indexOf(Object object) {
    checkPending();
    return super.indexOf(object);
  }

  @Override
  public int lastIndexOf(Object object) {
    checkPending();
    return super.lastIndexOf(object);
  }

  @Override
  public int size() {
    checkPending();
    return super.size();
  }

  @Override
  public boolean isEmpty() {
    checkPending();
    return super.isEmpty();
  }

  @Override
  public Object[] toArray() {
    checkPending();
    return super.toArray();
  }

  @Override
  public <E> E[] toArray(E[] a) {
    checkPending();
    return super.toArray(a);
  }

  @Override
  public List<T> subList(int fromIndex, int toIndex) {
    checkPending();
    return super.subList(fromIndex, toIndex);
  }

  @Override
  public Iterator<T> iterator() {
    checkPending();
    return super.iterator();
  }

  @Override
  public ListIterator<T> listIterator() {
    checkPending();
    return super.listIterator();
  }

  @Override
  public ListIterator<T> listIterator(int index) {
    checkPending();
    return super.listIterator(index);
  }

  @Override
  public Spliterator<T> spliterator() {
    checkPending();
    return super.spliterator();
  }

  @Override
  public boolean add(T t) {
    doAction(l -> super.add(t));
    return true;
  }

  @Override
  public void add(int index, T t) {
    doAction(l -> super.add(index, t));
  }

  @Override
  public boolean addAll(Collection<? extends T> collection) {
    doAction(l -> super.addAll(new ArrayList<T>(collection)));
    return true;
  }

  @Override
  public boolean addAll(int index, Collection<? extends T> collection) {
    doAction(l -> super.addAll(index, new ArrayList<T>(collection)));
    return true;
  }

  @Override
  public T set(int index, T t) {
    T existing = get(index);
    doAction(l -> super.set(index, t));
    return existing;
  }

  @Override
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public boolean retainAll(Collection<?> collection) {
    doAction(l -> super.retainAll(new ArrayList(collection)));
    return true;
  }

  @Override
  public boolean remove(Object t) {
    doAction(l -> super.remove(t));
    return true;
  }

  @Override
  public T remove(int index) {
    T existing = get(index);
    doAction(l -> super.remove(index));
    return existing;
  }

  @Override
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public boolean removeAll(Collection<?> collection) {
    doAction(l -> super.removeAll(new ArrayList(collection)));
    return true;
  }

  @Override
  public void clear() {
    doAction(l -> {
      pending.clear();
      super.clear();
    });
  }

  @Override
  public void ensureCapacity(int minCapacity) {
    doAction(l -> super.ensureCapacity(minCapacity));
  }

  @Override
  public void trimToSize() {
    doAction(l -> super.trimToSize());
  }

  @Override
  public void sort(Comparator<? super T> comparator) {
    doAction(l -> super.sort(comparator));
  }

  @Override
  public void forEach(Consumer<? super T> action) {
    checkPending();
    modLock.lock();
    try {
      int size = super.size();
      for (int i = 0; i < size; i++) {
        try {
          action.accept(super.get(i));
        } catch (Exception te) {
          PLogger.error(te);
        }
      }
    } catch (Exception e) {
      PLogger.error(e);
    } finally {
      modLock.unlock();
    }
  }

  @Override
  public boolean removeIf(Predicate<? super T> filter) {
    checkPending();
    modLock.lock();
    try {
      T[] es = toTypeArray();
      int i = 0;
      int end = es.length;
      for (; i < end; i++) {
        try {
          if (filter.test(es[i])) {
            break;
          }
        } catch (Exception te) {
          PLogger.error(te);
          break;
        }
      }
      if (i < end) {
        int beg = i;
        long[] deathRow = nBits(end - beg);
        deathRow[0] = 1L;
        for (i = beg + 1; i < end; i++) {
          try {
            if (filter.test(es[i])) {
              setBit(deathRow, i - beg);
            }
          } catch (Exception te) {
            PLogger.error(te);
            setBit(deathRow, i - beg);
          }
        }
        int w = beg;
        for (i = beg; i < end; i++) {
          if (isClear(deathRow, i - beg)) {
            es[w++] = es[i];
          }
        }
        int size = shiftTailOverGap(es, w, end, es.length);
        super.clear();
        for (int ii = 0; ii < size; ii++) {
          super.add(es[ii]);
        }
        return true;
      } else {
        return false;
      }
    } catch (Exception e) {
      PLogger.error(e);
    } finally {
      modLock.unlock();
    }
    return false;
  }

  public void removeEach(Consumer<? super T> action) {
    checkPending();
    modLock.lock();
    try {
      int size = super.size();
      for (int i = 0; i < size; i++) {
        try {
          action.accept(super.get(i));
        } catch (Exception te) {
          PLogger.error(te);
        }
      }
      super.clear();
    } catch (Exception e) {
      PLogger.error(e);
    } finally {
      modLock.unlock();
    }
  }

  public int addEach(Function<? super T, Integer> action) {
    checkPending();
    int value = 0;
    modLock.lock();
    try {
      int size = super.size();
      for (int i = 0; i < size; i++) {
        try {
          value += action.apply(super.get(i));
        } catch (Exception te) {
          PLogger.error(te);
        }
      }
    } catch (Exception e) {
      PLogger.error(e);
    } finally {
      modLock.unlock();
    }
    return value;
  }

  public T getIf(Predicate<? super T> filter) {
    checkPending();
    modLock.lock();
    try {
      int size = super.size();
      for (int i = 0; i < size; i++) {
        try {
          T value = super.get(i);
          if (filter.test(value)) {
            return value;
          }
        } catch (Exception te) {
          PLogger.error(te);
        }
      }
    } catch (Exception e) {
      PLogger.error(e);
    } finally {
      modLock.unlock();
    }
    return null;
  }

  public boolean containsIf(Predicate<? super T> filter) {
    return getIf(filter) != null;
  }

  public boolean meetsIf(Predicate<? super T> filter) {
    checkPending();
    modLock.lock();
    try {
      int size = super.size();
      for (int i = 0; i < size; i++) {
        try {
          if (!filter.test(super.get(i))) {
            return false;
          }
        } catch (Exception te) {
          PLogger.error(te);
          return false;
        }
      }
    } catch (Exception e) {
      PLogger.error(e);
      return false;
    } finally {
      modLock.unlock();
    }
    return true;
  }

  public int countMeetsIf(Predicate<? super T> filter) {
    checkPending();
    int count = 0;
    modLock.lock();
    try {
      int size = super.size();
      for (int i = 0; i < size; i++) {
        try {
          if (filter.test(super.get(i))) {
            count++;
          }
        } catch (Exception te) {
          PLogger.error(te);
        }
      }
    } catch (Exception e) {
      PLogger.error(e);
    } finally {
      modLock.unlock();
    }
    return count;
  }

  @SuppressWarnings("unchecked")
  public T[] toTypeArray() {
    checkPending();
    return (T[]) super.toArray();
  }

  public int pendingSize() {
    return pending.size();
  }

  private boolean doAction(Consumer<PArrayList<T>> action) {
    checkPending();
    if (modLock.tryLock()) {
      try {
        action.accept(this);
      } catch (Exception e) {
        PLogger.error(e);
      } finally {
        modLock.unlock();
      }
      return true;
    }
    pending.add(action);
    return false;
  }

  private void checkPending() {
    if (pending.isEmpty()) {
      return;
    }
    if (modLock.tryLock()) {
      try {
        List<Consumer<PArrayList<T>>> pending = new ArrayList<>(this.pending);
        this.pending.clear();
        pending.forEach(p -> p.accept(this));
      } catch (Exception e) {
        PLogger.error(e);
      } finally {
        modLock.unlock();
      }
    }
  }

  private static long[] nBits(int n) {
    return new long[((n - 1) >> 6) + 1];
  }

  private static void setBit(long[] bits, int i) {
    bits[i >> 6] |= 1L << i;
  }

  private static boolean isClear(long[] bits, int i) {
    return (bits[i >> 6] & (1L << i)) == 0;
  }

  private static int shiftTailOverGap(Object[] es, int lo, int hi, int size) {
    System.arraycopy(es, hi, es, lo, size - hi);
    for (int to = size, i = (size -= hi - lo); i < to; i++) {
      es[i] = null;
    }
    return size;
  }

  private class UnsafeLock implements Lock {
    private boolean locked;

    @Override
    public void lock() {
      if (locked) {
        throw new IllegalMonitorStateException();
      }
      locked = true;
    }

    @Override
    public void lockInterruptibly() {
      throw new UnsupportedOperationException();
    }

    @Override
    public Condition newCondition() {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean tryLock() {
      return !locked ? locked = true : false;
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void unlock() {
      locked = false;
    }
  }
}
