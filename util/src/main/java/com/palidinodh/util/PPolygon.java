package com.palidinodh.util;

import java.awt.Polygon;
import java.awt.geom.Point2D;

public class PPolygon extends Polygon {
  public static final long serialVersionUID = 20190229;

  public PPolygon(Point2D... points) {
    for (Point2D point : points) {
      addPoint((int) point.getX(), (int) point.getY());
    }
  }

  public PPolygon(int[] xPoints, int[] yPoints) {
    super(xPoints, yPoints, xPoints.length);
  }

  public static Point2D getPoint(int x, int y) {
    return new Point2D.Double(x, y);
  }
}
