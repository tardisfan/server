package com.palidinodh.osrsscript.world.event.competitivehiscores;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.ScriptId;
import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategory;
import com.palidinodh.rs.adaptive.RsFriend;
import com.palidinodh.util.PEvent;
import com.palidinodh.util.PNumber;
import lombok.var;

public class CompetitiveHiscoresEvent extends PEvent {
  private static final int WIDGET_FIRST_DURATION = 30;
  private static final int WIDGET_LAST_DURATION = 53;
  private static final int WIDGET_FIRST_CATEGORY = 69;
  private static final int WIDGET_LAST_CATEGORY = 118;
  private static final int WIDGET_FIRST_USERNAME = 126;
  private static final int WIDGET_LAST_USERNAME = 240;

  private List<CompetitiveHiscoresTracking> trackings = new ArrayList<>();

  public CompetitiveHiscoresEvent() {
    super(50);
    for (var duration : CompetitiveHiscoresDuration.values()) {
      trackings.add(new CompetitiveHiscoresTracking(duration));
    }
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("competitive_hiscores_update")) {
      update((Player) args[0], (CompetitiveHiscoresCategory) args[1], (long) args[2]);
    }
    return null;
  }

  @Override
  public void execute() {
    trackings.forEach(d -> d.checkExpiration());
  }

  public void update(Player player, CompetitiveHiscoresCategory category, long value) {
    trackings.forEach(d -> d.update(player, category, value));
  }

  public void open(Player player) {
    player.putAttribute("hiscores_sorting", CompetitiveHiscoresSorting.INDIVIDUALS);
    var selectedCategory = 0;
    player.putAttribute("hiscores_category", selectedCategory);
    player.putAttribute("hiscores_tracking_entries", getTracking(selectedCategory).getCurrent());
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.HISCORES_1009);
    player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, 64, "");
    player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, 65, "");
    sendSorting(player);
    sendDurations(player);
    sendCategories(player);
    sendTotals(player);
  }

  public void hiscoresWidgetHandler(Player player, int option, int childId, int slot, int itemId) {
    if (childId >= WIDGET_FIRST_DURATION && childId <= WIDGET_LAST_DURATION) {
      var displayIndex = childToDuration(childId);
      var duration = getTracking(displayIndex);
      if (duration == null) {
        return;
      }
      player.putAttribute("hiscores_tracking_entries",
          displayIndex % 2 != 0 ? duration.getPrevious() : duration.getCurrent());
      sendDurations(player);
    }
    if (childId >= WIDGET_FIRST_CATEGORY && childId <= WIDGET_LAST_CATEGORY) {
      var displayIndex = childToCategory(childId);
      var category = getCategory(displayIndex);
      if (category == null) {
        return;
      }
      player.putAttribute("hiscores_category", displayIndex);
      sendCategories(player);
    }
    if (childId >= WIDGET_FIRST_USERNAME && childId <= WIDGET_LAST_USERNAME) {
      var sorting = (CompetitiveHiscoresSorting) player.getAttribute("hiscores_sorting");
      var displayIndex = childToEntry(childId);
      if (sorting == CompetitiveHiscoresSorting.CLANS) {
        var selectedCategory = getCategory(player.getAttributeInt("hiscores_category"));
        var entries =
            (CompetitiveHiscoresTrackingEntries) player.getAttribute("hiscores_tracking_entries");
        var clans = CompetitiveHiscoresClan.sort(entries.getClansAsList(), selectedCategory);
        var clan = displayIndex < clans.size() ? clans.get(displayIndex) : null;
        if (clan == null) {
          return;
        }
        player.getGameEncoder().sendMessage(clan.getInformation(selectedCategory));
      }
    }
    switch (childId) {
      case 15:
        player.putAttribute("hiscores_sorting", CompetitiveHiscoresSorting.INDIVIDUALS);
        sendSorting(player);
        break;
      case 18:
        player.putAttribute("hiscores_sorting", CompetitiveHiscoresSorting.FRIENDS);
        sendSorting(player);
        break;
      case 22:
        player.putAttribute("hiscores_sorting", CompetitiveHiscoresSorting.CLANS);
        sendSorting(player);
        break;
      case 27:
        player.putAttribute("hiscores_sorting", CompetitiveHiscoresSorting.SELF);
        sendSorting(player);
        break;
    }
    sendTotals(player);
  }

  public void sendSorting(Player player) {
    var sorting = (CompetitiveHiscoresSorting) player.getAttribute("hiscores_sorting");
    player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, 17,
        sorting == CompetitiveHiscoresSorting.INDIVIDUALS ? "*" : "");
    player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, 21,
        sorting == CompetitiveHiscoresSorting.FRIENDS ? "*" : "");
    player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, 26,
        sorting == CompetitiveHiscoresSorting.CLANS ? "*" : "");
    player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, 29,
        sorting == CompetitiveHiscoresSorting.SELF ? "*" : "");
  }

  public void sendDurations(Player player) {
    var selectedEntries =
        (CompetitiveHiscoresTrackingEntries) player.getAttribute("hiscores_tracking_entries");
    for (var displayIndex = 0;; displayIndex += 2) {
      var currentChildren = durationToChildren(displayIndex);
      var previousChildren = durationToChildren(displayIndex + 1);
      if (currentChildren == null || previousChildren == null) {
        break;
      }
      var currentText = "";
      var previousText = "";
      var tracking = getTracking(displayIndex);
      player.getGameEncoder().sendHideWidget(WidgetId.HISCORES_1009, currentChildren[0],
          tracking == null);
      player.getGameEncoder().sendHideWidget(WidgetId.HISCORES_1009, previousChildren[0],
          tracking == null);
      player.getGameEncoder().sendHideWidget(WidgetId.HISCORES_1009,
          WIDGET_LAST_DURATION + 1 + displayIndex / 2, tracking == null);
      if (tracking != null) {
        currentText = tracking.getDuration().getCurrentName();
        previousText = tracking.getDuration().getPreviousName();
        if (tracking.getCurrent() == selectedEntries) {
          currentText = "<col=ffffff>" + currentText + "</col>";
        }
        if (tracking.getPrevious() == selectedEntries) {
          previousText = "<col=ffffff>" + previousText + "</col>";
        }
        player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, currentChildren[1],
            currentText);
        player.getGameEncoder().sendClientscript(ScriptId.SET_WIDGET_TOOLTIP_8192,
            WidgetId.HISCORES_1009 << 16 | currentChildren[1], WidgetId.HISCORES_1009 << 16 | 242,
            tracking.getDuration().getCurrentDescription(), 500);
        player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, previousChildren[1],
            previousText);
        player.getGameEncoder().sendClientscript(ScriptId.SET_WIDGET_TOOLTIP_8192,
            WidgetId.HISCORES_1009 << 16 | previousChildren[1], WidgetId.HISCORES_1009 << 16 | 242,
            tracking.getDuration().getPreviousDescription(), 500);
      }
    }
  }

  public void sendCategories(Player player) {
    var selectedCategory = player.getAttributeInt("hiscores_category");
    for (var displayIndex = 0;; displayIndex++) {
      var children = categoryToChildren(displayIndex);
      if (children == null) {
        break;
      }
      var text = "";
      var category = getCategory(displayIndex);
      player.getGameEncoder().sendHideWidget(WidgetId.HISCORES_1009, children[0], category == null);
      if (category != null) {
        text = category.getFormattedName();
        if (displayIndex == selectedCategory) {
          text = "<col=ffffff>" + text + "</col>";
        }
        player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, children[1], text);
      }
    }
  }

  public void sendTotals(Player player) {
    var sorting = (CompetitiveHiscoresSorting) player.getAttribute("hiscores_sorting");
    var selectedCategory = getCategory(player.getAttributeInt("hiscores_category"));
    var entries =
        (CompetitiveHiscoresTrackingEntries) player.getAttribute("hiscores_tracking_entries");
    var individuals =
        CompetitiveHiscoresIndividual.sort(entries.getIndividualsAsList(), selectedCategory);
    var clans = CompetitiveHiscoresClan.sort(entries.getClansAsList(), selectedCategory);
    var friends = player.getMessaging().getFriends();
    for (var displayIndex = 0;; displayIndex++) {
      var children = entryToChildren(displayIndex);
      if (children == null) {
        break;
      }
      var text = "";
      var total = "";
      if (sorting == CompetitiveHiscoresSorting.CLANS) {
        var clan = displayIndex < clans.size() ? clans.get(displayIndex) : null;
        player.getGameEncoder().sendHideWidget(WidgetId.HISCORES_1009, children[0], clan == null);
        if (clan != null) {
          text = clan.getName();
          total = PNumber.formatNumber(clan.getTotal(selectedCategory));
          player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, children[1], text);
          player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, children[1] + 1, total);
        }
      } else {
        var individual = sorting == CompetitiveHiscoresSorting.SELF && displayIndex > 0 ? null
            : getIndividual(individuals, displayIndex, sorting, player.getUsername(), friends);
        player.getGameEncoder().sendHideWidget(WidgetId.HISCORES_1009, children[0],
            individual == null);
        if (individual != null) {
          text = individual.getIconImage() + individual.getUsername();
          total = PNumber.formatNumber(individual.getTotal(selectedCategory));
          player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, children[1], text);
          player.getGameEncoder().sendWidgetText(WidgetId.HISCORES_1009, children[1] + 1, total);
        }
      }
    }
  }

  private CompetitiveHiscoresTracking getTracking(int atDisplayIndex) {
    var displayIndex = 0;
    for (var tracking : trackings) {
      if (tracking.getDuration().isHidden()) {
        continue;
      }
      if (displayIndex++ == atDisplayIndex || displayIndex++ == atDisplayIndex) {
        return tracking;
      }
    }
    return null;
  }

  private CompetitiveHiscoresCategory getCategory(int atDisplayIndex) {
    var categories = CompetitiveHiscoresCategory.values();
    var displayIndex = 0;
    for (var category : categories) {
      if (category.isHidden()) {
        continue;
      }
      if (displayIndex++ == atDisplayIndex) {
        return category;
      }
    }
    return null;
  }

  private CompetitiveHiscoresIndividual getIndividual(
      List<CompetitiveHiscoresIndividual> individuals, int atDisplayIndex,
      CompetitiveHiscoresSorting sorting, String username, List<RsFriend> friends) {
    var displayIndex = 0;
    for (var individual : individuals) {
      if (individual.isHidden()) {
        continue;
      }
      if (sorting == CompetitiveHiscoresSorting.FRIENDS
          && !username.equalsIgnoreCase(individual.getUsername())
          && !friends.contains(new RsFriend(individual.getUsername()))) {
        continue;
      }
      if (sorting == CompetitiveHiscoresSorting.SELF
          && !username.equalsIgnoreCase(individual.getUsername())) {
        continue;
      }
      if (displayIndex++ == atDisplayIndex) {
        return individual;
      }
    }
    return null;
  }

  private int[] durationToChildren(int displayIndex) {
    var buttonChildId = WIDGET_FIRST_DURATION;
    var durationTextChildId = buttonChildId + 1;
    for (var i = 1; i <= displayIndex; i++) {
      buttonChildId += 2;
      durationTextChildId = buttonChildId + 1;
    }
    return buttonChildId > WIDGET_LAST_DURATION ? null
        : new int[] { buttonChildId, durationTextChildId };
  }

  private int childToDuration(int childId) {
    if (childId < WIDGET_FIRST_DURATION || childId > WIDGET_LAST_DURATION) {
      return -1;
    }
    return (childId - WIDGET_FIRST_DURATION) / 2;
  }


  private int[] categoryToChildren(int displayIndex) {
    var buttonChildId = WIDGET_FIRST_CATEGORY;
    var categoryTextChildId = buttonChildId + 1;
    for (var i = 1; i <= displayIndex; i++) {
      buttonChildId += i % 2 != 0 ? 2 : 3;
      categoryTextChildId += i % 2 != 0 ? 3 : 2;
    }
    return buttonChildId > WIDGET_LAST_CATEGORY ? null
        : new int[] { buttonChildId, categoryTextChildId };
  }

  private int childToCategory(int childId) {
    if (childId < WIDGET_FIRST_CATEGORY || childId > WIDGET_LAST_CATEGORY) {
      return -1;
    }
    var buttonChildId = WIDGET_FIRST_CATEGORY;
    if (childId == buttonChildId) {
      return 0;
    }
    for (var i = 1; buttonChildId <= WIDGET_LAST_CATEGORY; i++) {
      buttonChildId += i % 2 != 0 ? 2 : 3;
      if (childId == buttonChildId) {
        return i;
      }
    }
    return -1;
  }

  private int[] entryToChildren(int displayIndex) {
    var buttonChildId = WIDGET_FIRST_USERNAME;
    var categoryTextChildId = buttonChildId + 3;
    switch (displayIndex) {
      case 0:
        return new int[] { buttonChildId, categoryTextChildId };
      case 1:
        return new int[] { buttonChildId + 6, categoryTextChildId + 6 };
      case 2:
        return new int[] { buttonChildId + 11, categoryTextChildId + 11 };
    }
    buttonChildId += 16;
    categoryTextChildId += 16;
    for (var i = 4; i <= displayIndex; i++) {
      buttonChildId += i % 2 != 0 ? 4 : 5;
      categoryTextChildId += i % 2 != 0 ? 5 : 4;
    }
    return buttonChildId > WIDGET_LAST_USERNAME ? null
        : new int[] { buttonChildId, categoryTextChildId };
  }

  private int childToEntry(int childId) {
    if (childId < WIDGET_FIRST_USERNAME || childId > WIDGET_LAST_USERNAME) {
      return -1;
    }
    var buttonChildId = WIDGET_FIRST_USERNAME + 2;
    if (childId == buttonChildId) {
      return 0;
    } else if (childId == buttonChildId + 6) {
      return 1;
    } else if (childId == buttonChildId + 11) {
      return 2;
    }
    buttonChildId += 16;
    for (var i = 1; buttonChildId <= WIDGET_LAST_USERNAME; i++) {
      buttonChildId += i % 2 != 0 ? 4 : 5;
      if (childId == buttonChildId) {
        return i;
      }
    }
    return -1;
  }
}
