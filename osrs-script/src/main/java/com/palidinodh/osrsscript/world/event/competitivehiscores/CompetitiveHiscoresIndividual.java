package com.palidinodh.osrsscript.world.event.competitivehiscores;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategory;
import lombok.Getter;
import lombok.var;

@Getter
class CompetitiveHiscoresIndividual {
  private int userId;
  private String username;
  private int icon;
  private boolean hidden;
  private Map<CompetitiveHiscoresCategory, Long> totals = new HashMap<>();

  public CompetitiveHiscoresIndividual(Player player) {
    userId = player.getId();
    update(player, null, 0);
  }

  public void update(Player player, CompetitiveHiscoresCategory category, long value) {
    username = player.getUsername();
    icon = player.getMessaging().getSpriteIndex();
    if (category == null) {
      return;
    }
    var total = totals.containsKey(category) ? totals.get(category) : 0L;
    totals.put(category, total + value);
  }

  public long getTotal(CompetitiveHiscoresCategory category) {
    var value = 0L;
    if (totals.containsKey(category)) {
      value = totals.get(category);
    }
    return value;
  }

  public String getIconImage() {
    return icon > -1 ? "<img=" + icon + ">" : "";
  }

  public static List<CompetitiveHiscoresIndividual> sort(
      List<CompetitiveHiscoresIndividual> individuals, CompetitiveHiscoresCategory category) {
    individuals.sort((i1, i2) -> {
      return Long.compare(i2.getTotal(category), i1.getTotal(category));
    });
    return individuals;
  }
}
