package com.palidinodh.osrsscript.world.event.competitivehiscores;

public enum CompetitiveHiscoresSorting {
  INDIVIDUALS, FRIENDS, CLANS, SELF
}
