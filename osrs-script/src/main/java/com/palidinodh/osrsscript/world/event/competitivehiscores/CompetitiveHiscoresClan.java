package com.palidinodh.osrsscript.world.event.competitivehiscores;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategory;
import com.palidinodh.util.PNumber;
import com.palidinodh.util.PString;
import lombok.Getter;
import lombok.var;

@Getter
class CompetitiveHiscoresClan {
  private int ownerUserId;
  private String ownerUsername;
  private String name;
  private Map<Integer, CompetitiveHiscoresIndividual> individuals = new HashMap<>();

  public CompetitiveHiscoresClan(Player player) {
    ownerUserId = player.getMessaging().getClanChatUserId();
    update(player);
  }

  public void update(Player player) {
    ownerUsername = PString.formatName(player.getMessaging().getClanChatUsername());
    name = PString.formatName(player.getMessaging().getClanChatName());
  }

  public List<CompetitiveHiscoresIndividual> getIndividualsAsList() {
    var list = new ArrayList<CompetitiveHiscoresIndividual>();
    list.addAll(individuals.values());
    return list;
  }

  public long getTotal(CompetitiveHiscoresCategory category) {
    var value = 0L;
    for (var individual : getIndividualsAsList()) {
      value += individual.getTotal(category);
    }
    return value;
  }

  public String getInformation(CompetitiveHiscoresCategory category) {
    var ownerIndividual = individuals.get(ownerUserId);
    var information = new StringBuilder();
    if (ownerIndividual != null) {
      information.append(ownerIndividual.getUsername()).append(": ")
          .append(PNumber.formatNumber(ownerIndividual.getTotal(category)));
    } else {
      information.append(ownerUsername);
    }
    for (var individual : getIndividualsAsList()) {
      if (individual.getUserId() == ownerUserId) {
        continue;
      }
      information.append(",").append(individual.getUsername()).append(": ")
          .append(PNumber.formatNumber(individual.getTotal(category)));
    }
    return information.toString();
  }

  public static List<CompetitiveHiscoresClan> sort(List<CompetitiveHiscoresClan> clans,
      CompetitiveHiscoresCategory category) {
    clans.sort((c1, c2) -> {
      return Long.compare(c2.getTotal(category), c1.getTotal(category));
    });
    return clans;
  }
}
