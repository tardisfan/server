package com.palidinodh.osrsscript.world.event.competitivehiscores;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.var;

@Getter
class CompetitiveHiscoresTrackingEntries {
  private Map<Integer, CompetitiveHiscoresIndividual> individuals = new HashMap<>();
  private Map<Integer, CompetitiveHiscoresClan> clans = new HashMap<>();

  public List<CompetitiveHiscoresIndividual> getIndividualsAsList() {
    var list = new ArrayList<CompetitiveHiscoresIndividual>();
    list.addAll(individuals.values());
    return list;
  }

  public List<CompetitiveHiscoresClan> getClansAsList() {
    var list = new ArrayList<CompetitiveHiscoresClan>();
    list.addAll(clans.values());
    return list;
  }
}
