package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.STATIUSS_WARHAMMER_CHARGED_32255)
class StatiussWarhammerSpecialAttack extends SpecialAttack {
  public StatiussWarhammerSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(35);
    entry.setAnimation(1378);
    entry.setCastGraphic(new Graphic(844));
    addEntry(entry);
  }
}
