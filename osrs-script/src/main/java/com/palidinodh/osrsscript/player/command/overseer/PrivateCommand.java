package com.palidinodh.osrsscript.player.command.overseer;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName("private")
class PrivateCommand implements CommandHandler, CommandHandler.OverseerRank {
  @Override
  public String getExample(String name) {
    return "username";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var targetPlayer = player.getWorld().getPlayerByUsername(message);
    if (targetPlayer == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + message + ".");
      return;
    }
    if (!player.getController().isSameInstance(targetPlayer)) {
      player.getGameEncoder().sendMessage("You must be in the same instance.");
      return;
    }
    if (!targetPlayer.getController().canTeleport(false) && !player.isHigherStaffUsergroup()) {
      player.getGameEncoder()
          .sendMessage("The player you are trying to move can't teleport, please use ::jail.");
      player.log(PlayerLogType.STAFF,
          "failed to teleport " + targetPlayer.getLogName() + " to private");
      return;
    }
    targetPlayer.getGameEncoder().sendMessage(player.getUsername() + " has moved you.");
    player.getGameEncoder().sendMessage(message + " has been moved.");
    targetPlayer.getMovement().teleport(2895, 2727);
    targetPlayer.getController().stopWithTeleport();
    player.log(PlayerLogType.STAFF, "teleported " + targetPlayer.getLogName() + " to private");
  }
}
