package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.MORRIGANS_THROWING_AXE)
class MorrigansThrowingAxeSpecialAttack extends SpecialAttack {
  public MorrigansThrowingAxeSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(50);
    entry.setAnimation(806);
    entry.setCastGraphic(new Graphic(1626, 96));
    entry.setProjectileId(1625);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    var opponent = hooks.getOpponent();
    if (!opponent.isPlayer()) {
      return;
    }
    opponent.asPlayer().getCombat().setMorrigansThrowingAxeSpecial(100);
  }
}
