package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.ARMADYL_GODSWORD, ItemId.ARMADYL_GODSWORD_20593,
    ItemId.ARMADYL_GODSWORD_BEGINNER_32326, ItemId.ARMADYL_GODSWORD_OR })
class ArmadylGodswordSpecialAttack extends SpecialAttack {
  public ArmadylGodswordSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(50);
    entry.setAnimation(7644);
    entry.setCastGraphic(new Graphic(1211));
    entry.setAccuracyModifier(2);
    entry.setDamageModifier(1.1);
    addEntry(entry);
  }
}
