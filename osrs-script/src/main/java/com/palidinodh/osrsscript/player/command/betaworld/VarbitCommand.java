package com.palidinodh.osrsscript.player.command.betaworld;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName("varbit")
class VarbitCommand implements CommandHandler, CommandHandler.BetaWorld {
  @Override
  public String getExample(String name) {
    return "id value";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.splitInt(message);
    player.getGameEncoder().setVarbit(messages[0], messages[1]);
  }
}
