package com.palidinodh.osrsscript.player.plugin.slayer.handler.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.InventoryItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerUnlock;
import com.palidinodh.osrsscript.player.plugin.slayer.SlayerPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.CORPOREAL_BEAST_TASKS_32301)
class CorporealBeastTasksItem implements InventoryItemHandler {
  @Override
  public void itemOption(Player player, int option, int slot, int itemId, boolean isEquipment) {
    var plugin = player.getPlugin(SlayerPlugin.class);
    if (plugin.isUnlocked(SlayerUnlock.CORPOREAL_BEAST)) {
      plugin.lock(SlayerUnlock.CORPOREAL_BEAST);
      player.getGameEncoder()
          .sendMessage("You can no longer be assigned Corporeal Beast boss tasks.");
    } else {
      plugin.unlock(SlayerUnlock.CORPOREAL_BEAST);
      player.getGameEncoder().sendMessage("You can now be assigned Corporeal Beast boss tasks.");
    }
  }
}
