package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.ZAMORAK_GODSWORD, ItemId.ZAMORAK_GODSWORD_OR })
class ZamorakGodswordSpecialAttack extends SpecialAttack {
  public ZamorakGodswordSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(50);
    entry.setAnimation(7638);
    entry.setCastGraphic(new Graphic(1210));
    entry.setTargetGraphic(new Graphic(369));
    entry.setAccuracyModifier(2);
    entry.setDamageModifier(1.1);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    if (hooks.getDamage() == 0) {
      hooks.setTargetGraphic(new Graphic(85, 124));
      return;
    }
    var opponent = hooks.getOpponent();
    if (!opponent.getController().canMagicBind()) {
      return;
    }
    opponent.getController().setMagicBind(34, hooks.getPlayer());
    if (opponent.isPlayer()) {
      opponent.asPlayer().getGameEncoder().sendMessage("<col=ef1020>You have been frozen!</col>");
    }
  }
}
