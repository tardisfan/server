package com.palidinodh.osrsscript.player.command.betaworld;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName("coords")
class CoordsCommand implements CommandHandler, CommandHandler.BetaWorld {
  @Override
  public void execute(Player player, String name, String message) {
    player.getGameEncoder()
        .sendMessage("x=" + player.getX() + ", y=" + player.getY() + ", z=" + player.getHeight()
            + ", client-z=" + player.getClientHeight() + ", region-id=" + player.getRegionId()
            + ", instanced=" + player.getController().isInstanced());
    var localChunkX = (player.getX() - Tile.getAbsRegionX(player.getRegionId())) >> 3;
    var localChunkY = (player.getY() - Tile.getAbsRegionY(player.getRegionId())) >> 3;
    var localChunkId = (localChunkX << 4) + localChunkY;
    player.getGameEncoder()
        .sendMessage("local-chunk: " + localChunkX + ", " + localChunkY + ": " + localChunkId);
    player.getGameEncoder().sendMessage("map-clip="
        + player.getController().getMapClip(player.getX(), player.getY(), player.getHeight()));
    player.getGameEncoder()
        .sendMessage("solid-map-object=" + player.getController().getSolidMapObject(player));
    player.getGameEncoder().sendMessage("overlay=" + player.getController().getMapOverlay(player));
  }
}
