package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.DRAGON_KNIFE, ItemId.DRAGON_KNIFE_P, ItemId.DRAGON_KNIFE_P_PLUS,
    ItemId.DRAGON_KNIFE_P_PLUS_PLUS })
class DragonKnifeSpecialAttack extends SpecialAttack {
  public DragonKnifeSpecialAttack() {
    var entry = new Entry(this);
    entry.addItemId(ItemId.DRAGON_KNIFE);
    entry.setDrain(25);
    entry.setAnimation(2068);
    entry.setProjectileId(699);
    entry.setDoubleHit(true);
    addEntry(entry);

    entry = new Entry(this);
    entry.addItemId(ItemId.DRAGON_KNIFE_P);
    entry.addItemId(ItemId.DRAGON_KNIFE_P_PLUS);
    entry.addItemId(ItemId.DRAGON_KNIFE_P_PLUS_PLUS);
    entry.setDrain(25);
    entry.setAnimation(2068);
    entry.setProjectileId(1629);
    entry.setDoubleHit(true);
    addEntry(entry);
  }
}
