package com.palidinodh.osrsscript.player.skill.mining;

import lombok.Getter;

@Getter
class MiningPickaxe {
  private int itemId;
  private int level;
  private int animation;
  private int wallAnimation;

  public MiningPickaxe(int itemId, int level, int animation, int wallAnimation) {
    this.itemId = itemId;
    this.level = level;
    this.animation = animation;
    this.wallAnimation = wallAnimation;
  }
}
