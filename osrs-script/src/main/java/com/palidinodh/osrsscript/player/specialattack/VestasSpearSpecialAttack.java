package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.VESTAS_SPEAR_CHARGED_32256)
class VestasSpearSpecialAttack extends SpecialAttack {
  public VestasSpearSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(50);
    entry.setAnimation(2927);
    entry.setCastGraphic(new Graphic(1627));
    addEntry(entry);
  }
}
