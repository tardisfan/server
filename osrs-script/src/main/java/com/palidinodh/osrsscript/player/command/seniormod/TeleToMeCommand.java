package com.palidinodh.osrsscript.player.command.seniormod;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName("teletome")
class TeleToMeCommand implements CommandHandler, CommandHandler.SeniorModeratorRank {
  @Override
  public String getExample(String name) {
    return "username";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var targetPlayer = player.getWorld().getPlayerByUsername(message);
    if (targetPlayer == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + message + ".");
      return;
    }
    if (player == targetPlayer) {
      player.getGameEncoder().sendMessage("You can't teleport to yourself.");
      return;
    }
    if (!player.getController().isSameInstance(targetPlayer)) {
      player.getGameEncoder().sendMessage("You must be in the same instance.");
      return;
    }
    if (!targetPlayer.getController().canTeleport(false)) {
      player.getGameEncoder()
          .sendMessage("The player you are trying to move can't teleport, please use ::jail.");
      return;
    }
    targetPlayer.getMovement().teleport(player);
    player.log(PlayerLogType.STAFF, "teletome " + targetPlayer.getLogName());
  }
}
