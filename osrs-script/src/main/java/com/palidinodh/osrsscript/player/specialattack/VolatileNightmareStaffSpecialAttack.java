package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.VOLATILE_NIGHTMARE_STAFF)
class VolatileNightmareStaffSpecialAttack extends SpecialAttack {
  public VolatileNightmareStaffSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(55);
    entry.setAnimation(8532);
    entry.setTargetGraphic(new Graphic(78));
    entry.setAccuracyModifier(1.5);
    entry.setMagic(true);
    entry.setMagicDamage(44);
    entry.setMagicLevelScale(75);
    entry.setMagicLevelScaleDivider(1.5);
    addEntry(entry);
  }
}
