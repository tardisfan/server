package com.palidinodh.osrsscript.player.command.betaworld;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName("hp")
class HitpointsCommand implements CommandHandler, CommandHandler.BetaWorld {
  @Override
  public String getExample(String name) {
    return "amount";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var amount = Integer.parseInt(message);
    player.getMovement().setEnergy(amount);
    player.getCombat().setHitpoints(amount);
    player.getGameEncoder().sendMessage("You set your hitpoints to " + amount + ".");
  }
}
