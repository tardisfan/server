package com.palidinodh.osrsscript.player.plugin.slayer.handler.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.InventoryItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrsscript.player.plugin.slayer.ColoredSlayerHelmet;
import com.palidinodh.osrsscript.player.plugin.slayer.SlayerPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.SLAYER_HELMET, ItemId.SLAYER_HELMET_I, ItemId.BLACK_SLAYER_HELMET,
    ItemId.BLACK_SLAYER_HELMET_I, ItemId.GREEN_SLAYER_HELMET, ItemId.GREEN_SLAYER_HELMET_I,
    ItemId.RED_SLAYER_HELMET, ItemId.RED_SLAYER_HELMET_I, ItemId.PURPLE_SLAYER_HELMET,
    ItemId.PURPLE_SLAYER_HELMET_I, ItemId.TURQUOISE_SLAYER_HELMET, ItemId.TURQUOISE_SLAYER_HELMET_I,
    ItemId.HYDRA_SLAYER_HELMET, ItemId.HYDRA_SLAYER_HELMET_I, ItemId.TWISTED_SLAYER_HELMET,
    ItemId.TWISTED_SLAYER_HELMET_I })
class SlayerHelmetItem implements InventoryItemHandler {
  @Override
  public void itemOption(Player player, int option, int slot, int itemId, boolean isEquipment) {
    var plugin = player.getPlugin(SlayerPlugin.class);
    if (!isEquipment && option == 2 || isEquipment && option == 3) {
      plugin.sendTask();
    }
  }

  @Override
  public boolean itemOnItem(Player player, int useSlot, int useItemId, int onSlot, int onItemId) {
    var plugin = player.getPlugin(SlayerPlugin.class);
    for (var slayerHelmet : ColoredSlayerHelmet.values()) {
      if (!InventoryItemHandler.used(useItemId, onItemId, slayerHelmet.getFromHelmetId(),
          slayerHelmet.getFromAttachmentId())) {
        continue;
      }
      if (slayerHelmet.getUnlock() != null && !plugin.isUnlocked(slayerHelmet.getUnlock())) {
        player.getGameEncoder().sendMessage("You need to unlock this feature first.");
        return true;
      }
      player.getInventory().deleteItem(useItemId, 1, useSlot);
      player.getInventory().deleteItem(onItemId, 1, onSlot);
      player.getInventory().addItem(slayerHelmet.getToHelmetId(), 1, onSlot);
      return true;
    }
    return false;
  }
}
