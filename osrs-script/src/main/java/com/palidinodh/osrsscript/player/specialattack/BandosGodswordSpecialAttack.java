package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.BANDOS_GODSWORD, ItemId.BANDOS_GODSWORD_OR })
class BandosGodswordSpecialAttack extends SpecialAttack {
  public BandosGodswordSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(50);
    entry.setAnimation(7642);
    entry.setCastGraphic(new Graphic(1212));
    entry.setAccuracyModifier(2);
    entry.setDamageModifier(1.1);
    addEntry(entry);
  }
}
