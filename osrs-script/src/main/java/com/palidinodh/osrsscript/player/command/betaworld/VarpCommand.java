package com.palidinodh.osrsscript.player.command.betaworld;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName({ "varp", "varploop" })
class VarpCommand implements CommandHandler, CommandHandler.BetaWorld {
  @Override
  public String getExample(String name) {
    switch (name) {
      case "id value":
        return "";
      case "varploop":
        return "start_id end_id value";
    }
    return "";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.splitInt(message);
    switch (name) {
      case "varp":
        player.getGameEncoder().setVarp(messages[0], messages[1]);
        break;
      case "varploop":
        for (var i = messages[0]; i < messages[1]; i++) {
          player.getGameEncoder().setVarp(i, messages[2]);
        }
        break;
    }
  }
}
