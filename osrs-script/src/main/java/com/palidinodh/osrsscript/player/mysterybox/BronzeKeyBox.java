package com.palidinodh.osrsscript.player.mysterybox;

import java.util.List;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.MysteryBox;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.BRONZE_KEY_32306)
class BronzeKeyBox extends MysteryBox {
  private static List<RandomItem> weightless = RandomItem.weightless(ItemTables.SUPPLIES);

  @Override
  public Item getRandomItem(Player player) {
    return RandomItem.getItem(ItemTables.SUPPLIES);
  }

  @Override
  public List<RandomItem> getAllItems(Player player) {
    return weightless;
  }
}
