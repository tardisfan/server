package com.palidinodh.osrsscript.player.command.betaworld;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.npc.NpcDef;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName("pnpc")
class PnpcCommand implements CommandHandler, CommandHandler.BetaWorld {
  @Override
  public String getExample(String name) {
    return "id";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var id = Integer.parseInt(message);
    player.getAppearance().setNpcId(id);
    if (id != -1 && NpcDef.getNpcDef(id) != null) {
      var animations = NpcDef.getNpcDef(id).getStanceAnimations();
      player.getGameEncoder().sendMessage("Anim: " + animations[0]);
    }
  }
}
