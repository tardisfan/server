package com.palidinodh.osrsscript.player.plugin.hunter;

import java.util.ArrayDeque;
import java.util.Deque;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.map.TempMapObject;
import lombok.Getter;
import lombok.var;

public class HunterPlugin implements PlayerPlugin {
  @Getter
  private transient Deque<TempMapObject> traps = new ArrayDeque<>();

  public void removeLostTraps() {
    for (var it = traps.iterator(); it.hasNext();) {
      var tempTrap = it.next();
      if (!tempTrap.isRunning()) {
        it.remove();
      }
    }
  }
}
