package com.palidinodh.osrsscript.player.plugin.bountyhunter;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.item.ItemDef;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.var;

@AllArgsConstructor
@Getter
public enum MysteriousEmblem {
  ARCHAIC_TIER_1(ItemId.ARCHAIC_EMBLEM_TIER_1, 75_000, 0),
  ARCHAIC_TIER_2(ItemId.ARCHAIC_EMBLEM_TIER_2, 100_000, 0),
  ARCHAIC_TIER_3(ItemId.ARCHAIC_EMBLEM_TIER_3, 200_000, 0),
  ARCHAIC_TIER_4(ItemId.ARCHAIC_EMBLEM_TIER_4, 400_000, 0),
  ARCHAIC_TIER_5(ItemId.ARCHAIC_EMBLEM_TIER_5, 750_000, 0),
  ARCHAIC_TIER_6(ItemId.ARCHAIC_EMBLEM_TIER_6, 1_200_000, 0),
  ARCHAIC_TIER_7(ItemId.ARCHAIC_EMBLEM_TIER_7, 1_750_000, 0),
  ARCHAIC_TIER_8(ItemId.ARCHAIC_EMBLEM_TIER_8, 2_500_000, 0),
  ARCHAIC_TIER_9(ItemId.ARCHAIC_EMBLEM_TIER_9, 3_500_000, 0),
  ARCHAIC_TIER_10(ItemId.ARCHAIC_EMBLEM_TIER_10, 5_000_000, 0),
  TIER_1(ItemId.MYSTERIOUS_EMBLEM_TIER_1, 75_000, 1),
  TIER_2(ItemId.MYSTERIOUS_EMBLEM_TIER_2, 150_000, 2),
  TIER_3(ItemId.MYSTERIOUS_EMBLEM_TIER_3, 300_000, 3),
  TIER_4(ItemId.MYSTERIOUS_EMBLEM_TIER_4, 600_000, 4),
  TIER_5(ItemId.MYSTERIOUS_EMBLEM_TIER_5, 1_200_000, 5);

  private int itemId;
  private int value;
  private int varbitValue;

  public boolean isArchaic() {
    return name().startsWith("ARCHAIC_");
  }

  public static int getValue(int itemId) {
    for (MysteriousEmblem emblem : values()) {
      if (itemId == emblem.itemId || ItemDef.getUnnotedId(itemId) == emblem.itemId) {
        return emblem.value;
      }
    }
    return 0;
  }

  public static int getPreviousId(int itemId) {
    var values = values();
    for (int i = 0; i < values.length; i++) {
      MysteriousEmblem emblem = values[i];
      if (itemId != emblem.itemId) {
        continue;
      }
      if (emblem.isArchaic()) {
        break;
      }
      return i - 1 >= 0 ? values[i - 1].itemId : itemId;
    }
    return -1;
  }

  public static int getNextId(int itemId) {
    var values = values();
    for (int i = 0; i < values.length; i++) {
      MysteriousEmblem emblem = values()[i];
      if (itemId != emblem.itemId) {
        continue;
      }
      if (emblem.isArchaic()) {
        break;
      }
      return i + 1 < values.length ? values[i + 1].itemId : itemId;
    }
    return -1;
  }

  public static boolean isEmblem(int itemId) {
    return getNextId(itemId) != -1;
  }
}
