package com.palidinodh.osrsscript.player.plugin.bountyhunter.handler.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.InventoryItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrsscript.player.plugin.bountyhunter.BountyHunterPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.BOUNTY_TELEPORT_SCROLL)
class BountyTeleportScrollItem implements InventoryItemHandler {
  @Override
  public void itemOption(Player player, int option, int slot, int itemId, boolean isEquipment) {
    var plugin = player.getPlugin(BountyHunterPlugin.class);
    if (plugin.isTeleportUnlocked()) {
      player.getGameEncoder().sendMessage("You already have this spell unlocked.");
      return;
    }
    player.getInventory().deleteItem(itemId, 1, slot);
    plugin.setTeleportUnlocked(true);
    player.getGameEncoder().sendMessage("You have unlocked the Teleport to Bounty Target spell.");
    player.getGameEncoder().sendMessage(
        "If you or the target are above 20 Wilderness, you need to be within 10 Wilderness levels of eachother.");
  }
}
