package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.DRAGON_SWORD, ItemId.DRAGON_SWORD_21206 })
class DragonSwordSpecialAttack extends SpecialAttack {
  public DragonSwordSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(40);
    entry.setAnimation(7515);
    entry.setCastGraphic(new Graphic(1369, 100));
    entry.setAccuracyModifier(1.25);
    entry.setDamageModifier(1.25);
    addEntry(entry);
  }
}
