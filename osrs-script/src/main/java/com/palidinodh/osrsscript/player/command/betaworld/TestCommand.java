package com.palidinodh.osrsscript.player.command.betaworld;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.route.Route;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("test")
class TestCommand implements CommandHandler, CommandHandler.BetaWorld {
  // Bullshit class to test things out combined with hot code replace
  @Override
  public void execute(Player player, String name, String message) {
    player.getGameEncoder()
        .sendMessage(Route.canMove(player, player.getX(), player.getY() + 2) + "");
    player.getGameEncoder()
        .sendMessage(Route.canReachTile(player, new Tile(player).moveY(2), false) + "");
  }
}
