package com.palidinodh.osrsscript.player.plugin.slayer.handler.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.InventoryItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerUnlock;
import com.palidinodh.osrsscript.player.plugin.slayer.SlayerPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.WILDERNESS_BOSS_TASKS_32336)
class WildernessBossTasksItem implements InventoryItemHandler {
  @Override
  public void itemOption(Player player, int option, int slot, int itemId, boolean isEquipment) {
    var plugin = player.getPlugin(SlayerPlugin.class);
    if (plugin.isUnlocked(SlayerUnlock.WILDERNESS_BOSS)) {
      plugin.lock(SlayerUnlock.WILDERNESS_BOSS);
      player.getGameEncoder().sendMessage("You can now be assigned wilderness boss tasks.");
    } else {
      plugin.unlock(SlayerUnlock.WILDERNESS_BOSS);
      player.getGameEncoder().sendMessage("You can no longer be assigned wilderness boss tasks.");
    }
  }
}
