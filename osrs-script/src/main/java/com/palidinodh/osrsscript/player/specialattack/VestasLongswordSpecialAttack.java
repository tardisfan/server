package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.VESTAS_LONGSWORD_CHARGED_32254)
class VestasLongswordSpecialAttack extends SpecialAttack {
  public VestasLongswordSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(25);
    entry.setAnimation(4198);
    addEntry(entry);
  }
}
