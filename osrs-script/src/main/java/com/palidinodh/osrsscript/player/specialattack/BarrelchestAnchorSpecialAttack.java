package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.BARRELCHEST_ANCHOR)
class BarrelchestAnchorSpecialAttack extends SpecialAttack {
  public BarrelchestAnchorSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(50);
    entry.setAnimation(5870);
    entry.setCastGraphic(new Graphic(1027));
    entry.setAccuracyModifier(2.0);
    entry.setDamageModifier(1.1);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    if (!hooks.getOpponent().isPlayer()) {
      return;
    }
    var skillId = PRandom.arrayRandom(Skills.ATTACK, Skills.DEFENCE, Skills.RANGED, Skills.MAGIC);
    hooks.getOpponent().asPlayer().getSkills().changeStat(skillId,
        (int) -(hooks.getDamage() * 0.1));
  }
}
