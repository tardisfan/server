package com.palidinodh.osrsscript.player.plugin.magic.handler.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.InventoryItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.RUNE_POUCH)
class RunePouchItem implements InventoryItemHandler {
  @Override
  public void itemOption(Player player, int option, int slot, int itemId, boolean isEquipment) {
    if (option == 0) {
      player.getController().openRunePouch();
    } else if (option == 3) {
      player.getController().removeRunePouchRunes();
    }
  }

  @Override
  public boolean itemOnItem(Player player, int useSlot, int useItemId, int onSlot, int onItemId) {
    var slot = onItemId == ItemId.RUNE_POUCH ? useSlot : onSlot;
    return player.getController().addRunePouchRune(slot, player.getInventory().getAmount(slot));
  }
}
