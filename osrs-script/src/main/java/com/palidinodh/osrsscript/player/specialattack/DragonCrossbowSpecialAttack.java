package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.DRAGON_CROSSBOW)
class DragonCrossbowSpecialAttack extends SpecialAttack {
  public DragonCrossbowSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(60);
    entry.setAnimation(4230);
    entry.setTargetGraphic(new Graphic(157, 96));
    entry.setProjectileId(698);
    entry.setDamageModifier(1.2);
    addEntry(entry);
  }
}
