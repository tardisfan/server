package com.palidinodh.osrsscript.player.plugin.combat.hook;

import java.util.ArrayList;
import java.util.Collections;
import com.palidinodh.io.JavaCord;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.combat.EloRating;
import com.palidinodh.osrscore.model.entity.player.combat.PlayerCombatDeathItemsHooks;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.ItemList;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategory;
import com.palidinodh.osrscore.world.PKRaffle;
import com.palidinodh.osrsscript.player.plugin.bountyhunter.BountyHunterPlugin;
import com.palidinodh.osrsscript.player.plugin.bountyhunter.MysteriousEmblem;
import com.palidinodh.osrsscript.player.plugin.lootingbag.LootingBagPlugin;
import com.palidinodh.osrsscript.world.event.holidayboss.HolidayBossEvent;
import com.palidinodh.osrsscript.world.event.wildernesshotspot.WildernessHotspotEvent;
import com.palidinodh.osrsscript.world.event.wildernesskey.WildernessKeyEvent;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.adaptive.GrandExchangeItem;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.util.PString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.var;

public class PlayerCombatDeathItems implements PlayerCombatDeathItemsHooks {
  private static int KILL_BLOOD_MONEY = 5_000;

  @AllArgsConstructor
  @Getter
  private enum Killstreak {
    STREAK_1000(1_000, 2.5), //
    STREAK_250(250, 2),
    STREAK_100(100, 1.75),
    STREAK_50(50, 1.5),
    STREAK_10(10, 1.25);

    private int initerval;
    private double bloodMoneyMultiplier;
  }

  @Override
  public Item[] getItemProtectedOnDeath(Player player, Player killer, Item item) {
    return new Item[] { item };
  }

  @Override
  public Item[] getItemLostOnDeath(Player player, Player killer, Item item) {
    if (player == killer) {
      return new Item[] { item };
    }
    if (item.getId() == ItemId.LOOTING_BAG || item.getId() == ItemId.LOOTING_BAG_22586) {
      var lootingBagPlugin = player.getPlugin(LootingBagPlugin.class);
      var lootingBagItems = lootingBagPlugin.getItems().toItemArray();
      lootingBagPlugin.getItems().clear();
      return lootingBagItems;
    }
    if (item.getId() == ItemId.LUNAR_STAFF || item.getId() == ItemId.LUNAR_HELM
        || item.getId() == ItemId.LUNAR_TORSO || item.getId() == ItemId.LUNAR_LEGS
        || item.getId() == ItemId.LUNAR_GLOVES || item.getId() == ItemId.LUNAR_BOOTS
        || item.getId() == ItemId.LUNAR_CAPE || item.getId() == ItemId.LUNAR_AMULET
        || item.getId() == ItemId.LUNAR_RING || item.getId() == ItemId.AMULET_OF_THE_DAMNED_FULL) {
      return new Item[] { new Item(ItemId.COINS, item.getDef().getValue()) };
    }
    if (item.getId() == ItemId.RUNE_POUCH) {
      var runePouch = player.getController().getRunePouch();
      var runes = new Item[runePouch.size() + 1];
      runes[0] = item;
      for (var i = 0; i < runePouch.size(); i++) {
        runes[i + 1] = runePouch.getItem(i);
      }
      runePouch.clear();
      player.getController().sendRunePouchVarbits();
      return runes;
    }
    if (item.getId() == ItemId.ABYSSAL_TENTACLE) {
      return new Item[] { new Item(ItemId.KRAKEN_TENTACLE) };
    }
    if (item.getId() == ItemId.SARADOMINS_BLESSED_SWORD) {
      return new Item[] { new Item(ItemId.SARADOMIN_SWORD) };
    }
    if (item.getId() == ItemId.BRACELET_OF_ETHEREUM) {
      return new Item[] { new Item(ItemId.BRACELET_OF_ETHEREUM_UNCHARGED),
          new Item(ItemId.REVENANT_ETHER, item.getCharges()) };
    }
    if (item.getId() == ItemId.VIGGORAS_CHAINMACE || item.getId() == ItemId.CRAWS_BOW
        || item.getId() == ItemId.THAMMARONS_SCEPTRE) {
      var items = new Item[item.getDef().getExchangeIds().length + 1];
      items[0] = new Item(ItemId.REVENANT_ETHER, item.getCharges());
      for (int i = 0; i < item.getDef().getExchangeIds().length; i++) {
        items[i + 1] = new Item(item.getDef().getExchangeIds()[i]);
      }
      return items;
    }
    if (item.getId() == ItemId.CRYSTAL_BOW || item.getId() == ItemId.CRYSTAL_BOW_INACTIVE
        || item.getId() == ItemId.CRYSTAL_SHIELD || item.getId() == ItemId.CRYSTAL_SHIELD_INACTIVE
        || item.getId() == ItemId.CRYSTAL_HALBERD
        || item.getId() == ItemId.CRYSTAL_HALBERD_INACTIVE) {
      return new Item[] { new Item(ItemId.CRYSTAL_SEED) };
    }
    if (item.getId() == ItemId.BLADE_OF_SAELDOR) {
      return new Item[] { new Item(ItemId.BLADE_OF_SAELDOR_INACTIVE) };
    }
    if (item.getId() == ItemId.VESTAS_LONGSWORD_CHARGED_32254) {
      return new Item[] { new Item(ItemId.VESTAS_LONGSWORD) };
    }
    if (item.getId() == ItemId.VESTAS_SPEAR_CHARGED_32256) {
      return new Item[] { new Item(ItemId.VESTAS_SPEAR) };
    }
    if (item.getId() == ItemId.STATIUSS_WARHAMMER_CHARGED_32255) {
      return new Item[] { new Item(ItemId.STATIUSS_WARHAMMER) };
    }
    if (item.getId() == ItemId.ZURIELS_STAFF_CHARGED_32257) {
      return new Item[] { new Item(ItemId.ZURIELS_STAFF) };
    }
    if (item.getDef().getBrokenId() != -1 && item.getDef().getRepairCoinCost() > 0) {
      var coins = item.getDef().getRepairCoinCost();
      return new Item[] { new Item(item.getDef().getBrokenId(), item.getAmount()),
          coins > 0 ? new Item(ItemId.COINS, coins) : null };
    }
    if (item.getDef().getExchangeIds() != null) {
      var items = new Item[item.getDef().getExchangeIds().length];
      for (var i = 0; i < item.getDef().getExchangeIds().length; i++) {
        items[i] = new Item(item.getDef().getExchangeIds()[i]);
      }
      return items;
    }
    return new Item[] { item };
  }

  @Override
  public boolean isUnprotectable(Player player, Item item) {
    if (MysteriousEmblem.isEmblem(item.getId())) {
      return true;
    }
    if (item.getId() == ItemId.BRONZE_KEY_32306 || item.getId() == ItemId.SILVER_KEY_32307
        || item.getId() == ItemId.GOLD_KEY_32308 || item.getId() == ItemId.DIAMOND_KEY_32309) {
      return true;
    }
    if (item.getId() == ItemId.LOOTING_BAG || item.getId() == ItemId.LOOTING_BAG_22586) {
      return true;
    }
    if (item.getId() == ItemId.BRACELET_OF_ETHEREUM) {
      return true;
    }
    if (item.getId() == ItemId.BLOODY_KEY || item.getId() == ItemId.BLOODIER_KEY) {
      return true;
    }
    if (item.getId() == ItemId.BLOOD_MONEY) {
      return true;
    }
    return false;
  }

  @Override
  public boolean canTransferUntradable(Player player, Item item) {
    if (item.getId() == ItemId.BRONZE_KEY_32306 || item.getId() == ItemId.SILVER_KEY_32307
        || item.getId() == ItemId.GOLD_KEY_32308 || item.getId() == ItemId.DIAMOND_KEY_32309) {
      return true;
    }
    return false;
  }

  @Override
  public void deathDropItems(Player player) {
    var killer = player.getCombat().getPlayerFromHitCount(false);
    if (killer == null) {
      if (player.getLastHitByEntity() != null && player.getLastHitByEntity().isPlayer()) {
        killer = player.getLastHitByEntity().asPlayer();
      } else {
        killer = player;
      }
      var npc = player.getCombat().getNpcFromHitCount(false);
      if (npc != null) {
        var npcInfo = npc.getDef().getName() + " (Level " + npc.getDef().getCombatLevel() + ")";
        var aOrAn = PString.startsWithVowel(npc.getDef().getName()) ? "an " : "a ";
        if (npc.getCombatDef().getKillCount().isSendMessage()) {
          aOrAn = "";
        }
        player.putAttribute("death_reason", aOrAn + npcInfo);
      }
    } else {
      var playerInfo =
          killer.getUsername() + " (Level " + killer.getSkills().getCombatLevel() + ")";
      player.putAttribute("death_reason", "player " + playerInfo);
    }
    if ((player.getArea().inWilderness() || player.getArea().inPvpWorld()) && player != killer) {
      applyUnsafePkDeath(player, killer);
    }
    var allItems = new ArrayList<Item>();
    for (var item : player.getEquipment()) {
      if (item == null || item.getAmount() < 1) {
        continue;
      }
      allItems.add(item);
    }
    player.getEquipment().clear();
    for (var item : player.getInventory()) {
      if (item == null || item.getAmount() < 1) {
        continue;
      }
      allItems.add(item);
    }
    player.getInventory().clear();
    Collections.sort(allItems);
    var protectedItems = new ArrayList<Item>();
    for (var count = 0; count < player.getController().getProtectedItemCount()
        && !allItems.isEmpty(); count++) {
      for (var index = 0; index < allItems.size(); index++) {
        var baseItem = allItems.get(index);
        if (isUnprotectable(player, baseItem)) {
          continue;
        }
        if (baseItem.getAmount() == 1) {
          allItems.remove(index);
        } else {
          allItems.set(index, new Item(baseItem.getId(), baseItem.getAmount() - 1));
          baseItem = new Item(baseItem.getId(), 1);
        }
        var items = getItemProtectedOnDeath(player, killer, baseItem);
        if (items != null) {
          for (var item : items) {
            protectedItems.add(item);
          }
        }
        break;
      }
    }
    var useItemCollection = (player.getController().isInstanced()
        || player.getCombat().getRiskedValue() > player.getController().getItemReclaimFeeHook())
        && !player.getArea().inWilderness() && !player.getArea().inPvpWorld() && player == killer;
    var itemCollection = player.getCombat().getItemCollection();
    if (useItemCollection) {
      var claimFee = player.getController().getItemReclaimFeeHook();
      if (player.isUsergroup(UserRank.OPAL_MEMBER)) {
        claimFee *= 0.75;
      }
      player.getCombat().setItemCollection(itemCollection = (ItemList) new ItemList(64)
          .setPlayer(player).setCAttachment(claimFee).setAlwaysStack(true));
    }
    var itemLogList = new ArrayList<String>();
    for (var i = 0; i < allItems.size(); i++) {
      var items = getItemLostOnDeath(player, killer, allItems.get(i));
      if (items == null) {
        continue;
      }
      for (var item : items) {
        if (item == null || item.getId() < 0 || item.getAmount() < 1
            || item.getDef().isFree() && !item.getDef().getStackable()
                && !player.getController().isFood(item.getId())
                && !player.getController().isDrink(item.getId())) {
          continue;
        }
        if (item.getId() == ItemId.BLOODY_KEY || item.getId() == ItemId.BLOODIER_KEY) {
          player.getWorld().getWorldEvent(WildernessKeyEvent.class).addMapItem(item.getId(), killer,
              MapItem.NORMAL_TIME, MapItem.NORMAL_TIME - 20);
          continue;
        }
        var unprotectable = isUnprotectable(player, item);
        var transferable = canTransferUntradable(player, item);
        if (unprotectable && (!transferable || player == killer)) {
          continue;
        }
        if (item.getDef().getUntradable() && !transferable || item.getAttachment() != null) {
          protectedItems.add(item);
          continue;
        }
        if (useItemCollection && itemCollection.canAddItem(item)) {
          itemCollection.addItem(item);
        } else if (player == killer) {
          if (!player.getArea().inWilderness() && !player.getArea().inPvpWorld()) {
            player.getController().addMapItem(item, player, MapItem.LONG_TIME,
                MapItem.NORMAL_APPEAR);
          } else {
            player.getController().addMapItem(item, player, killer);
          }
        } else if (killer.isGameModeIronmanRelated() || killer.isGameModeHard()
            && !player.isGameModeHard() && GrandExchangeItem.isBlockedHardModeItem(item.getId())) {
          player.getController().addMapItem(item, player, null);
        } else {
          player.getController().addMapItem(item, player, killer);
        }
        itemLogList.add(item.getLogName());
      }
    }
    if (itemLogList.isEmpty()) {
      itemLogList.add("nothing");
    }
    if (useItemCollection && itemCollection != null && !itemCollection.isEmpty()) {
      player.getGameEncoder()
          .sendMessage("<col=ff0000>Speak to Perdu in Edgeville to reclaim any items lost.");
    }
    for (var item : protectedItems) {
      player.getInventory().addItem(item);
    }
    player.getController().addMapItem(new Item(ItemId.BONES), player, killer);
    if (player != killer) {
      player.log(PlayerLogType.PVP_RISKED_ITEM_DEATH,
          "killed by " + killer.getLogName() + " and lost " + PString.toString(itemLogList, ", "));
      killer.log(PlayerLogType.PVP_RISKED_ITEM_DEATH,
          "killed " + player.getLogName() + " and received " + PString.toString(itemLogList, ", "));
    }
  }

  private void applyUnsafePkDeath(Player player, Player killer) {
    killer.getGameEncoder().sendMessage("You have defeated " + player.getUsername() + ".");
    if (killer.inEdgevilleWilderness() || killer.getArea().inPvpWorldUnsafe()) {
      killer.setCombatImmunity(34);
    }
    player.getCombat().setDeaths(player.getCombat().getDeaths() + 1);
    player.getCombat().setTotalDeaths(player.getCombat().getTotalKills() + 1);
    killer.getCombat().setKills(killer.getCombat().getKills() + 1);
    killer.getCombat().setTotalKills(killer.getCombat().getTotalKills() + 1);
    var isTargetKill = killer.getPlugin(BountyHunterPlugin.class).getTarget() == player;
    if (!player.getWorld().isPrimary()
        || !isTargetKill && player.getCombat().isRecentCombatantBlock(killer)) {
      return;
    }
    killer.getWorld().competitiveHiscoresUpdate(killer, CompetitiveHiscoresCategory.WILD_KILLS, 1);
    if (killer.getArea().getWildernessLevel() >= 20) {
      killer.getWorld().competitiveHiscoresUpdate(killer,
          CompetitiveHiscoresCategory.DEEP_WILD_KILLS, 1);
    }
    killer.getCombat().setKillingSpree(killer.getCombat().getKillingSpree() + 1);
    var isHotspot = player.getWorld().getWorldEvent(WildernessHotspotEvent.class).inside(killer);
    if (killer.getSkills().getCombatLevel() < 50
        || !killer.getPlugin(BountyHunterPlugin.class).hasEmblem()) {
      isHotspot = false;
    }
    if (isHotspot) {
      killer.getWorld().competitiveHiscoresUpdate(killer, CompetitiveHiscoresCategory.HOTSPOT_KILLS,
          1);
    }
    Killstreak killstreak = null;
    for (var aStreak : Killstreak.values()) {
      if (killer.getCombat().getKillingSpree() % aStreak.getIniterval() != 0) {
        continue;
      }
      killstreak = aStreak;
      break;
    }
    PKRaffle.addEntry(killer);
    if (isTargetKill) {
      killer.getWorld().competitiveHiscoresUpdate(killer, CompetitiveHiscoresCategory.BOUNTY_KILLS,
          1);
      killer.setCombatImmunity(34);
      killer.getPlugin(BountyHunterPlugin.class).upgradeEmblem();
      var item = RandomItem.getItem(BountyHunterPlugin.TARGET_DROP_TABLE);
      if (item.getId() != -1) {
        player.getController().addMapItem(item, player, killer);
        if (item.getId() == ItemId.VESTAS_LONGSWORD || item.getId() == ItemId.STATIUSS_WARHAMMER
            || item.getId() == ItemId.VESTAS_SPEAR || item.getId() == ItemId.MORRIGANS_JAVELIN
            || item.getId() == ItemId.MORRIGANS_THROWING_AXE
            || item.getId() == ItemId.ZURIELS_STAFF) {
          player.getWorld().sendItemDropNews(killer, item.getId(), "PKing " + player.getUsername());
        }
      }
    }
    var holidayItemId = player.getWorld().getWorldEvent(HolidayBossEvent.class).getItemId();
    if (holidayItemId != -1 && PRandom.randomE(4) == 0) {
      player.getController().addMapItem(new Item(holidayItemId, 2 + PRandom.randomI(4)), player,
          killer);
    }
    var bloodMoneyAwarded = KILL_BLOOD_MONEY;
    if (isHotspot && isTargetKill) {
      bloodMoneyAwarded *= WildernessHotspotEvent.BLOOD_MONEY_MULTIPLIER;
    }
    if (killstreak != null) {
      bloodMoneyAwarded *= killstreak.getBloodMoneyMultiplier();
    }
    var carriedBloodMoney = player.getInventory().getCount(ItemId.BLOOD_MONEY);
    player.getInventory().deleteItem(ItemId.BLOOD_MONEY, Item.MAX_AMOUNT);
    if (!killer.isGameModeNormal() && !killer.isGameModeHard()) {
      carriedBloodMoney = 0;
    }
    bloodMoneyAwarded += carriedBloodMoney;
    if (bloodMoneyAwarded > 0 && Settings.getInstance().isEconomy()) {
      player.getController().addMapItem(new Item(ItemId.BLOOD_MONEY, bloodMoneyAwarded), player,
          killer);
    }
    if (killer.getCombat().getKillingSpree() % 5 == 0) {
      var spreeSprites = new int[] { 8, 7, 6, 5, 4 };
      var spreeMessages =
          new String[] { "on a killing spree", "on a killing frenzy", "running riot",
              "on a rampage", "untouchable", "invincible", "inconceivable", "unfriggenbelievable" };
      var spreeSprite = spreeSprites[Math.min(killer.getCombat().getKillingSpree() / 5 - 1,
          spreeSprites.length - 1)];
      var spreeUsername = "<img=" + spreeSprite + ">" + killer.getUsername();
      var spreeMessage = spreeMessages[Math.min(killer.getCombat().getKillingSpree() / 5 - 1,
          spreeMessages.length - 1)];
      player.getWorld().sendMessage("<col=ff0000>" + spreeUsername + " is " + spreeMessage + "!");
      JavaCord.sendMessage(DiscordChannel.GAME_ANNOUNCEMENTS,
          spreeUsername + " is " + spreeMessage + "!");
    }
    if (player.getCombat().getPKSkullDelay() > 0 && killer.getCombat().getPKSkullDelay() > 0) {
      var myRating = player.getCombat().getElo().getRating();
      var opponentRating = killer.getCombat().getElo().getRating();
      var myState = killer.getCombat().isDead() ? EloRating.DRAW : EloRating.LOSS;
      var opponentState = killer.getCombat().isDead() ? EloRating.DRAW : EloRating.WIN;
      player.getCombat().getElo().updateRating(opponentRating, myState, isTargetKill);
      killer.getCombat().getElo().updateRating(myRating, opponentState, isTargetKill);
      myRating = player.getCombat().getElo().getMonthlyRating();
      opponentRating = killer.getCombat().getElo().getMonthlyRating();
      player.getCombat().getElo().updateMonthlyRating(opponentRating, myState, isTargetKill);
      killer.getCombat().getElo().updateMonthlyRating(myRating, opponentState, isTargetKill);
      myRating = player.getCombat().getElo().getEventRating();
      opponentRating = killer.getCombat().getElo().getEventRating();
      if (!Settings.getInstance().isStaffOnly() && !Settings.getInstance().isBeta()) {
        player.getCombat().getElo().updateEventRating(opponentRating, myState, isTargetKill);
        killer.getCombat().getElo().updateEventRating(myRating, opponentState, isTargetKill);
      }
    }
    player.getCombat().setKillingSpree(0);
  }
}
