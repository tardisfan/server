package com.palidinodh.osrsscript.player.plugin.boss;

import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;

class BossInstanceDialogue extends OptionsDialogue {
  public BossInstanceDialogue(Player player, int npcId, boolean isPrivate) {
    addOption("Enter Area", (c, s) -> BossInstance.join(player, npcId, false));
    if (!isPrivate) {
      addOption("Join Instance", (c, s) -> BossInstance.join(player, npcId, true));
    }
    addOption("Start Instance", (c, s) -> BossInstance.start(player, npcId, isPrivate));
  }
}
