package com.palidinodh.osrsscript.player.command.betaworld;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName("gfx")
class GraphicCommand implements CommandHandler, CommandHandler.BetaWorld {
  @Override
  public String getExample(String name) {
    return "id (height)";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.splitInt(message);
    var height = 0;
    if (messages.length > 1) {
      height = messages[1];
    }
    player.setGraphic(messages[0], height);
  }
}
