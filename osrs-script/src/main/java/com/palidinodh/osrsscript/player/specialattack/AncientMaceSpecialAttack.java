package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.ANCIENT_MACE)
class AncientMaceSpecialAttack extends SpecialAttack {
  public AncientMaceSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(100);
    entry.setAnimation(6147);
    entry.setCastGraphic(new Graphic(1052));
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    if (!hooks.getOpponent().isPlayer()) {
      return;
    }
    var opponent = hooks.getOpponent().asPlayer();
    var prayerDrain = Math.min(hooks.getDamage(), opponent.getPrayer().getPoints());
    opponent.getPrayer().changePoints(-prayerDrain);
    hooks.getPlayer().getPrayer().changePoints(prayerDrain);
  }
}
