package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.BONE_DAGGER, ItemId.BONE_DAGGER_P, ItemId.BONE_DAGGER_P_PLUS,
    ItemId.BONE_DAGGER_P_PLUS_PLUS })
class BoneDaggerSpecialAttack extends SpecialAttack {
  public BoneDaggerSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(75);
    entry.setAnimation(4198);
    entry.setCastGraphic(new Graphic(704));
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    hooks.getOpponent().getCombat().changeDefenceLevel(-hooks.getDamage());
  }
}
