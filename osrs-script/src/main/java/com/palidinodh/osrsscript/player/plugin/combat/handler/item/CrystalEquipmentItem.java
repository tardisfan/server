package com.palidinodh.osrsscript.player.plugin.combat.handler.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.InventoryItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ ItemId.CRYSTAL_BOW, ItemId.CRYSTAL_HALBERD, ItemId.CRYSTAL_SHIELD })
class CrystalEquipmentItem implements InventoryItemHandler {
  @Override
  public void itemOption(Player player, int option, int slot, int itemId, boolean isEquipment) {
    if (option == 3) {
      player.getInventory().deleteItem(itemId, 1, slot);
      player.getInventory().addItem(ItemId.CRYSTAL_SEED, 1, slot);
    }
  }
}
