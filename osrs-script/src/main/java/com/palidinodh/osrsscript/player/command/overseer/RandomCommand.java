package com.palidinodh.osrsscript.player.command.overseer;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName("random")
class RandomCommand implements CommandHandler, CommandHandler.OverseerRank {
  @Override
  public String getExample(String name) {
    return "username";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var targetPlayer = player.getWorld().getPlayerByUsername(message);
    if (targetPlayer == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + message + ".");
      return;
    }
    if (!player.getController().isSameInstance(targetPlayer)) {
      player.getGameEncoder().sendMessage("You must be in the same instances.");
      return;
    }
    if (!targetPlayer.getController().canTeleport(false)) {
      player.getGameEncoder()
          .sendMessage("The player you are trying to move can't teleport, please use ::jail.");
      return;
    }
    player.getMovement().teleport(3228, 3410);
    targetPlayer.getRandomEvent().startRandom(player.getId());
    player.log(PlayerLogType.STAFF, "applied a random event to " + targetPlayer.getLogName());
  }
}
