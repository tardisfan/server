package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.GRANITE_MAUL, ItemId.GRANITE_MAUL_12848, ItemId.GRANITE_MAUL_20557 })
class GraniteMaulSpecialAttack extends SpecialAttack {
  public GraniteMaulSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(50);
    entry.setAnimation(1667);
    entry.setCastGraphic(new Graphic(340, 100));
    entry.setInstant(true);
    addEntry(entry);
  }
}
