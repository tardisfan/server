package com.palidinodh.osrsscript.player.plugin.achievementdiary.handler.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.InventoryItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrsscript.player.plugin.achievementdiary.AchievementDiaryPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PEvent;
import com.palidinodh.util.PTime;
import lombok.var;

@ReferenceId({ ItemId.FALADOR_SHIELD_1, ItemId.FALADOR_SHIELD_2, ItemId.FALADOR_SHIELD_3,
    ItemId.FALADOR_SHIELD_4 })
class FaladorShieldItem implements InventoryItemHandler {
  @Override
  public void itemOption(Player player, int option, int slot, int itemId, boolean isEquipment) {
    var plugin = player.getPlugin(AchievementDiaryPlugin.class);
    switch (itemId) {
      case ItemId.FALADOR_SHIELD_1:
      case ItemId.FALADOR_SHIELD_2: {
        var prayerLevel = player.getController().getLevelForXP(Skills.PRAYER);
        if (!isEquipment && option == 2 || isEquipment && option == 1) {
          if (PTime.getDate().equals(plugin.getFaladorShield1And2())) {
            player.getGameEncoder().sendMessage("You can only use this once a day.");
            break;
          }
          plugin.setFaladorShield1And2(PTime.getDate());
          if (itemId == ItemId.FALADOR_SHIELD_1) {
            player.getPrayer().changePoints((int) (prayerLevel * 0.25));
          } else if (itemId == ItemId.FALADOR_SHIELD_2) {
            player.getPrayer().changePoints((int) (prayerLevel * 0.5));
          }
          player.getGameEncoder().sendMessage("Your prayer points have been restored.");
        }
        break;
      }
      case ItemId.FALADOR_SHIELD_3:
      case ItemId.FALADOR_SHIELD_4: {
        var prayerLevel = player.getController().getLevelForXP(Skills.PRAYER);
        if (!isEquipment && option == 2 || isEquipment && option == 1) {
          if (!PTime.getDate().equals(plugin.getFaladorShield3And4())) {
            plugin.setFaladorShield3And4(PTime.getDate());
            player.getPrayer().changePoints(prayerLevel);
            player.getGameEncoder().sendMessage("Your prayer points have been restored.");
          } else if (!PTime.getDate().equals(plugin.getFaladorShield1And2())) {
            plugin.setFaladorShield1And2(PTime.getDate());
            if (itemId == ItemId.FALADOR_SHIELD_3) {
              player.getPrayer().changePoints((int) (prayerLevel * 0.5));
            } else if (itemId == ItemId.FALADOR_SHIELD_4) {
              player.getPrayer().changePoints(prayerLevel);
            }
            player.getGameEncoder().sendMessage("Your prayer points have been restored.");
          } else {
            player.getGameEncoder().sendMessage("You can only use this once a day.");
          }
        } else if (!isEquipment && option == 3 || isEquipment && option == 2) {
          if (player.getRegionId() != 6992 && player.getRegionId() != 6993) {
            player.getGameEncoder().sendMessage("You can't use this here.");
            break;
          }
          var mole = player.getController().getNpc(NpcId.GIANT_MOLE_230);
          if (mole == null) {
            player.getGameEncoder().sendMessage("Unable to locate the giant mole.");
            break;
          }
          player.getGameEncoder().sendHintIconTile(mole);
          player.getWorld().addEvent(new PEvent(16) {
            @Override
            public void execute() {
              stop();
              if (!player.isVisible()) {
                return;
              }
              player.getGameEncoder().sendHintIconReset();
            }
          });
        }
        break;
      }
    }
  }
}
