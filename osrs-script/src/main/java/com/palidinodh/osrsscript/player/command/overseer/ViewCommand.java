package com.palidinodh.osrsscript.player.command.overseer;

import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName("view")
class ViewCommand implements CommandHandler, CommandHandler.OverseerRank, CommandHandler.Teleport {
  @Override
  public void execute(Player player, String message, String username) {
    var targetPlayer = player.getWorld().getPlayerByUsername(username);
    if (targetPlayer == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + username + ".");
      return;
    }
    if (player == targetPlayer) {
      player.getGameEncoder().sendMessage("You can't view yourself.");
      return;
    }
    if (!player.getController().isSameInstance(targetPlayer)) {
      player.getGameEncoder().sendMessage("You must be in the same instances.");
      return;
    }
    if (!targetPlayer.getController().canTeleport(false) && !player.isHigherStaffUsergroup()) {
      player.getGameEncoder().sendMessage("The player you are trying to view can't teleport.");
      player.log(PlayerLogType.STAFF, "failed to view " + targetPlayer.getLogName());
      return;
    }
    var viewTile = new Tile(targetPlayer);
    viewTile.randomize(1);
    player.getMovement().setViewing(viewTile);
    player.getWidgetManager().sendInventoryOverlay(WidgetId.UNMORPH);
    player.log(PlayerLogType.STAFF, "viewed " + targetPlayer.getLogName());
  }
}
