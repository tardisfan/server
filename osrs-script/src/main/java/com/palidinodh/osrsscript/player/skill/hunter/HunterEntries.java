package com.palidinodh.osrsscript.player.skill.hunter;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.entity.player.skill.SkillModel;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import lombok.Getter;
import lombok.var;

class HunterEntries {
  @Getter
  private static List<SkillEntry> entries = load();
  @Getter
  private static List<CapturedHunterTrap> capturedTrapEntries = loadCapturedTrapEntries();

  private static List<SkillEntry> load() {
    var entries = new ArrayList<SkillEntry>();

    var entry = SkillEntry.builder();
    entry.level(17).experience(18).animation(6606).npc(new SkillModel(NpcId.BABY_IMPLING, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET)).consume(new RandomItem(ItemId.IMPLING_JAR))
        .consume(new RandomItem(ItemId.IMPLING_JAR))
        .create(new RandomItem(ItemId.BABY_IMPLING_JAR));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(22).experience(20).animation(6606).npc(new SkillModel(NpcId.YOUNG_IMPLING, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET)).consume(new RandomItem(ItemId.IMPLING_JAR))
        .create(new RandomItem(ItemId.YOUNG_IMPLING_JAR));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(28).experience(22).animation(6606).npc(new SkillModel(NpcId.GOURMET_IMPLING, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET)).consume(new RandomItem(ItemId.IMPLING_JAR))
        .create(new RandomItem(ItemId.GOURMET_IMPLING_JAR));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(36).experience(25).animation(6606).npc(new SkillModel(NpcId.EARTH_IMPLING, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET)).consume(new RandomItem(ItemId.IMPLING_JAR))
        .create(new RandomItem(ItemId.EARTH_IMPLING_JAR));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(42).experience(27).animation(6606).npc(new SkillModel(NpcId.ESSENCE_IMPLING, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET)).consume(new RandomItem(ItemId.IMPLING_JAR))
        .create(new RandomItem(ItemId.ESSENCE_IMPLING_JAR));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(50).experience(30).animation(6606).npc(new SkillModel(NpcId.ECLECTIC_IMPLING, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET)).consume(new RandomItem(ItemId.IMPLING_JAR))
        .create(new RandomItem(ItemId.ECLECTIC_IMPLING_JAR));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(58).experience(34).animation(6606).npc(new SkillModel(NpcId.NATURE_IMPLING, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET)).consume(new RandomItem(ItemId.IMPLING_JAR))
        .create(new RandomItem(ItemId.NATURE_IMPLING_JAR));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(65).experience(44).animation(6606).npc(new SkillModel(NpcId.MAGPIE_IMPLING, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET)).consume(new RandomItem(ItemId.IMPLING_JAR))
        .create(new RandomItem(ItemId.MAGPIE_IMPLING_JAR));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(74).experience(50).animation(6606).npc(new SkillModel(NpcId.NINJA_IMPLING, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET)).consume(new RandomItem(ItemId.IMPLING_JAR))
        .create(new RandomItem(ItemId.NINJA_IMPLING_JAR));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(83).experience(65).animation(6606).npc(new SkillModel(NpcId.DRAGON_IMPLING, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET)).consume(new RandomItem(ItemId.IMPLING_JAR))
        .create(new RandomItem(ItemId.DRAGON_IMPLING_JAR));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(89).experience(80).animation(6606).npc(new SkillModel(NpcId.LUCKY_IMPLING, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET)).consume(new RandomItem(ItemId.IMPLING_JAR))
        .create(new RandomItem(ItemId.LUCKY_IMPLING_JAR));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(15).experience(24).animation(6606).npc(new SkillModel(NpcId.RUBY_HARVEST, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET)).consume(new RandomItem(ItemId.BUTTERFLY_JAR))
        .create(new RandomItem(ItemId.RUBY_HARVEST));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(25).experience(34).animation(6606).npc(new SkillModel(NpcId.SAPPHIRE_GLACIALIS, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET)).consume(new RandomItem(ItemId.BUTTERFLY_JAR))
        .create(new RandomItem(ItemId.SAPPHIRE_GLACIALIS));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(35).experience(44).animation(6606).npc(new SkillModel(NpcId.SNOWY_KNIGHT, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET)).consume(new RandomItem(ItemId.BUTTERFLY_JAR))
        .create(new RandomItem(ItemId.SNOWY_KNIGHT));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(45).experience(54).animation(6606).npc(new SkillModel(NpcId.BLACK_WARLOCK, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET)).consume(new RandomItem(ItemId.BUTTERFLY_JAR))
        .create(new RandomItem(ItemId.BLACK_WARLOCK));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(1).experience(5).animation(6606).npc(new SkillModel(NpcId.GUANIC_BAT, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET)).create(new RandomItem(ItemId.RAW_GUANIC_BAT_0));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(15).experience(15).animation(6606).npc(new SkillModel(NpcId.PRAEL_BAT, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET)).create(new RandomItem(ItemId.RAW_PRAEL_BAT_1));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(30).experience(20).animation(6606).npc(new SkillModel(NpcId.GIRAL_BAT, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET)).create(new RandomItem(ItemId.RAW_GIRAL_BAT_2));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(45).experience(25).animation(6606).npc(new SkillModel(NpcId.PHLUXIA_BAT, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET)).create(new RandomItem(ItemId.RAW_PHLUXIA_BAT_3));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(60).experience(30).animation(6606).npc(new SkillModel(NpcId.KRYKET_BAT, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET)).create(new RandomItem(ItemId.RAW_KRYKET_BAT_4));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(75).experience(35).animation(6606).npc(new SkillModel(NpcId.MURNG_BAT, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET)).create(new RandomItem(ItemId.RAW_MURNG_BAT_5));
    entries.add(entry.build());

    entry = SkillEntry.builder();
    entry.level(90).experience(38).animation(6606).npc(new SkillModel(NpcId.PSYKK_BAT, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET)).create(new RandomItem(ItemId.RAW_PSYKK_BAT_6));
    entries.add(entry.build());

    return entries;
  }

  private static List<CapturedHunterTrap> loadCapturedTrapEntries() {
    var entries = new ArrayList<CapturedHunterTrap>();

    entries.add(new CapturedHunterTrap(ObjectId.BIRD_SNARE_9373, 1, 34,
        new RandomItem[] { new RandomItem(ItemId.BONES), new RandomItem(ItemId.FEATHER, 6, 15),
            new RandomItem(ItemId.RAW_BIRD_MEAT) }));

    entries.add(new CapturedHunterTrap(ObjectId.BIRD_SNARE_9377, 5, 47,
        new RandomItem[] { new RandomItem(ItemId.BONES), new RandomItem(ItemId.FEATHER, 6, 15),
            new RandomItem(ItemId.RAW_BIRD_MEAT) }));

    entries.add(new CapturedHunterTrap(ObjectId.BIRD_SNARE_9379, 9, 61,
        new RandomItem[] { new RandomItem(ItemId.BONES), new RandomItem(ItemId.FEATHER, 6, 15),
            new RandomItem(ItemId.RAW_BIRD_MEAT) }));

    entries.add(new CapturedHunterTrap(ObjectId.BIRD_SNARE_9375, 11, 65,
        new RandomItem[] { new RandomItem(ItemId.BONES), new RandomItem(ItemId.FEATHER, 6, 15),
            new RandomItem(ItemId.RAW_BIRD_MEAT) }));

    entries.add(new CapturedHunterTrap(ObjectId.BIRD_SNARE_9348, 19, 95,
        new RandomItem[] { new RandomItem(ItemId.BONES), new RandomItem(ItemId.FEATHER, 6, 15),
            new RandomItem(ItemId.RAW_BIRD_MEAT) }));

    entries.add(new CapturedHunterTrap(ObjectId.NET_TRAP_9004, 29, 152,
        new RandomItem[] { new RandomItem(ItemId.SWAMP_LIZARD) }));

    entries.add(new CapturedHunterTrap(ObjectId.NET_TRAP_8734, 47, 224,
        new RandomItem[] { new RandomItem(ItemId.ORANGE_SALAMANDER) }));

    entries.add(new CapturedHunterTrap(ObjectId.NET_TRAP_8986, 59, 272,
        new RandomItem[] { new RandomItem(ItemId.RED_SALAMANDER) }));

    entries.add(new CapturedHunterTrap(ObjectId.NET_TRAP_8996, 67, 319,
        new RandomItem[] { new RandomItem(ItemId.BLACK_SALAMANDER) }));

    entries.add(new CapturedHunterTrap(ObjectId.SHAKING_BOX_9384, 27, 115,
        new RandomItem[] { new RandomItem(ItemId.FERRET) }));

    entries.add(new CapturedHunterTrap(ObjectId.SHAKING_BOX_9382, 53, 198,
        new RandomItem[] { new RandomItem(ItemId.CHINCHOMPA_10033, 5) }));

    entries.add(new CapturedHunterTrap(ObjectId.SHAKING_BOX_9383, 63, 265,
        new RandomItem[] { new RandomItem(ItemId.RED_CHINCHOMPA_10034, 5) }));

    entries.add(new CapturedHunterTrap(ObjectId.SHAKING_BOX, 73, 315,
        new RandomItem[] { new RandomItem(ItemId.BLACK_CHINCHOMPA, 5) }));

    return entries;
  }
}
