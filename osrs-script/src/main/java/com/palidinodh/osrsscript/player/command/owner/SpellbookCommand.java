package com.palidinodh.osrsscript.player.command.owner;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("spellbook")
class SpellbookCommand implements CommandHandler, CommandHandler.OwnerRank {
  @Override
  public void execute(Player player, String user, String message) {
    player.openDialogue("spellbooks", 0);
  }
}
