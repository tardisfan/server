package com.palidinodh.osrsscript.player.plugin.boss;

import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.PrayerChild;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import lombok.Setter;
import lombok.var;

public class BossPlugin implements PlayerPlugin {
  @Inject
  private transient Player player;
  @Setter
  private transient int theNightmareCurse;

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("boss_instance_plugin_cancel")) {
      cancel();
      return Boolean.TRUE;
    }
    return null;
  }

  @Override
  public void restore() {
    theNightmareCurse = 0;
  }

  @Override
  public void tick() {
    if (theNightmareCurse > 0) {
      theNightmareCurse--;
      if (theNightmareCurse == 0) {
        player.getGameEncoder().sendMessage(
            "<col=005500>You feel the effects of the Nightmare's curse wear off.</col>");
      }
    }
  }

  @Override
  public boolean widgetHook(int option, int widgetId, int childId, int slot, int itemId) {
    if (widgetId == WidgetId.PRAYER && theNightmareCurse > 0) {
      var child = PrayerChild.get(childId);
      switch (child) {
        case PROTECT_FROM_MAGIC:
          player.getPrayer().deactivate(child);
          player.getPrayer().activate(PrayerChild.PROTECT_FROM_MISSILES);
          return true;
        case PROTECT_FROM_MISSILES:
          player.getPrayer().deactivate(child);
          player.getPrayer().activate(PrayerChild.PROTECT_FROM_MELEE);
          return true;
        case PROTECT_FROM_MELEE:
          player.getPrayer().deactivate(child);
          player.getPrayer().activate(PrayerChild.PROTECT_FROM_MAGIC);
          return true;
        default:
          return false;
      }
    }
    return false;
  }

  public void start(int npcId, boolean isPrivate) {
    BossInstance.start(player, npcId, isPrivate);
  }

  public void openBossInstanceDialogue(int npcId) {
    openBossInstanceDialogue(npcId, false);
  }

  public void openBossInstanceDialogue(int npcId, boolean isPrivate) {
    player.openDialogue(new BossInstanceDialogue(player, npcId, isPrivate));
  }

  private void cancel() {
    var clanChatUsername = player.getMessaging().getClanChatUsername();
    var playerInstance =
        player.getWorld().getPlayerBossInstance(clanChatUsername, player.getController());
    if (playerInstance == null || !playerInstance.isController(BossInstanceController.class)) {
      player.getGameEncoder().sendMessage("There is no boss instance for this Clan Chat.");
      return;
    }
    if (!player.getMessaging().canClanChatEvent()) {
      player.getGameEncoder()
          .sendMessage("Your Clan Chat privledges aren't high enough to do that.");
      return;
    }
    playerInstance.cast(BossInstanceController.class).expire();
  }
}
