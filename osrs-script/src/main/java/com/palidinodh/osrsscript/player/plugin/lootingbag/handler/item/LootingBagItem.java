package com.palidinodh.osrsscript.player.plugin.lootingbag.handler.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.ScriptId;
import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.InventoryItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrsscript.player.plugin.lootingbag.LootingBagPlugin;
import com.palidinodh.osrsscript.player.plugin.lootingbag.LootingBagStoreType;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.LOOTING_BAG, ItemId.LOOTING_BAG_22586 })
class LootingBagItem implements InventoryItemHandler {
  @Override
  public void itemOption(Player player, int option, int slot, int itemId, boolean isEquipment) {
    var plugin = player.getPlugin(LootingBagPlugin.class);
    if (option == 0) {
      player.getInventory().deleteItem(itemId, 1, slot);
      if (itemId == ItemId.LOOTING_BAG) {
        player.getInventory().addItem(ItemId.LOOTING_BAG_22586, 1, slot);
      } else {
        player.getInventory().addItem(ItemId.LOOTING_BAG, 1, slot);
      }
    } else if (option == 1) {
      plugin.getItems().displayItemList();
    } else if (option == 2) {
      if (!player.getArea().inWilderness() && !player.getArea().inPvpWorld()) {
        player.getGameEncoder()
            .sendMessage("You can't put items in the bag unless you're in the Wilderness.");
        return;
      }
      player.getWidgetManager().sendInventoryOverlay(WidgetId.LOOTING_BAG_DEPOSIT);
      player.getGameEncoder().sendScript(ScriptId.LOOTING_BAG_DEPOSIT, 1, "Looting bag");
      player.getGameEncoder().sendWidgetSettings(WidgetId.LOOTING_BAG_DEPOSIT, 5, 0,
          player.getInventory().capacity() - 1, 1086);
      player.getInventory().setUpdate(true);
    } else if (option == 3) {
      player.openDialogue(new OptionsDialogue(new DialogueOption("Always Ask", (c, s) -> {
        plugin.setStoreType(LootingBagStoreType.ASK);
      }), new DialogueOption("Always Store-1", (c, s) -> {
        plugin.setStoreType(LootingBagStoreType.STORE_1);
      }), new DialogueOption("Always Store-5", (c, s) -> {
        plugin.setStoreType(LootingBagStoreType.STORE_5);
      }), new DialogueOption("Always Store-All", (c, s) -> {
        plugin.setStoreType(LootingBagStoreType.STORE_ALL);
      }), new DialogueOption("Always Store-X", (c, s) -> {
        plugin.setStoreType(LootingBagStoreType.STORE_X);
      })));
    }
  }

  @Override
  public boolean itemOnItem(Player player, int useSlot, int useItemId, int onSlot, int onItemId) {
    var plugin = player.getPlugin(LootingBagPlugin.class);
    var itemSlot = useItemId != ItemId.LOOTING_BAG ? useSlot : onSlot;
    if (plugin.getStoreType() == LootingBagStoreType.ASK) {
      player.openDialogue(new OptionsDialogue(new DialogueOption("Store-1", (c, s) -> {
        plugin.storeItemFromInventory(itemSlot, LootingBagStoreType.STORE_1);
      }), new DialogueOption("Store-5", (c, s) -> {
        plugin.storeItemFromInventory(itemSlot, LootingBagStoreType.STORE_5);
      }), new DialogueOption("Store-All", (c, s) -> {
        plugin.storeItemFromInventory(itemSlot, LootingBagStoreType.STORE_ALL);
      }), new DialogueOption("Store-X", (c, s) -> {
        plugin.storeItemFromInventory(itemSlot, LootingBagStoreType.STORE_X);
      })));
      return true;
    } else if (plugin.getStoreType() == LootingBagStoreType.STORE_1) {
      plugin.storeItemFromInventory(itemSlot, LootingBagStoreType.STORE_1);
    } else if (plugin.getStoreType() == LootingBagStoreType.STORE_5) {
      plugin.storeItemFromInventory(itemSlot, LootingBagStoreType.STORE_5);
    } else if (plugin.getStoreType() == LootingBagStoreType.STORE_ALL) {
      plugin.storeItemFromInventory(itemSlot, LootingBagStoreType.STORE_ALL);
    } else if (plugin.getStoreType() == LootingBagStoreType.STORE_X) {
      plugin.storeItemFromInventory(itemSlot, LootingBagStoreType.STORE_X);
    }
    return true;
  }
}
