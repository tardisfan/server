package com.palidinodh.osrsscript.player.plugin.slayer.handler.item;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.InventoryItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerUnlock;
import com.palidinodh.osrsscript.player.plugin.slayer.SlayerPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(ItemId.RAIDS_TASKS_32329)
class RaidsTasksItem implements InventoryItemHandler {
  @Override
  public void itemOption(Player player, int option, int slot, int itemId, boolean isEquipment) {
    var plugin = player.getPlugin(SlayerPlugin.class);
    if (plugin.isUnlocked(SlayerUnlock.RAIDS)) {
      plugin.lock(SlayerUnlock.RAIDS);
      player.getGameEncoder().sendMessage("You can no longer be assigned raids boss tasks.");
    } else {
      plugin.unlock(SlayerUnlock.RAIDS);
      player.getGameEncoder().sendMessage("You can now be assigned raids boss tasks.");
    }
  }
}
