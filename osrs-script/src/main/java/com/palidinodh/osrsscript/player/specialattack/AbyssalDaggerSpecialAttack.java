package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.ABYSSAL_DAGGER, ItemId.ABYSSAL_DAGGER_P, ItemId.ABYSSAL_DAGGER_P_PLUS,
    ItemId.ABYSSAL_DAGGER_P_PLUS_PLUS })
class AbyssalDaggerSpecialAttack extends SpecialAttack {
  public AbyssalDaggerSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(50);
    entry.setAnimation(3300);
    entry.setCastGraphic(new Graphic(1283));
    entry.setAccuracyModifier(1.25);
    entry.setDamageModifier(0.85);
    entry.setDoubleHit(true);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    if (hooks.getApplyAttackLoopCount() == 0) {
      return;
    }
    if (hooks.getLastHitEvent().getDamage() > 0) {
      return;
    }
    hooks.setDamage(0);
  }
}
