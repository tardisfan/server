package com.palidinodh.osrsscript.player.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId({ ItemId.DRAGON_WARHAMMER, ItemId.DRAGON_WARHAMMER_20785 })
class DragonWarhammerSpecialAttack extends SpecialAttack {
  public DragonWarhammerSpecialAttack() {
    var entry = new Entry(this);
    entry.setDrain(50);
    entry.setAnimation(1378);
    entry.setCastGraphic(new Graphic(1292));
    entry.setDamageModifier(1.5);
    addEntry(entry);
  }
}
