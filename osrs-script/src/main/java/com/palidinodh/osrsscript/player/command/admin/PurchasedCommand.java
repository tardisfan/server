package com.palidinodh.osrsscript.player.command.admin;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import lombok.var;

@ReferenceName("setpurchased")
class PurchasedCommand implements CommandHandler, CommandHandler.AdministratorRank {
  @Override
  public String getExample(String name) {
    return "\"username\" amount";
  }

  @Override
  public void execute(Player player, String user, String message) {
    var messages = CommandHandler.split(message);
    var username = messages[0].replace("_", " ");
    var amount = Integer.parseInt(messages[1]);
    var player2 = player.getWorld().getPlayerByUsername(username);
    if (amount == 0) {
      player2.getBonds().setTotalPurchased(0);
      player.getGameEncoder().sendMessage("Reset purchased bonds for " + player2.getUsername());
      player2.getGameEncoder().sendMessage(player.getUsername() + " reset your purchased bonds.");
    } else {
      player2.getBonds().setTotalPurchased(player2.getBonds().getTotalPurchased() + amount);
      player.getGameEncoder()
          .sendMessage("Added " + amount + " purchased bonds to " + player2.getUsername());
      player2.getGameEncoder().sendMessage(
          player.getUsername() + " added " + amount + " purchased bonds to your account.");
    }
  }
}
