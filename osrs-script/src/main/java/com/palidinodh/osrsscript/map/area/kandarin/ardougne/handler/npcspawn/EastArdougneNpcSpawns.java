package com.palidinodh.osrsscript.map.area.kandarin.ardougne.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class EastArdougneNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.TOOL_LEPRECHAUN, new Tile(2672, 3381)));
    spawns.add(new NpcSpawn(NpcId.MASTER_FARMER, new Tile(2637, 3363), 2));
    spawns.add(new NpcSpawn(NpcId.KRAGEN, new Tile(2663, 3375), 2));
    spawns.add(new NpcSpawn(NpcId.WIZARD_4399, new Tile(2651, 3280), Tile.Direction.NORTH));
    spawns.add(new NpcSpawn(NpcId.MILES, new Tile(2652, 3311)));
    spawns.add(new NpcSpawn(NpcId.MILES, new Tile(2672, 3301), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(2657, 3286), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(2657, 3283), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(2657, 3280), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.GUARD_20, new Tile(2653, 3307), 4));
    spawns.add(new NpcSpawn(NpcId.GUARD_20, new Tile(2654, 3316), 4));
    spawns.add(new NpcSpawn(NpcId.GUARD_20, new Tile(2659, 3311), 4));
    spawns.add(new NpcSpawn(NpcId.GUARD_20, new Tile(2659, 3305), 4));
    spawns.add(new NpcSpawn(NpcId.GUARD_20, new Tile(2664, 3302), 4));
    spawns.add(new NpcSpawn(NpcId.GUARD_20, new Tile(2660, 3301), 4));
    spawns.add(new NpcSpawn(NpcId.GUARD_20, new Tile(2665, 3309), 4));
    spawns.add(new NpcSpawn(NpcId.GUARD_20, new Tile(2668, 3314), 4));
    spawns.add(new NpcSpawn(NpcId.GUARD_20, new Tile(2669, 3298), 4));
    spawns.add(new NpcSpawn(NpcId.HERO_69, new Tile(2650, 3301), 4));
    spawns.add(new NpcSpawn(NpcId.PALADIN_62, new Tile(2661, 3307), 4));
    spawns.add(new NpcSpawn(NpcId.KNIGHT_OF_ARDOUGNE_46, new Tile(2658, 3300), 4));
    spawns.add(new NpcSpawn(NpcId.KNIGHT_OF_ARDOUGNE_46, new Tile(2650, 3307), 4));
    spawns.add(new NpcSpawn(NpcId.KNIGHT_OF_ARDOUGNE_46, new Tile(2665, 3299), 4));
    spawns.add(new NpcSpawn(NpcId.KNIGHT_OF_ARDOUGNE_46, new Tile(2656, 3321), 4));
    spawns.add(new NpcSpawn(NpcId.HERO_69, new Tile(2662, 3320), 4));
    spawns.add(new NpcSpawn(NpcId.GUARD_20, new Tile(2671, 3317), 4));
    spawns.add(new NpcSpawn(NpcId.PALADIN_62, new Tile(2667, 3291), 4));
    spawns.add(new NpcSpawn(NpcId.PALADIN_62, new Tile(2650, 3313), 4));
    spawns.add(new NpcSpawn(NpcId.HERO_69, new Tile(2671, 3305), 4));

    return spawns;
  }
}
