package com.palidinodh.osrsscript.map.area.wilderness.deepwildernessdungeon.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class Wilderness implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.HILL_GIANT_28, new Tile(3045, 10320), 2));
    spawns.add(new NpcSpawn(NpcId.HILL_GIANT_28, new Tile(3042, 10317), 2));
    spawns.add(new NpcSpawn(NpcId.HILL_GIANT_28, new Tile(3045, 10314), 2));
    spawns.add(new NpcSpawn(NpcId.CHAOS_DWARF_48, new Tile(3030, 10312), 4));
    spawns.add(new NpcSpawn(NpcId.CHAOS_DWARF_48, new Tile(3028, 10309), 4));
    spawns.add(new NpcSpawn(NpcId.CHAOS_DWARF_48, new Tile(3034, 10309), 4));
    spawns.add(new NpcSpawn(NpcId.SHADOW_SPIDER_52, new Tile(3018, 10313), 4));
    spawns.add(new NpcSpawn(NpcId.SHADOW_SPIDER_52, new Tile(3017, 10322), 4));
    spawns.add(new NpcSpawn(NpcId.SHADOW_SPIDER_52, new Tile(3024, 10326), 4));
    spawns.add(new NpcSpawn(NpcId.SHADOW_SPIDER_52, new Tile(3030, 10332), 4));
    spawns.add(new NpcSpawn(NpcId.SHADOW_SPIDER_52, new Tile(3024, 10334), 4));
    spawns.add(new NpcSpawn(NpcId.SHADOW_SPIDER_52, new Tile(3028, 10339), 4));
    spawns.add(new NpcSpawn(NpcId.SHADOW_SPIDER_52, new Tile(3033, 10344), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_GIANT_86, new Tile(3051, 10347), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_GIANT_86, new Tile(3047, 10344), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_GIANT_86, new Tile(3046, 10339), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_GIANT_86, new Tile(3050, 10337), 4));
    spawns.add(new NpcSpawn(NpcId.CHAOS_DWARF_48, new Tile(3026, 10312), 4));
    spawns.add(new NpcSpawn(NpcId.CHAOS_DWARF_48, new Tile(3034, 10312), 4));
    spawns.add(new NpcSpawn(NpcId.SHADOW_SPIDER_52, new Tile(3016, 10317), 4));
    spawns.add(new NpcSpawn(NpcId.SHADOW_SPIDER_52, new Tile(3020, 10324), 4));
    spawns.add(new NpcSpawn(NpcId.SHADOW_SPIDER_52, new Tile(3027, 10329), 4));
    spawns.add(new NpcSpawn(NpcId.SHADOW_SPIDER_52, new Tile(3027, 10344), 4));

    return spawns;
  }
}
