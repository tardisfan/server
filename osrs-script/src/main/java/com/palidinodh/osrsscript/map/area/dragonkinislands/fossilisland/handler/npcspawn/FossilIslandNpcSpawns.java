package com.palidinodh.osrsscript.map.area.dragonkinislands.fossilisland.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class FossilIslandNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.DERANGED_ARCHAEOLOGIST_276, new Tile(3680, 3722), 4));
    spawns.add(new NpcSpawn(NpcId.DERANGED_ARCHAEOLOGIST_276, new Tile(3682, 3755), 4));
    spawns.add(new NpcSpawn(NpcId.DERANGED_ARCHAEOLOGIST_276, new Tile(3685, 3767), 4));

    return spawns;
  }
}
