package com.palidinodh.osrsscript.map.area.kharidiandesert.kalphitehive.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class KalphiteHiveNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3277, 9512), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3273, 9514), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3274, 9520), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3278, 9523), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3282, 9527), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3282, 9522), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3278, 9518), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3282, 9516), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3318, 9496), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3322, 9494), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3325, 9497), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3328, 9500), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3330, 9505), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3325, 9508), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3321, 9509), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3319, 9504), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3321, 9499), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3325, 9504), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_GUARDIAN_141, new Tile(3269, 9483), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_GUARDIAN_141, new Tile(3277, 9479), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_GUARDIAN_141, new Tile(3285, 9481), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_GUARDIAN_141, new Tile(3279, 9486), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_GUARDIAN_141, new Tile(3277, 9492), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3307, 9518), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3306, 9522), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3303, 9526), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3300, 9530), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3301, 9537), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3307, 9537), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3312, 9533), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3314, 9528), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3317, 9524), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3312, 9520), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3310, 9526), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3306, 9532), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3311, 9481), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3317, 9477), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3317, 9484), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3324, 9486), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3323, 9478), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3330, 9484), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3332, 9490), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3337, 9495), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_LARVA, new Tile(3297, 9494), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_LARVA, new Tile(3294, 9497), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_LARVA, new Tile(3294, 9502), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_LARVA, new Tile(3300, 9503), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_LARVA, new Tile(3301, 9499), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_LARVA, new Tile(3300, 9494), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_LARVA, new Tile(3296, 9507), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_LARVA, new Tile(3300, 9510), 4));

    return spawns;
  }
}
