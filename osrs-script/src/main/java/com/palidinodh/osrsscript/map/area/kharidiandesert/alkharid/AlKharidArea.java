package com.palidinodh.osrsscript.map.area.kharidiandesert.alkharid;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 13105, 13106 })
public class AlKharidArea extends Area {
}
