package com.palidinodh.osrsscript.map.area.wilderness.edgevilledungeon.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class EdgevilleDungeonNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.THUG_10, new Tile(3129, 9929), 4));
    spawns.add(new NpcSpawn(NpcId.THUG_10, new Tile(3131, 9933), 4));
    spawns.add(new NpcSpawn(NpcId.THUG_10, new Tile(3133, 9936), 4));
    spawns.add(new NpcSpawn(NpcId.THUG_10, new Tile(3132, 9927), 4));
    spawns.add(new NpcSpawn(NpcId.THUG_10, new Tile(3125, 9930), 4));
    spawns.add(new NpcSpawn(NpcId.CHAOS_DRUID_13, new Tile(3120, 9929), 4));
    spawns.add(new NpcSpawn(NpcId.CHAOS_DRUID_13, new Tile(3118, 9925), 4));
    spawns.add(new NpcSpawn(NpcId.CHAOS_DRUID_13, new Tile(3113, 9926), 4));
    spawns.add(new NpcSpawn(NpcId.CHAOS_DRUID_13, new Tile(3110, 9929), 4));
    spawns.add(new NpcSpawn(NpcId.CHAOS_DRUID_13, new Tile(3114, 9930), 4));
    spawns.add(new NpcSpawn(NpcId.CHAOS_DRUID_13, new Tile(3111, 9933), 4));
    spawns.add(new NpcSpawn(NpcId.CHAOS_DRUID_13, new Tile(3106, 9934), 4));
    spawns.add(new NpcSpawn(NpcId.DEADLY_RED_SPIDER_34, new Tile(3127, 9949), 4));
    spawns.add(new NpcSpawn(NpcId.DEADLY_RED_SPIDER_34, new Tile(3123, 9950), 4));
    spawns.add(new NpcSpawn(NpcId.DEADLY_RED_SPIDER_34, new Tile(3119, 9950), 4));
    spawns.add(new NpcSpawn(NpcId.DEADLY_RED_SPIDER_34, new Tile(3122, 9954), 4));
    spawns.add(new NpcSpawn(NpcId.DEADLY_RED_SPIDER_34, new Tile(3127, 9954), 4));
    spawns.add(new NpcSpawn(NpcId.DEADLY_RED_SPIDER_34, new Tile(3124, 9957), 4));
    spawns.add(new NpcSpawn(NpcId.DEADLY_RED_SPIDER_34, new Tile(3119, 9956), 4));
    spawns.add(new NpcSpawn(NpcId.DEADLY_RED_SPIDER_34, new Tile(3115, 9958), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(3109, 9954), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(3105, 9951), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(3105, 9947), 4));
    spawns.add(new NpcSpawn(NpcId.SKELETON_22, new Tile(3105, 9956), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(3098, 9955), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(3092, 9952), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(3087, 9949), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(3080, 9953), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(3086, 9955), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(3091, 9957), 4));
    spawns.add(new NpcSpawn(NpcId.BLACK_DEMON_172, new Tile(3087, 9962), 4));
    spawns.add(new NpcSpawn(NpcId.POISON_SPIDER_64, new Tile(3085, 9934), 4));
    spawns.add(new NpcSpawn(NpcId.POISON_SPIDER_64, new Tile(3088, 9935), 4));
    spawns.add(new NpcSpawn(NpcId.POISON_SPIDER_64, new Tile(3089, 9938), 4));
    spawns.add(new NpcSpawn(NpcId.POISON_SPIDER_64, new Tile(3085, 9938), 4));
    spawns.add(new NpcSpawn(NpcId.POISON_SPIDER_64, new Tile(3089, 9942), 4));
    spawns.add(new NpcSpawn(NpcId.POISON_SPIDER_64, new Tile(3085, 9941), 4));
    spawns.add(new NpcSpawn(NpcId.POISON_SPIDER_64, new Tile(3089, 9932), 4));
    spawns.add(new NpcSpawn(NpcId.POISON_SPIDER_64, new Tile(3091, 9934), 4));
    spawns.add(new NpcSpawn(NpcId.EARTH_WARRIOR_51, new Tile(3118, 9972), 4));
    spawns.add(new NpcSpawn(NpcId.EARTH_WARRIOR_51, new Tile(3120, 9974), 4));
    spawns.add(new NpcSpawn(NpcId.EARTH_WARRIOR_51, new Tile(3121, 9971), 4));
    spawns.add(new NpcSpawn(NpcId.EARTH_WARRIOR_51, new Tile(3124, 9974), 4));
    spawns.add(new NpcSpawn(NpcId.EARTH_WARRIOR_51, new Tile(3122, 9976), 4));
    spawns.add(new NpcSpawn(NpcId.EARTH_WARRIOR_51, new Tile(3120, 9979), 4));
    spawns.add(new NpcSpawn(NpcId.EARTH_WARRIOR_51, new Tile(3120, 9985), 4));
    spawns.add(new NpcSpawn(NpcId.EARTH_WARRIOR_51, new Tile(3119, 9988), 4));
    spawns.add(new NpcSpawn(NpcId.EARTH_WARRIOR_51, new Tile(3123, 9988), 4));
    spawns.add(new NpcSpawn(NpcId.EARTH_WARRIOR_51, new Tile(3124, 9991), 4));
    spawns.add(new NpcSpawn(NpcId.EARTH_WARRIOR_51, new Tile(3120, 9992), 4));
    spawns.add(new NpcSpawn(NpcId.EARTH_WARRIOR_51, new Tile(3120, 9995), 4));
    spawns.add(new NpcSpawn(NpcId.EARTH_WARRIOR_51, new Tile(3125, 9994), 4));
    spawns.add(new NpcSpawn(NpcId.EARTH_WARRIOR_51, new Tile(3122, 9998), 4));
    spawns.add(new NpcSpawn(NpcId.EARTH_WARRIOR_51, new Tile(3116, 9991), 4));

    return spawns;
  }
}
