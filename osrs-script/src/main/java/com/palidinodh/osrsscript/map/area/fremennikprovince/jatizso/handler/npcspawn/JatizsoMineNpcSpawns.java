package com.palidinodh.osrsscript.map.area.fremennikprovince.jatizso.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class JatizsoMineNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(2412, 10193)));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(2375, 10197), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(2404, 10224), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(2417, 10216)));

    return spawns;
  }
}
