package com.palidinodh.osrsscript.map.area.kandarin.sorcererstower.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class SorcerersTowerNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.IGNATIUS_VULCAN, new Tile(2722, 3433), 4));
    spawns.add(new NpcSpawn(NpcId.FLAX_KEEPER, new Tile(2742, 3443), 4));

    return spawns;
  }
}
