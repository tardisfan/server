package com.palidinodh.osrsscript.map.area.misthalin.edgeville.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class EdgevillePvpNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.SURGEON_GENERAL_TAFANI, new Tile(3094, 3499, 20)));
    spawns.add(new NpcSpawn(NpcId.HEAD_CHEF, new Tile(3095, 3499, 20)));
    spawns.add(new NpcSpawn(NpcId.AJJAT, new Tile(3096, 3499, 20)));
    spawns.add(new NpcSpawn(NpcId.WIZARD_4399, new Tile(3097, 3499, 20)));

    return spawns;
  }
}
