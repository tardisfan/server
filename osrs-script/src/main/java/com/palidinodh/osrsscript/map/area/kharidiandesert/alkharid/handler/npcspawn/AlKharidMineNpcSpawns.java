package com.palidinodh.osrsscript.map.area.kharidiandesert.alkharid.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class AlKharidMineNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.SCORPION_14, new Tile(3299, 3278), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPION_14, new Tile(3299, 3289), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPION_14, new Tile(3298, 3300), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPION_14, new Tile(3299, 3312), 4));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(3298, 3294)));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(3298, 3308), Tile.Direction.NORTH));

    return spawns;
  }
}
