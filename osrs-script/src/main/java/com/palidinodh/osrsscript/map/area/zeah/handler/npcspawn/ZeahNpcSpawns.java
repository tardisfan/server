package com.palidinodh.osrsscript.map.area.zeah.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class ZeahNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(1755, 3849), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.MARISI, new Tile(1817, 3485), 2));
    spawns.add(new NpcSpawn(NpcId.TOOL_LEPRECHAUN, new Tile(1811, 3490)));
    spawns.add(new NpcSpawn(NpcId.LIZARDMAN_SHAMAN_150, new Tile(1445, 3696), 4));
    spawns.add(new NpcSpawn(NpcId.LIZARDMAN_SHAMAN_150, new Tile(1433, 3699), 4));
    spawns.add(new NpcSpawn(NpcId.LIZARDMAN_SHAMAN_150, new Tile(1421, 3701), 4));
    spawns.add(new NpcSpawn(NpcId.LIZARDMAN_SHAMAN_150, new Tile(1421, 3712), 4));
    spawns.add(new NpcSpawn(NpcId.LIZARDMAN_SHAMAN_150, new Tile(1429, 3719), 4));
    spawns.add(new NpcSpawn(NpcId.LIZARDMAN_SHAMAN_150, new Tile(1439, 3717), 4));
    spawns.add(new NpcSpawn(NpcId.LIZARDMAN_SHAMAN_150, new Tile(1438, 3707), 4));
    spawns.add(new NpcSpawn(NpcId.LIZARDMAN_SHAMAN_150, new Tile(1486, 3697), 4));
    spawns.add(new NpcSpawn(NpcId.LIZARDMAN_SHAMAN_150, new Tile(1500, 3701), 4));
    spawns.add(new NpcSpawn(NpcId.LIZARDMAN_SHAMAN_150, new Tile(1511, 3693), 4));
    spawns.add(new NpcSpawn(NpcId.LIZARDMAN_SHAMAN_150, new Tile(1523, 3691), 4));
    spawns.add(new NpcSpawn(NpcId.LIZARDMAN_SHAMAN_150, new Tile(1535, 3688), 4));
    spawns.add(new NpcSpawn(NpcId.LIZARDMAN_SHAMAN_150, new Tile(1547, 3695), 4));
    spawns.add(new NpcSpawn(NpcId.LIZARDMAN_SHAMAN_150, new Tile(1551, 3679), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1740, 3471), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1745, 3476), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1749, 3471), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1759, 3470), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1757, 3478), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1766, 3467), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1768, 3473), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1774, 3464), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1780, 3469), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1773, 3469), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1789, 3462), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1791, 3471), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1798, 3463), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1802, 3468), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1805, 3458), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1805, 3449), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1802, 3443), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1809, 3443), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1806, 3440), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1813, 3448), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1810, 3455), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1819, 3451), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1818, 3457), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1826, 3447), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1832, 3449), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1828, 3453), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1827, 3459), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1837, 3451), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1834, 3456), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1838, 3461), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1844, 3459), 4));
    spawns.add(new NpcSpawn(NpcId.SAND_CRAB_15, new Tile(1843, 3466), 4));

    return spawns;
  }
}
