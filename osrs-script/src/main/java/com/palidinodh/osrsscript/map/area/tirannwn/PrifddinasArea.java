package com.palidinodh.osrsscript.map.area.tirannwn;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 8499, 8500, 8755, 8756, 9011, 9012, 12637, 12638, 12639, 12640, 12893, 12894, 12895,
    12896, 13149, 13150, 13151, 13152, 13405, 13406, 13407, 13408 })
public class PrifddinasArea extends Area {
}
