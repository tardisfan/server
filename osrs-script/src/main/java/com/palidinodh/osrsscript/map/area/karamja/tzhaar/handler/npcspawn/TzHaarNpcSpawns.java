package com.palidinodh.osrsscript.map.area.karamja.tzhaar.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class TzHaarNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.WIZARD_4399, new Tile(2448, 5181)));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_KET_KEH, new Tile(2495, 5106), 4));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_MEJ_ROH, new Tile(2461, 5125), 4));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_MEJ_JAL, new Tile(2439, 5169), 2));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_HUR_TEL, new Tile(2477, 5146), 4));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_HUR_LEK, new Tile(2463, 5149), 4));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_KET_ZUH, new Tile(2445, 5178), 4));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_KET_149, new Tile(2481, 5167), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_XIL_133, new Tile(2484, 5168), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_KET_149, new Tile(2488, 5170), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_XIL_133, new Tile(2490, 5175), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_KET_149, new Tile(2491, 5176), 4));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_KET_149, new Tile(2491, 5172), 4));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_MEJ_103, new Tile(2489, 5159), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_KET_149, new Tile(2486, 5156), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_KET_149, new Tile(2491, 5156), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_HUR_74, new Tile(2492, 5159), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_HUR_74, new Tile(2488, 5155), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_HUR_74, new Tile(2492, 5154), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_HUR_74, new Tile(2484, 5159), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_MEJ_103, new Tile(2481, 5154), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_XIL_133_2169, new Tile(2472, 5166), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_KET_149, new Tile(2467, 5164), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_XIL_133_2169, new Tile(2481, 5162), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_KET_149, new Tile(2476, 5166), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_HUR_74, new Tile(2479, 5156), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_KET_149, new Tile(2478, 5151), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_KET_149, new Tile(2474, 5153), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_MEJ_103, new Tile(2472, 5149), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_KET_149, new Tile(2448, 5149), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_XIL_133, new Tile(2452, 5149), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_XIL_133, new Tile(2455, 5155), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_KET_149, new Tile(2460, 5159), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_XIL_133, new Tile(2463, 5161), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_HUR_74, new Tile(2461, 5141), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_MEJ_103, new Tile(2468, 5141), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_KET_149, new Tile(2455, 5140), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_XIL_133_2169, new Tile(2455, 5161), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_XIL_133, new Tile(2439, 5171), 4));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_KET_149, new Tile(2447, 5170), 4));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_KET_149, new Tile(2448, 5166), 4));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_KET_149, new Tile(2442, 5169), 4));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_HUR_74, new Tile(2465, 5145), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_HUR_74, new Tile(2471, 5143), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_HUR_74, new Tile(2465, 5169), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_XIL_133, new Tile(2452, 5168), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_MEJ_103, new Tile(2450, 5123), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_HUR_74, new Tile(2456, 5124), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_MEJ_103, new Tile(2458, 5129), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_MEJ_103, new Tile(2456, 5135), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_HUR_74, new Tile(2469, 5147), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_HUR_74, new Tile(2448, 5143), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_HUR_74, new Tile(2440, 5145), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_XIL_133, new Tile(2442, 5141), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_XIL_133_2169, new Tile(2440, 5137), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_XIL_133_2169, new Tile(2437, 5133), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_XIL_133, new Tile(2440, 5131), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_HUR_74, new Tile(2437, 5129), 8));
    spawns.add(new NpcSpawn(NpcId.TZHAAR_HUR_74, new Tile(2441, 5134), 8));

    return spawns;
  }
}
