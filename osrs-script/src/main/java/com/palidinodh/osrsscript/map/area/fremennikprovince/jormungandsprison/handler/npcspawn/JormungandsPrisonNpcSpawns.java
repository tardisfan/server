package com.palidinodh.osrsscript.map.area.fremennikprovince.jormungandsprison.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class JormungandsPrisonNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.BASILISK_KNIGHT_204, new Tile(2417, 10373), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_KNIGHT_204, new Tile(2423, 10377), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_KNIGHT_204, new Tile(2427, 10383), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_KNIGHT_204, new Tile(2434, 10388), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_KNIGHT_204, new Tile(2436, 10395), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_KNIGHT_204, new Tile(2421, 10384), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_KNIGHT_204, new Tile(2414, 10387), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_KNIGHT_204, new Tile(2406, 10385), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_KNIGHT_204, new Tile(2409, 10392), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_KNIGHT_204, new Tile(2416, 10398), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_KNIGHT_204, new Tile(2424, 10397), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_KNIGHT_204, new Tile(2451, 10379), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_KNIGHT_204, new Tile(2456, 10380), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_KNIGHT_204, new Tile(2460, 10384), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_KNIGHT_204, new Tile(2450, 10384), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_KNIGHT_204, new Tile(2455, 10385), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_KNIGHT_204, new Tile(2451, 10390), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_KNIGHT_204, new Tile(2459, 10390), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_61, new Tile(2459, 10397), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_61, new Tile(2464, 10397), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_61, new Tile(2466, 10401), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_61, new Tile(2461, 10402), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_61, new Tile(2461, 10407), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_61, new Tile(2475, 10402), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_61, new Tile(2480, 10401), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_61, new Tile(2484, 10406), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_61, new Tile(2478, 10406), 4));
    spawns.add(new NpcSpawn(NpcId.BASILISK_61, new Tile(2481, 10410), 4));

    return spawns;
  }
}
