package com.palidinodh.osrsscript.map.area.asgarnia.taverley;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.reference.ReferenceIdSet;

@ReferenceId({ 11161, 11416, 11417, 11671, 11672, 11673, 11928 })
@ReferenceIdSet(primary = 11929,
    secondary = { 0, 1, 2, 3, 4, 5, 16, 17, 18, 19, 20, 21, 32, 33, 46, 37, 48, 49 })
public class TaverleyDungeonArea extends Area {
}
