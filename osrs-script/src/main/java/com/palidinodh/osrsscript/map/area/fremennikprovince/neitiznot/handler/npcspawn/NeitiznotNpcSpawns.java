package com.palidinodh.osrsscript.map.area.fremennikprovince.neitiznot.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class NeitiznotNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.WIZARD_4399, new Tile(2339, 3806), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.YAK_22, new Tile(2318, 3791), 4));
    spawns.add(new NpcSpawn(NpcId.YAK_22, new Tile(2323, 3791), 4));
    spawns.add(new NpcSpawn(NpcId.YAK_22, new Tile(2327, 3793), 4));
    spawns.add(new NpcSpawn(NpcId.YAK_22, new Tile(2319, 3796), 4));
    spawns.add(new NpcSpawn(NpcId.YAK_22, new Tile(2325, 3798), 4));
    spawns.add(new NpcSpawn(NpcId.YAK_22, new Tile(2322, 3795), 4));
    spawns.add(new NpcSpawn(NpcId.MARIA_GUNNARS, new Tile(2311, 3781)));
    spawns.add(new NpcSpawn(NpcId.MORTEN_HOLDSTROM, new Tile(2331, 3803), 4));
    spawns.add(new NpcSpawn(NpcId.JOFRIDR_MORDSTATTER, new Tile(2336, 3807), 4));
    spawns.add(new NpcSpawn(NpcId.MAWNIS_BUROWGAR, new Tile(2335, 3799), 4));
    spawns.add(new NpcSpawn(NpcId.KJEDELIG_UPPSEN, new Tile(2334, 3800), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.TROGEN_KONUNGARDE, new Tile(2334, 3798), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.FRIDLEIF_SHIELDSON, new Tile(2338, 3800)));
    spawns.add(new NpcSpawn(NpcId.THAKKRAD_SIGMUNDSON, new Tile(2339, 3798), Tile.Direction.NORTH));
    spawns.add(new NpcSpawn(NpcId.SLUG_HEMLIGSSEN, new Tile(2337, 3810), 4));
    spawns.add(new NpcSpawn(NpcId.GUNNAR_HOLDSTROM, new Tile(2349, 3802), 4));

    return spawns;
  }
}
