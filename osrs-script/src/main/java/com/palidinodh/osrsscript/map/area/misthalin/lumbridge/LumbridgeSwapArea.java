package com.palidinodh.osrsscript.map.area.misthalin.lumbridge;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 12593, 12849 })
public class LumbridgeSwapArea extends Area {
}
