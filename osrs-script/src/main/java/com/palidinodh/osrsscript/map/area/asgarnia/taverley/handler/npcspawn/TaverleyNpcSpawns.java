package com.palidinodh.osrsscript.map.area.asgarnia.taverley.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class TaverleyNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.ALAIN, new Tile(2932, 3435), 2));
    spawns.add(new NpcSpawn(NpcId.TOOL_LEPRECHAUN, new Tile(2941, 3441), Tile.Direction.WEST));

    return spawns;
  }
}
