package com.palidinodh.osrsscript.map.area.misthalin;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 12079, 12080, 12335, 12336, 12592 })
public class TutorialIslandArea extends Area {
}
