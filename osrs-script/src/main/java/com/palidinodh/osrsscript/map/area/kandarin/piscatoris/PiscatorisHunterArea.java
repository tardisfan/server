package com.palidinodh.osrsscript.map.area.kandarin.piscatoris;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.reference.ReferenceIdSet;

@ReferenceId({ 9015, 9016, 9271, 9272, 9528 })
@ReferenceIdSet(primary = 9527,
    secondary = { 0, 1, 2, 3, 4, 5, 6, 7, 18, 19, 20, 21, 22, 23, 37, 38, 39, 53, 54, 55, 70, 71 })
public class PiscatorisHunterArea extends Area {
}
