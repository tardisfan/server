package com.palidinodh.osrsscript.map.area.tirannwn.lletya.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class LletyaNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.TOOL_LEPRECHAUN, new Tile(2348, 3159), Tile.Direction.NORTH));
    spawns.add(new NpcSpawn(NpcId.LILIWEN, new Tile(2344, 3164), 2));
    spawns.add(new NpcSpawn(NpcId.WIZARD_4399, new Tile(2353, 3159), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.GOREU, new Tile(2345, 3167), 4));
    spawns.add(new NpcSpawn(NpcId.YSGAWYN, new Tile(2353, 3180), 4));
    spawns.add(new NpcSpawn(NpcId.ARVEL, new Tile(2353, 3173), 4));
    spawns.add(new NpcSpawn(NpcId.MAWRTH, new Tile(2340, 3186), 4));
    spawns.add(new NpcSpawn(NpcId.KELYN, new Tile(2324, 3179), 4));
    spawns.add(new NpcSpawn(NpcId.GOREU, new Tile(2327, 3168), 4));
    spawns.add(new NpcSpawn(NpcId.YSGAWYN, new Tile(2333, 3160, 1), 4));
    spawns.add(new NpcSpawn(NpcId.ARVEL, new Tile(2336, 3184, 1), 4));
    spawns.add(new NpcSpawn(NpcId.MILES, new Tile(2356, 3175, 1), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.BANKER_1479, new Tile(2352, 3166)));
    spawns.add(new NpcSpawn(NpcId.BANKER_1480, new Tile(2354, 3166)));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_90, new Tile(2331, 3168), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_108, new Tile(2333, 3172), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_90, new Tile(2335, 3178), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_108, new Tile(2340, 3180), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_90, new Tile(2346, 3176), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_108, new Tile(2343, 3171), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_90, new Tile(2340, 3165), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_108, new Tile(2335, 3162), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_90, new Tile(2337, 3158), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_108, new Tile(2344, 3157), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_90, new Tile(2351, 3157), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_108, new Tile(2328, 3162), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_90, new Tile(2320, 3162), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_108, new Tile(2322, 3167), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_108, new Tile(2329, 3182), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_90, new Tile(2323, 3183), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_108, new Tile(2320, 3178), 4));
    spawns.add(new NpcSpawn(NpcId.TYRAS_GUARD_110_3433, new Tile(2320, 3191), 4));
    spawns.add(new NpcSpawn(NpcId.TYRAS_GUARD_110_3433, new Tile(2324, 3189), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_90, new Tile(2346, 3186), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_108, new Tile(2352, 3184), 4));
    spawns.add(new NpcSpawn(NpcId.TYRAS_GUARD_110_3433, new Tile(2350, 3172), 4));
    spawns.add(new NpcSpawn(NpcId.TYRAS_GUARD_110_3433, new Tile(2353, 3176), 4));
    spawns.add(new NpcSpawn(NpcId.TYRAS_GUARD_110_3433, new Tile(2343, 3175, 1), 4));
    spawns.add(new NpcSpawn(NpcId.TYRAS_GUARD_110_3433, new Tile(2335, 3177, 1), 4));
    spawns.add(new NpcSpawn(NpcId.TYRAS_GUARD_110_3433, new Tile(2343, 3169, 1), 4));
    spawns.add(new NpcSpawn(NpcId.TYRAS_GUARD_110_3433, new Tile(2337, 3167, 1), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_90, new Tile(2350, 3169, 1), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_108, new Tile(2350, 3174, 1), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_90, new Tile(2353, 3179, 1), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_108, new Tile(2352, 3163, 1), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_90, new Tile(2352, 3157, 1), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_108, new Tile(2347, 3156, 1), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_90, new Tile(2339, 3159, 1), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_108, new Tile(2351, 3187, 1), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_90, new Tile(2343, 3185, 1), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_108, new Tile(2335, 3180, 1), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_90, new Tile(2325, 3177, 1), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_90, new Tile(2326, 3174, 2), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_108, new Tile(2326, 3167, 2), 4));
    spawns.add(new NpcSpawn(NpcId.ELF_WARRIOR_108, new Tile(2325, 3165, 1), 4));
    spawns.add(new NpcSpawn(NpcId.ILFEEN, new Tile(2324, 3164), 4));

    return spawns;
  }
}
