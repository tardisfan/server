package com.palidinodh.osrsscript.map.area.asgarnia.mudskipperpoint.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class MudskipperPointNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.MOGRE_60, new Tile(2995, 3125)));
    spawns.add(new NpcSpawn(NpcId.MOGRE_60, new Tile(2999, 3122)));
    spawns.add(new NpcSpawn(NpcId.MOGRE_60, new Tile(3000, 3115)));
    spawns.add(new NpcSpawn(NpcId.MOGRE_60, new Tile(2996, 3109)));
    spawns.add(new NpcSpawn(NpcId.MOGRE_60, new Tile(2986, 3108)));
    spawns.add(new NpcSpawn(NpcId.MOGRE_60, new Tile(2986, 3116)));
    spawns.add(new NpcSpawn(NpcId.MOGRE_60, new Tile(2991, 3121)));

    return spawns;
  }
}
