package com.palidinodh.osrsscript.map.area.kandarin.catherby;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.reference.ReferenceIdSet;

@ReferenceId({ 11317, 11061 })
@ReferenceIdSet(primary = 11062, secondary = { 96, 97, 112, 113 })
@ReferenceIdSet(primary = 11318, secondary = { 0, 1, 16, 17 })
public class CatherbyArea extends Area {
}
