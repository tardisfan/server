package com.palidinodh.osrsscript.map.area.misthalin.edgeville.handler.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrsscript.player.plugin.slayer.SlayerPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.var;

@ReferenceId(NpcId.NIEVE)
class NieveNpc implements NpcHandler {
  @Override
  public void npcOption(Player player, int option, Npc npc) {
    var plugin = player.getPlugin(SlayerPlugin.class);
    if (option == 0) {
      plugin.openMasterMenuDialogue();
    } else if (option == 2) {
      plugin.getAssignment();
    } else if (option == 3) {
      player.openShop("slayer");
    } else if (option == 4) {
      plugin.openRewardsDialogue();
    }
  }
}
