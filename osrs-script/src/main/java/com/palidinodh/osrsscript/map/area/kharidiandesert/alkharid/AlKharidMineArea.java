package com.palidinodh.osrsscript.map.area.kharidiandesert.alkharid;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(13107)
public class AlKharidMineArea extends Area {
}
