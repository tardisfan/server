package com.palidinodh.osrsscript.map.area.misthalin.varrock;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 12597, 12852, 12853, 12854, 13108, 13109 })
public class VarrockArea extends Area {
}
