package com.palidinodh.osrsscript.map.area.feldiphills.hunterarea.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class MogreCaveNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.MOGRE_60, new Tile(2515, 9291), 4));
    spawns.add(new NpcSpawn(NpcId.MOGRE_60, new Tile(2514, 9297), 4));
    spawns.add(new NpcSpawn(NpcId.MOGRE_60, new Tile(2515, 9302), 4));
    spawns.add(new NpcSpawn(NpcId.MOGRE_60, new Tile(2521, 9290), 4));
    spawns.add(new NpcSpawn(NpcId.MOGRE_60, new Tile(2520, 9296), 4));
    spawns.add(new NpcSpawn(NpcId.MOGRE_60, new Tile(2521, 9302), 4));
    spawns.add(new NpcSpawn(NpcId.MOGRE_60, new Tile(2526, 9300), 4));
    spawns.add(new NpcSpawn(NpcId.MOGRE_60, new Tile(2525, 9291), 4));
    spawns.add(new NpcSpawn(NpcId.MOGRE_60, new Tile(2525, 9295), 4));

    return spawns;
  }
}
