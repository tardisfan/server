package com.palidinodh.osrsscript.map.area.kandarin.smokedevildungeon;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 9363, 9619 })
public class SmokeDevilDungeonArea extends Area {
}
