package com.palidinodh.osrsscript.map.area.karamja.shilovillage.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class ShiloVillageNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.WIZARD_4399, new Tile(2858, 2954), 2));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(2818, 2996), Tile.Direction.EAST));

    return spawns;
  }
}
