package com.palidinodh.osrsscript.map.area.kandarin.catherby.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class CatherbyNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.ELLENA, new Tile(2859, 3431), 2));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(2807, 3443)));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(2810, 3443)));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(2811, 3443)));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(2809, 3443)));
    spawns.add(new NpcSpawn(NpcId.ARHEIN, new Tile(2803, 3430), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.TRADER_CREWMEMBER_1331, new Tile(2797, 3415)));
    spawns.add(new NpcSpawn(NpcId.TRADER_CREWMEMBER_1334, new Tile(2799, 3414), 4));
    spawns.add(new NpcSpawn(NpcId.MAN_2_3078, new Tile(2804, 3425), 4));
    spawns.add(new NpcSpawn(NpcId.MAN_2_3078, new Tile(2805, 3433), 4));
    spawns.add(new NpcSpawn(NpcId.CANDLE_MAKER, new Tile(2799, 3439), 2));
    spawns.add(new NpcSpawn(NpcId.PERDU, new Tile(2808, 3454)));
    spawns.add(new NpcSpawn(NpcId.VANESSA, new Tile(2820, 3461), 2));
    spawns.add(new NpcSpawn(NpcId.CALEB, new Tile(2816, 3452), 4));
    spawns.add(new NpcSpawn(NpcId.WOMAN_2_3084, new Tile(2820, 3441), 4));
    spawns.add(new NpcSpawn(NpcId.HARRY, new Tile(2834, 3443), 2));
    spawns.add(new NpcSpawn(NpcId.TOOL_LEPRECHAUN, new Tile(2856, 3435)));
    spawns.add(new NpcSpawn(NpcId.TOOL_LEPRECHAUN, new Tile(2815, 3466), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.DANTAERA, new Tile(2807, 3463), 2));

    return spawns;
  }
}
