package com.palidinodh.osrsscript.map.area.kandarin.ancientcavern.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class AncientCavernNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.MITHRIL_DRAGON_304, new Tile(1779, 5339, 1), 4));
    spawns.add(new NpcSpawn(NpcId.MITHRIL_DRAGON_304, new Tile(1780, 5355, 1), 4));
    spawns.add(new NpcSpawn(NpcId.MITHRIL_DRAGON_304, new Tile(1761, 5337, 1), 4));
    spawns.add(new NpcSpawn(NpcId.MITHRIL_DRAGON_304, new Tile(1766, 5342, 1), 4));
    spawns.add(new NpcSpawn(NpcId.MITHRIL_DRAGON_304, new Tile(1767, 5332, 1), 4));
    spawns.add(new NpcSpawn(NpcId.MITHRIL_DRAGON_304, new Tile(1755, 5325, 1), 4));
    spawns.add(new NpcSpawn(NpcId.MITHRIL_DRAGON_304, new Tile(1782, 5328, 1), 4));
    spawns.add(new NpcSpawn(NpcId.BRUTAL_GREEN_DRAGON_227, new Tile(1774, 5359), 4));
    spawns.add(new NpcSpawn(NpcId.BRUTAL_GREEN_DRAGON_227, new Tile(1774, 5350), 4));
    spawns.add(new NpcSpawn(NpcId.BRUTAL_GREEN_DRAGON_227, new Tile(1771, 5336), 4));
    spawns.add(new NpcSpawn(NpcId.BRUTAL_GREEN_DRAGON_227, new Tile(1778, 5328), 4));
    spawns.add(new NpcSpawn(NpcId.BRUTAL_GREEN_DRAGON_227, new Tile(1764, 5325), 4));
    spawns.add(new NpcSpawn(NpcId.BRUTAL_GREEN_DRAGON_227, new Tile(1757, 5333), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(1747, 5362), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(1743, 5359), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(1739, 5356), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(1740, 5351), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(1737, 5348), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(1740, 5344), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(1737, 5342), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(1743, 5341), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(1747, 5342), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(1752, 5341), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(1751, 5338), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(1745, 5337), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(1751, 5351), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(1755, 5352), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(1757, 5356), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(1757, 5360), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(1760, 5356), 4));
    spawns.add(new NpcSpawn(NpcId.WATERFIEND_115, new Tile(1762, 5359), 4));

    return spawns;
  }
}
