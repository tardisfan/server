package com.palidinodh.osrsscript.map.area.zeah.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class PiscariliusHouseNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(1796, 3792)));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(1800, 3792)));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(1804, 3792)));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(1808, 3792)));
    spawns.add(new NpcSpawn(NpcId.TYNAN, new Tile(1840, 3786), 4));
    spawns.add(new NpcSpawn(NpcId.ROD_FISHING_SPOT_6825, new Tile(1826, 3770)));
    spawns.add(new NpcSpawn(NpcId.ROD_FISHING_SPOT_6825, new Tile(1827, 3770)));
    spawns.add(new NpcSpawn(NpcId.ROD_FISHING_SPOT_6825, new Tile(1829, 3768)));
    spawns.add(new NpcSpawn(NpcId.ROD_FISHING_SPOT_6825, new Tile(1832, 3768)));
    spawns.add(new NpcSpawn(NpcId.ROD_FISHING_SPOT_6825, new Tile(1834, 3769)));
    spawns.add(new NpcSpawn(NpcId.ROD_FISHING_SPOT_6825, new Tile(1835, 3770)));
    spawns.add(new NpcSpawn(NpcId.ROD_FISHING_SPOT_6825, new Tile(1840, 3777)));
    spawns.add(new NpcSpawn(NpcId.ROD_FISHING_SPOT_6825, new Tile(1840, 3776)));
    spawns.add(new NpcSpawn(NpcId.ROD_FISHING_SPOT_6825, new Tile(1840, 3775)));

    return spawns;
  }
}
