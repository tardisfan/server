package com.palidinodh.osrsscript.map.area.fremennikprovince.jatizso.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class JatizsoNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(2375, 3856), 1));
    spawns.add(new NpcSpawn(NpcId.WIZARD_4399, new Tile(2418, 3803)));
    spawns.add(new NpcSpawn(NpcId.MINER_1993, new Tile(2413, 3805), 4));
    spawns.add(new NpcSpawn(NpcId.LENSA, new Tile(2411, 3807), 4));
    spawns.add(new NpcSpawn(NpcId.THORKEL_SILKBEARD, new Tile(2409, 3803), 4));
    spawns.add(new NpcSpawn(NpcId.KING_GJUKI_SORVOTT_IV, new Tile(2407, 3802), 4));
    spawns.add(new NpcSpawn(NpcId.HRH_HRAFN, new Tile(2406, 3802), 4));
    spawns.add(new NpcSpawn(NpcId.ERIC_1997, new Tile(2405, 3808), 4));
    spawns.add(new NpcSpawn(NpcId.KEEPA_KETTILON, new Tile(2417, 3815), 4));
    spawns.add(new NpcSpawn(NpcId.FLOSI_DALKSSON, new Tile(2417, 3813), 4));
    spawns.add(new NpcSpawn(NpcId.BRENDT, new Tile(2409, 3814), 4));
    spawns.add(new NpcSpawn(NpcId.GRUNDT, new Tile(2406, 3815), 4));
    spawns.add(new NpcSpawn(NpcId.MINER_1993, new Tile(2414, 3820), 4));
    spawns.add(new NpcSpawn(NpcId.MAGNUS_GRAM, new Tile(2416, 3799), Tile.Direction.NORTH));
    spawns.add(new NpcSpawn(NpcId.RAUM_URDA_STEIN, new Tile(2396, 3797), 4));
    spawns.add(new NpcSpawn(NpcId.HRING_HRING, new Tile(2398, 3804), 4));
    spawns.add(new NpcSpawn(NpcId.SKULI_MYRKA, new Tile(2398, 3797), 4));
    spawns.add(new NpcSpawn(NpcId.MINER_1993, new Tile(2392, 3801), 4));

    return spawns;
  }
}
