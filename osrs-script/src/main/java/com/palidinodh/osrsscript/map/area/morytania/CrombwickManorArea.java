package com.palidinodh.osrsscript.map.area.morytania;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 14643, 14644, 14899, 14900, 14901 })
public class CrombwickManorArea extends Area {
}
