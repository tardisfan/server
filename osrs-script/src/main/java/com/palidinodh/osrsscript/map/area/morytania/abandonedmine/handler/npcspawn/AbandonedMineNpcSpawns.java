package com.palidinodh.osrsscript.map.area.morytania.abandonedmine.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class AbandonedMineNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.GIANT_BAT_27, new Tile(3434, 9637), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_BAT_27, new Tile(3428, 9638), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_BAT_27, new Tile(3427, 9634), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_BAT_27, new Tile(3426, 9631), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_BAT_27, new Tile(3423, 9637), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_BAT_27, new Tile(3427, 9645), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_BAT_27, new Tile(3429, 9648), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_BAT_27, new Tile(3425, 9652), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_BAT_27, new Tile(3414, 9636), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_BAT_27, new Tile(3410, 9641), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_BAT_27, new Tile(3418, 9629), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_BAT_27, new Tile(3412, 9621), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_BAT_27, new Tile(3421, 9622), 4));
    spawns.add(new NpcSpawn(NpcId.MINE_CART, new Tile(2727, 4491)));
    spawns.add(new NpcSpawn(NpcId.HAZE, new Tile(2798, 4571), 8));
    spawns.add(new NpcSpawn(NpcId.HAZE, new Tile(2793, 4576), 8));
    spawns.add(new NpcSpawn(NpcId.HAZE, new Tile(2807, 4570), 8));
    spawns.add(new NpcSpawn(NpcId.HAZE, new Tile(2784, 4488), 8));
    spawns.add(new NpcSpawn(NpcId.GIANT_BAT_27, new Tile(2791, 4498), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_BAT_27, new Tile(2788, 4501), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_BAT_27, new Tile(2791, 4504), 4));
    spawns.add(new NpcSpawn(NpcId.HAZE, new Tile(2785, 4569), 8));
    spawns.add(new NpcSpawn(NpcId.HAZE, new Tile(2785, 4580), 8));
    spawns.add(new NpcSpawn(NpcId.HAZE, new Tile(2793, 4584), 8));
    spawns.add(new NpcSpawn(NpcId.HAZE, new Tile(2800, 4593), 8));
    spawns.add(new NpcSpawn(NpcId.HAZE, new Tile(2808, 4599), 8));
    spawns.add(new NpcSpawn(NpcId.HAZE, new Tile(2801, 4578), 8));
    spawns.add(new NpcSpawn(NpcId.MINE_CART, new Tile(2739, 4531)));
    spawns.add(new NpcSpawn(NpcId.MINE_CART, new Tile(2795, 4462)));
    spawns.add(new NpcSpawn(NpcId.MINE_CART, new Tile(2791, 4452)));
    spawns.add(new NpcSpawn(NpcId.MINE_CART, new Tile(2785, 4447)));
    spawns.add(new NpcSpawn(NpcId.LOADING_CRANE, new Tile(2787, 4451)));
    spawns.add(new NpcSpawn(NpcId.LOADING_CRANE, new Tile(2787, 4446)));
    spawns.add(new NpcSpawn(NpcId.INNOCENT_LOOKING_KEY, new Tile(2788, 4455)));
    spawns.add(new NpcSpawn(NpcId.LOADING_CRANE, new Tile(2782, 4458)));
    spawns.add(new NpcSpawn(NpcId.LOADING_CRANE, new Tile(2793, 4458)));

    return spawns;
  }
}
