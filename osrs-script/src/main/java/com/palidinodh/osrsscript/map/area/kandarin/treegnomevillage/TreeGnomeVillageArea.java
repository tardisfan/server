package com.palidinodh.osrsscript.map.area.kandarin.treegnomevillage;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(10033)
public class TreeGnomeVillageArea extends Area {
}
