package com.palidinodh.osrsscript.map.area.kharidiandesert;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(12591)
public class BedabinCampArea extends Area {
}
