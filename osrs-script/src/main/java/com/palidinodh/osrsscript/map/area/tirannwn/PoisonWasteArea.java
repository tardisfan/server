package com.palidinodh.osrsscript.map.area.tirannwn;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 8495, 8752, 9007, 9008 })
public class PoisonWasteArea extends Area {
}
