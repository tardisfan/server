package com.palidinodh.osrsscript.map.area.karamja.brimhaven;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 10801, 10802, 11057, 11058 })
public class BrimhavenArea extends Area {
}
