package com.palidinodh.osrsscript.map.area.karamja;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 11053, 11309, 11565, 11821 })
public class KharaziJungleArea extends Area {
}
