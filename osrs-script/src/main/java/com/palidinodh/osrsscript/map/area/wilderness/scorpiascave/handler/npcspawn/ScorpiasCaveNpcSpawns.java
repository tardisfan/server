package com.palidinodh.osrsscript.map.area.wilderness.scorpiascave.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class ScorpiasCaveNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3223, 10347), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3225, 10348), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3223, 10344), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3224, 10339), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3221, 10335), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3225, 10333), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3229, 10333), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3229, 10337), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3228, 10342), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3229, 10347), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3232, 10349), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3236, 10349), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3233, 10344), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3234, 10338), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3232, 10334), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3236, 10335), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3240, 10334), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3244, 10335), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3240, 10337), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3243, 10344), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3243, 10348), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3238, 10346), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3238, 10341), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIAS_OFFSPRING_15, new Tile(3243, 10339), 4));
    spawns.add(new NpcSpawn(NpcId.SCORPIA_225, new Tile(3231, 10340), 8));

    return spawns;
  }
}
