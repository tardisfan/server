package com.palidinodh.osrsscript.map.area.morytania.canifis.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class CanifisHunterNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.SWAMP_LIZARD, new Tile(3539, 3450), 4));
    spawns.add(new NpcSpawn(NpcId.SWAMP_LIZARD, new Tile(3535, 3448), 4));
    spawns.add(new NpcSpawn(NpcId.SWAMP_LIZARD, new Tile(3530, 3444), 4));
    spawns.add(new NpcSpawn(NpcId.SWAMP_LIZARD, new Tile(3538, 3448), 4));
    spawns.add(new NpcSpawn(NpcId.SWAMP_LIZARD, new Tile(3535, 3446), 4));
    spawns.add(new NpcSpawn(NpcId.SWAMP_LIZARD, new Tile(3549, 3451), 4));
    spawns.add(new NpcSpawn(NpcId.SWAMP_LIZARD, new Tile(3545, 3450), 8));
    spawns.add(new NpcSpawn(NpcId.SWAMP_LIZARD, new Tile(3556, 3453), 4));
    spawns.add(new NpcSpawn(NpcId.SWAMP_LIZARD, new Tile(3555, 3451), 4));
    spawns.add(new NpcSpawn(NpcId.SWAMP_LIZARD, new Tile(3550, 3452), 4));
    spawns.add(new NpcSpawn(NpcId.SWAMP_LIZARD, new Tile(3548, 3449), 4));
    spawns.add(new NpcSpawn(NpcId.SWAMP_LIZARD, new Tile(3561, 3437), 4));
    spawns.add(new NpcSpawn(NpcId.SWAMP_LIZARD, new Tile(3558, 3434), 4));
    spawns.add(new NpcSpawn(NpcId.SWAMP_LIZARD, new Tile(3561, 3431), 4));
    spawns.add(new NpcSpawn(NpcId.SWAMP_LIZARD, new Tile(3554, 3437), 4));
    spawns.add(new NpcSpawn(NpcId.SWAMP_LIZARD, new Tile(3551, 3440), 4));
    spawns.add(new NpcSpawn(NpcId.SWAMP_LIZARD, new Tile(3550, 3438), 4));
    spawns.add(new NpcSpawn(NpcId.SWAMP_LIZARD, new Tile(3546, 3439), 4));
    spawns.add(new NpcSpawn(NpcId.SWAMP_LIZARD, new Tile(3548, 3439), 4));
    spawns.add(new NpcSpawn(NpcId.SWAMP_LIZARD, new Tile(3554, 3438), 4));

    return spawns;
  }
}
