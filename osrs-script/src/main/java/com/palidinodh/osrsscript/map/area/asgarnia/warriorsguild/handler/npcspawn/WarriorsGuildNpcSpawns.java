package com.palidinodh.osrsscript.map.area.asgarnia.warriorsguild.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class WarriorsGuildNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.HARRALLAK_MENAROUS, new Tile(2869, 3544), 8));
    spawns.add(new NpcSpawn(NpcId.GHOMMAL, new Tile(2879, 3544), 2));
    spawns.add(new NpcSpawn(NpcId.SHANOMI, new Tile(2857, 3543), 2));
    spawns.add(new NpcSpawn(NpcId.JADE, new Tile(2841, 3543), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.LILLY, new Tile(2846, 3551), 2));
    spawns.add(new NpcSpawn(NpcId.LIDIO, new Tile(2840, 3551), 2));
    spawns.add(new NpcSpawn(NpcId.ANTON, new Tile(2856, 3536, 1), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_56_2463, new Tile(2840, 3544, 2), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_56_2463, new Tile(2839, 3548, 2), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_56_2463, new Tile(2842, 3552, 2), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_56_2463, new Tile(2845, 3546, 2), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_56_2463, new Tile(2848, 3551, 2), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_56_2463, new Tile(2850, 3542, 2), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_56_2463, new Tile(2850, 3536, 2), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_56_2463, new Tile(2856, 3537, 2), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_56_2463, new Tile(2861, 3539, 2), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_56_2463, new Tile(2856, 3543, 2), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_56_2463, new Tile(2853, 3548, 2), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_56_2463, new Tile(2858, 3552, 2), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_56_2463, new Tile(2862, 3546, 2), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_56_2463, new Tile(2866, 3541, 2), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_56_2463, new Tile(2871, 3539, 2), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_56_2463, new Tile(2872, 3545, 2), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_56_2463, new Tile(2868, 3548, 2), 3));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_56_2463, new Tile(2872, 3552, 2), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_56_2463, new Tile(2863, 3551, 2), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_106, new Tile(2906, 9962), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_106, new Tile(2908, 9959), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_106, new Tile(2913, 9963), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_106, new Tile(2915, 9958), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_106, new Tile(2919, 9962), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_106, new Tile(2923, 9958), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_106, new Tile(2916, 9966), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_106, new Tile(2914, 9971), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_106, new Tile(2920, 9970), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_106, new Tile(2922, 9966), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_106, new Tile(2927, 9962), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_106, new Tile(2929, 9958), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_106, new Tile(2927, 9968), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_106, new Tile(2932, 9970), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_106, new Tile(2932, 9965), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_106, new Tile(2933, 9960), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_106, new Tile(2937, 9958), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_106, new Tile(2937, 9964), 4));
    spawns.add(new NpcSpawn(NpcId.CYCLOPS_106, new Tile(2936, 9969), 4));

    return spawns;
  }
}
