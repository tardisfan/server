package com.palidinodh.osrsscript.map.area.wilderness.deepwildernessdungeon;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(12193)
public class DeepWildernessDungeonArea extends Area {
  @Override
  public boolean inWilderness() {
    return true;
  }

  @Override
  public int getWildernessLevel() {
    return (getTile().getY() - 9920) / 8 + 1;
  }
}
