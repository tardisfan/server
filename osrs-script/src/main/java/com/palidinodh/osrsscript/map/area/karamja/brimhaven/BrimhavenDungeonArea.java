package com.palidinodh.osrsscript.map.area.karamja.brimhaven;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 10643, 10644, 10645, 10899, 10900, 10901 })
public class BrimhavenDungeonArea extends Area {
}
