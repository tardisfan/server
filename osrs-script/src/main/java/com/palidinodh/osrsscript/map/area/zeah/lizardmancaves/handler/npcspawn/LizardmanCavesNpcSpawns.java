package com.palidinodh.osrsscript.map.area.zeah.lizardmancaves.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class LizardmanCavesNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.LIZARDMAN_SHAMAN_150, new Tile(1286, 9952), 4));
    spawns.add(new NpcSpawn(NpcId.LIZARDMAN_SHAMAN_150, new Tile(1292, 9958), 4));
    spawns.add(new NpcSpawn(NpcId.LIZARDMAN_SHAMAN_150, new Tile(1302, 9945), 4));
    spawns.add(new NpcSpawn(NpcId.LIZARDMAN_SHAMAN_150, new Tile(1308, 9950), 4));
    spawns.add(new NpcSpawn(NpcId.LIZARDMAN_SHAMAN_150, new Tile(1318, 9947), 4));
    spawns.add(new NpcSpawn(NpcId.LIZARDMAN_SHAMAN_150, new Tile(1324, 9953), 4));
    spawns.add(new NpcSpawn(NpcId.LIZARDMAN_SHAMAN_150, new Tile(1324, 9963), 4));
    spawns.add(new NpcSpawn(NpcId.LIZARDMAN_SHAMAN_150, new Tile(1331, 9969), 4));

    return spawns;
  }
}
