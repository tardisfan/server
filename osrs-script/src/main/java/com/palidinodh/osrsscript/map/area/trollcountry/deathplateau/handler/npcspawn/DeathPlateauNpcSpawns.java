package com.palidinodh.osrsscript.map.area.trollcountry.deathplateau.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class DeathPlateauNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.MOUNTAIN_TROLL_69, new Tile(2856, 3596), 4));
    spawns.add(new NpcSpawn(NpcId.MOUNTAIN_TROLL_69, new Tile(2861, 3597), 4));
    spawns.add(new NpcSpawn(NpcId.MOUNTAIN_TROLL_69, new Tile(2869, 3596), 4));
    spawns.add(new NpcSpawn(NpcId.MOUNTAIN_TROLL_69, new Tile(2875, 3593), 4));
    spawns.add(new NpcSpawn(NpcId.MOUNTAIN_TROLL_69, new Tile(2880, 3587), 4));
    spawns.add(new NpcSpawn(NpcId.MOUNTAIN_TROLL_69, new Tile(2870, 3587), 4));
    spawns.add(new NpcSpawn(NpcId.MOUNTAIN_TROLL_69, new Tile(2866, 3591), 4));
    spawns.add(new NpcSpawn(NpcId.MOUNTAIN_TROLL_69, new Tile(2859, 3592), 4));
    spawns.add(new NpcSpawn(NpcId.MOUNTAIN_TROLL_69, new Tile(2861, 3588), 4));
    spawns.add(new NpcSpawn(NpcId.MOUNTAIN_TROLL_69, new Tile(2875, 3588), 4));
    spawns.add(new NpcSpawn(NpcId.MOUNTAIN_TROLL_69, new Tile(2874, 3598), 4));
    spawns.add(new NpcSpawn(NpcId.MOUNTAIN_TROLL_69_937, new Tile(2865, 3595), 4));
    spawns.add(new NpcSpawn(NpcId.MOUNTAIN_TROLL_69_937, new Tile(2855, 3591), 4));
    spawns.add(new NpcSpawn(NpcId.MOUNTAIN_TROLL_69_937, new Tile(2866, 3587), 4));
    spawns.add(new NpcSpawn(NpcId.MOUNTAIN_TROLL_69_937, new Tile(2871, 3592), 4));
    spawns.add(new NpcSpawn(NpcId.MOUNTAIN_TROLL_69_937, new Tile(2853, 3586), 4));
    spawns.add(new NpcSpawn(NpcId.MOUNTAIN_TROLL_69_937, new Tile(2847, 3591), 4));
    spawns.add(new NpcSpawn(NpcId.MOUNTAIN_TROLL_69_937, new Tile(2881, 3591), 4));

    return spawns;
  }
}
