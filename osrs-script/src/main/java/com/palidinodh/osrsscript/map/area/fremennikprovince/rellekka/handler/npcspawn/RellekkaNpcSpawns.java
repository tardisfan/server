package com.palidinodh.osrsscript.map.area.fremennikprovince.rellekka.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class RellekkaNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.HUNTING_EXPERT_1504, new Tile(2644, 3662)));
    spawns.add(new NpcSpawn(NpcId.WIZARD_4399, new Tile(2648, 3657), Tile.Direction.NORTH));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(2649, 3661), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(2621, 3658), Tile.Direction.NORTH));
    spawns.add(new NpcSpawn(NpcId.ROCK_CRAB_13, new Tile(2669, 3725), 4));
    spawns.add(new NpcSpawn(NpcId.ROCK_CRAB_13, new Tile(2672, 3728), 4));
    spawns.add(new NpcSpawn(NpcId.ROCK_CRAB_13, new Tile(2676, 3730), 4));
    spawns.add(new NpcSpawn(NpcId.ROCK_CRAB_13, new Tile(2681, 3731), 4));
    spawns.add(new NpcSpawn(NpcId.ROCK_CRAB_13, new Tile(2685, 3729), 4));
    spawns.add(new NpcSpawn(NpcId.ROCK_CRAB_13, new Tile(2681, 3725), 4));
    spawns.add(new NpcSpawn(NpcId.ROCK_CRAB_13, new Tile(2686, 3723), 4));
    spawns.add(new NpcSpawn(NpcId.ROCK_CRAB_13, new Tile(2686, 3719), 4));
    spawns.add(new NpcSpawn(NpcId.ROCK_CRAB_13, new Tile(2686, 3714), 4));
    spawns.add(new NpcSpawn(NpcId.ROCK_CRAB_13, new Tile(2679, 3714), 4));
    spawns.add(new NpcSpawn(NpcId.ROCK_CRAB_13, new Tile(2679, 3720), 4));
    spawns.add(new NpcSpawn(NpcId.ROCK_CRAB_13, new Tile(2673, 3718), 4));
    spawns.add(new NpcSpawn(NpcId.ROCK_CRAB_13, new Tile(2674, 3712), 4));
    spawns.add(new NpcSpawn(NpcId.ROCK_CRAB_13, new Tile(2667, 3711), 4));
    spawns.add(new NpcSpawn(NpcId.ROCK_CRAB_13, new Tile(2665, 3716), 4));
    spawns.add(new NpcSpawn(NpcId.TOWN_GUARD, new Tile(2660, 3646)));
    spawns.add(new NpcSpawn(NpcId.TOWN_GUARD, new Tile(2664, 3646), 3));
    spawns.add(new NpcSpawn(NpcId.SIGLI_THE_HUNTSMAN, new Tile(2661, 3653), 4));
    spawns.add(new NpcSpawn(NpcId.PONTAK_48, new Tile(2668, 3652), 4));
    spawns.add(new NpcSpawn(NpcId.LENSA_48, new Tile(2655, 3651), 2));
    spawns.add(new NpcSpawn(NpcId.GUARD_3928, new Tile(2657, 3663)));
    spawns.add(new NpcSpawn(NpcId.GUARD_3928, new Tile(2660, 3663)));
    spawns.add(new NpcSpawn(NpcId.ASKELADDEN, new Tile(2658, 3660), 4));
    spawns.add(new NpcSpawn(NpcId.BRUNDT_THE_CHIEFTAIN, new Tile(2659, 3669), 4));
    spawns.add(new NpcSpawn(NpcId.BJORN, new Tile(2657, 3673), 4));
    spawns.add(new NpcSpawn(NpcId.MANNI_THE_REVELLER, new Tile(2660, 3673), 4));
    spawns.add(new NpcSpawn(NpcId.THORA_THE_BARKEEP, new Tile(2662, 3672), 2));
    spawns.add(new NpcSpawn(NpcId.ELDGRIM, new Tile(2659, 3677), 4));
    spawns.add(new NpcSpawn(NpcId.STYRMIR, new Tile(2657, 3680)));
    spawns.add(new NpcSpawn(NpcId.OSPAK, new Tile(2660, 3680)));
    spawns.add(new NpcSpawn(NpcId.TORBRUND, new Tile(2658, 3679)));
    spawns.add(new NpcSpawn(NpcId.FRIDGEIR, new Tile(2659, 3678)));
    spawns.add(new NpcSpawn(NpcId.SWENSEN_THE_NAVIGATOR, new Tile(2647, 3659), 4));
    spawns.add(new NpcSpawn(NpcId.JENNELLA_48, new Tile(2642, 3651), 4));
    spawns.add(new NpcSpawn(NpcId.SASSILIK_48, new Tile(2629, 3653), 4));
    spawns.add(new NpcSpawn(NpcId.PEER_THE_SEER_8147, new Tile(2634, 3669), 4));
    spawns.add(new NpcSpawn(NpcId.YRSA, new Tile(2624, 3673), 4));
    spawns.add(new NpcSpawn(NpcId.AGNAR, new Tile(2643, 3675), 8));
    spawns.add(new NpcSpawn(NpcId.FISH_MONGER, new Tile(2645, 3674), 4));
    spawns.add(new NpcSpawn(NpcId.FUR_TRADER_3948, new Tile(2641, 3678), 4));
    spawns.add(new NpcSpawn(NpcId.SIGMUND_THE_MERCHANT, new Tile(2644, 3680), 4));
    spawns.add(new NpcSpawn(NpcId.TORFINN, new Tile(2640, 3697), 4));
    spawns.add(new NpcSpawn(NpcId.FISHERMAN, new Tile(2641, 3699), Tile.Direction.NORTH));
    spawns.add(new NpcSpawn(NpcId.SAILOR_3936, new Tile(2630, 3692), 4));
    spawns.add(new NpcSpawn(NpcId.LOKAR_SEARUNNER, new Tile(2621, 3688)));
    spawns.add(new NpcSpawn(NpcId.JARVALD_7205, new Tile(2620, 3684)));
    spawns.add(new NpcSpawn(NpcId.VOLF_OLAFSON, new Tile(2658, 3692), 4));
    spawns.add(new NpcSpawn(NpcId.SKULGRIMEN, new Tile(2664, 3694), 4));
    spawns.add(new NpcSpawn(NpcId.THORVALD_THE_WARRIOR, new Tile(2665, 3691), 4));
    spawns.add(new NpcSpawn(NpcId.DRON, new Tile(2660, 3701), 4));
    spawns.add(new NpcSpawn(NpcId.FREYGERD_48, new Tile(2669, 3702), 4));
    spawns.add(new NpcSpawn(NpcId.REESO, new Tile(2663, 3708), 4));
    spawns.add(new NpcSpawn(NpcId.MORD_GUNNARS, new Tile(2644, 3709), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.MARIA_GUNNARS_1883, new Tile(2644, 3710), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.BORROKAR_48, new Tile(2680, 3690), 4));
    spawns.add(new NpcSpawn(NpcId.LONGHALL_BOUNCER, new Tile(2669, 3684), 4));
    spawns.add(new NpcSpawn(NpcId.OLAF_THE_BARD, new Tile(2673, 3683), 4));
    spawns.add(new NpcSpawn(NpcId.FREIDIR_48, new Tile(2675, 3675), 4));
    spawns.add(new NpcSpawn(NpcId.INGA_48, new Tile(2676, 3677), 4));
    spawns.add(new NpcSpawn(NpcId.BLANIN, new Tile(2673, 3670), 4));
    spawns.add(new NpcSpawn(NpcId.LANZIG_48, new Tile(2675, 3665), 4));
    spawns.add(new NpcSpawn(NpcId.INGRID_HRADSON, new Tile(2677, 3666), 4));

    return spawns;
  }
}
