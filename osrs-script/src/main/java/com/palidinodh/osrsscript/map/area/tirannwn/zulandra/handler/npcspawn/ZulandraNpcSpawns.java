package com.palidinodh.osrsscript.map.area.tirannwn.zulandra.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class ZulandraNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.ZUL_ANIEL, new Tile(2196, 3057), 4));
    spawns.add(new NpcSpawn(NpcId.ZUL_ARETH, new Tile(2197, 3061), 4));
    spawns.add(new NpcSpawn(NpcId.ZUL_URGISH, new Tile(2200, 3054)));
    spawns.add(new NpcSpawn(NpcId.HIGH_PRIESTESS_ZUL_HARCINQA, new Tile(2192, 3055)));
    spawns.add(new NpcSpawn(NpcId.PRIESTESS_ZUL_GWENWYNIG_2033, new Tile(2211, 3056), 4));

    return spawns;
  }
}
