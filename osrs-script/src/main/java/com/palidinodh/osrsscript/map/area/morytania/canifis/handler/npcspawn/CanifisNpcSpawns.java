package com.palidinodh.osrsscript.map.area.morytania.canifis.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class CanifisNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.HUNTING_EXPERT_1504, new Tile(3508, 3479), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.WIZARD_4399, new Tile(3509, 3483)));
    spawns.add(new NpcSpawn(NpcId.YADVIGA_24, new Tile(3481, 3489), 4));
    spawns.add(new NpcSpawn(NpcId.IRINA_24, new Tile(3483, 3493), 4));
    spawns.add(new NpcSpawn(NpcId.SOFIYA_24, new Tile(3482, 3497), 4));
    spawns.add(new NpcSpawn(NpcId.BORIS_24, new Tile(3490, 3493), 4));
    spawns.add(new NpcSpawn(NpcId.YURI_24, new Tile(3496, 3493), 4));
    spawns.add(new NpcSpawn(NpcId.SVETLANA_24, new Tile(3500, 3491), 4));
    spawns.add(new NpcSpawn(NpcId.ZOJA_24, new Tile(3503, 3487), 4));
    spawns.add(new NpcSpawn(NpcId.LEV_24, new Tile(3506, 3483), 4));
    spawns.add(new NpcSpawn(NpcId.SHOP_KEEPER, new Tile(3477, 3496), 4));
    spawns.add(new NpcSpawn(NpcId.SBOTT, new Tile(3491, 3501), 4));
    spawns.add(new NpcSpawn(NpcId.RUFUS, new Tile(3507, 3494), 4));
    spawns.add(new NpcSpawn(NpcId.MILLA_24, new Tile(3499, 3482), 4));
    spawns.add(new NpcSpawn(NpcId.NIKOLAI_24, new Tile(3493, 3483), 4));
    spawns.add(new NpcSpawn(NpcId.GEORGY_24, new Tile(3478, 3483), 4));
    spawns.add(new NpcSpawn(NpcId.TAXIDERMIST, new Tile(3479, 3485), 4));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(3514, 3478), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(3514, 3479), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(3514, 3481), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(3514, 3480), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(3514, 3482), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(3514, 3483), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.ROAVAR, new Tile(3494, 3469), 4));
    spawns.add(new NpcSpawn(NpcId.VERA_24, new Tile(3490, 3472), 4));
    spawns.add(new NpcSpawn(NpcId.IMRE_24, new Tile(3491, 3477), 4));
    spawns.add(new NpcSpawn(NpcId.MALAK, new Tile(3496, 3477), 4));
    spawns.add(new NpcSpawn(NpcId.STRANGER, new Tile(3503, 3477)));

    return spawns;
  }
}
