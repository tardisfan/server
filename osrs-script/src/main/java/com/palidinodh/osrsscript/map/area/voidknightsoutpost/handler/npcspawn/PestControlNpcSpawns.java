package com.palidinodh.osrsscript.map.area.voidknightsoutpost.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class PestControlNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.SQUIRE_1769, new Tile(2656, 2614)));
    spawns.add(new NpcSpawn(NpcId.VOID_KNIGHT_2950, new Tile(2656, 2592)));
    spawns.add(new NpcSpawn(NpcId.PORTAL, new Tile(2628, 2591)));
    spawns.add(new NpcSpawn(NpcId.PORTAL_1740, new Tile(2680, 2588)));
    spawns.add(new NpcSpawn(NpcId.PORTAL_1742, new Tile(2645, 2569)));
    spawns.add(new NpcSpawn(NpcId.PORTAL_1741, new Tile(2669, 2570)));
    spawns.add(new NpcSpawn(NpcId.SPINNER_55, new Tile(2629, 2595), 4));
    spawns.add(new NpcSpawn(NpcId.SPINNER_55, new Tile(2643, 2570), 4));
    spawns.add(new NpcSpawn(NpcId.SPINNER_55, new Tile(2670, 2568), 4));
    spawns.add(new NpcSpawn(NpcId.SPINNER_55, new Tile(2684, 2589), 4));
    spawns.add(new NpcSpawn(NpcId.BRAWLER_51, new Tile(2632, 2594), 32));
    spawns.add(new NpcSpawn(NpcId.BRAWLER_51, new Tile(2632, 2594), 32));
    spawns.add(new NpcSpawn(NpcId.DEFILER_33, new Tile(2632, 2593), 32));
    spawns.add(new NpcSpawn(NpcId.DEFILER_33, new Tile(2632, 2593), 32));
    spawns.add(new NpcSpawn(NpcId.TORCHER_33, new Tile(2632, 2592), 32));
    spawns.add(new NpcSpawn(NpcId.TORCHER_33, new Tile(2632, 2592), 32));
    spawns.add(new NpcSpawn(NpcId.RAVAGER_36, new Tile(2632, 2591), 32));
    spawns.add(new NpcSpawn(NpcId.RAVAGER_36, new Tile(2632, 2591), 32));
    spawns.add(new NpcSpawn(NpcId.RAVAGER_36, new Tile(2632, 2591), 32));
    spawns.add(new NpcSpawn(NpcId.RAVAGER_36, new Tile(2632, 2591), 32));
    spawns.add(new NpcSpawn(NpcId.SHIFTER_38, new Tile(2632, 2590), 32));
    spawns.add(new NpcSpawn(NpcId.SHIFTER_38, new Tile(2632, 2590), 32));
    spawns.add(new NpcSpawn(NpcId.BRAWLER_51, new Tile(2648, 2573), 32));
    spawns.add(new NpcSpawn(NpcId.BRAWLER_51, new Tile(2648, 2573), 32));
    spawns.add(new NpcSpawn(NpcId.DEFILER_33, new Tile(2647, 2573), 32));
    spawns.add(new NpcSpawn(NpcId.DEFILER_33, new Tile(2647, 2573), 32));
    spawns.add(new NpcSpawn(NpcId.TORCHER_33, new Tile(2646, 2573), 32));
    spawns.add(new NpcSpawn(NpcId.TORCHER_33, new Tile(2646, 2573), 32));
    spawns.add(new NpcSpawn(NpcId.RAVAGER_36, new Tile(2645, 2573), 32));
    spawns.add(new NpcSpawn(NpcId.RAVAGER_36, new Tile(2645, 2573), 32));
    spawns.add(new NpcSpawn(NpcId.RAVAGER_36, new Tile(2645, 2573), 32));
    spawns.add(new NpcSpawn(NpcId.RAVAGER_36, new Tile(2645, 2573), 32));
    spawns.add(new NpcSpawn(NpcId.SHIFTER_38, new Tile(2644, 2573), 32));
    spawns.add(new NpcSpawn(NpcId.SHIFTER_38, new Tile(2644, 2573), 32));
    spawns.add(new NpcSpawn(NpcId.BRAWLER_51, new Tile(2672, 2574), 32));
    spawns.add(new NpcSpawn(NpcId.BRAWLER_51, new Tile(2672, 2574), 32));
    spawns.add(new NpcSpawn(NpcId.DEFILER_33, new Tile(2671, 2574), 32));
    spawns.add(new NpcSpawn(NpcId.DEFILER_33, new Tile(2671, 2574), 32));
    spawns.add(new NpcSpawn(NpcId.TORCHER_33, new Tile(2670, 2574), 32));
    spawns.add(new NpcSpawn(NpcId.TORCHER_33, new Tile(2670, 2574), 32));
    spawns.add(new NpcSpawn(NpcId.RAVAGER_36, new Tile(2669, 2574), 32));
    spawns.add(new NpcSpawn(NpcId.RAVAGER_36, new Tile(2669, 2574), 32));
    spawns.add(new NpcSpawn(NpcId.RAVAGER_36, new Tile(2669, 2574), 32));
    spawns.add(new NpcSpawn(NpcId.RAVAGER_36, new Tile(2669, 2574), 32));
    spawns.add(new NpcSpawn(NpcId.SHIFTER_38, new Tile(2668, 2574), 32));
    spawns.add(new NpcSpawn(NpcId.SHIFTER_38, new Tile(2668, 2574), 32));
    spawns.add(new NpcSpawn(NpcId.BRAWLER_51, new Tile(2677, 2591), 32));
    spawns.add(new NpcSpawn(NpcId.BRAWLER_51, new Tile(2677, 2591), 32));
    spawns.add(new NpcSpawn(NpcId.DEFILER_33, new Tile(2678, 2590), 32));
    spawns.add(new NpcSpawn(NpcId.DEFILER_33, new Tile(2678, 2590), 32));
    spawns.add(new NpcSpawn(NpcId.TORCHER_33, new Tile(2678, 2589), 32));
    spawns.add(new NpcSpawn(NpcId.TORCHER_33, new Tile(2678, 2589), 32));
    spawns.add(new NpcSpawn(NpcId.RAVAGER_36, new Tile(2678, 2588), 32));
    spawns.add(new NpcSpawn(NpcId.RAVAGER_36, new Tile(2678, 2588), 32));
    spawns.add(new NpcSpawn(NpcId.RAVAGER_36, new Tile(2678, 2588), 32));
    spawns.add(new NpcSpawn(NpcId.RAVAGER_36, new Tile(2678, 2588), 32));
    spawns.add(new NpcSpawn(NpcId.SHIFTER_38, new Tile(2678, 2587), 32));
    spawns.add(new NpcSpawn(NpcId.SHIFTER_38, new Tile(2678, 2587), 32));

    return spawns;
  }
}
