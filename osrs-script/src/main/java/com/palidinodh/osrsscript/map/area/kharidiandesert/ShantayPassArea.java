package com.palidinodh.osrsscript.map.area.kharidiandesert;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(13104)
public class ShantayPassArea extends Area {
}
