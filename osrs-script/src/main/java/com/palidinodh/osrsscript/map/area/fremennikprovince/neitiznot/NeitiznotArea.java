package com.palidinodh.osrsscript.map.area.fremennikprovince.neitiznot;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 9275, 9276 })
public class NeitiznotArea extends Area {
}
