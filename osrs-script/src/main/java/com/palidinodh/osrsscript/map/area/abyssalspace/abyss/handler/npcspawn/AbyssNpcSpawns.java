package com.palidinodh.osrsscript.map.area.abyssalspace.abyss.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class AbyssNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.ABYSSAL_LEECH_41, new Tile(3033, 4853), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_LEECH_41, new Tile(3044, 4853), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_LEECH_41, new Tile(3054, 4850), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_LEECH_41, new Tile(3061, 4841), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_LEECH_41, new Tile(3062, 4826), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_LEECH_41, new Tile(3059, 4814), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_LEECH_41, new Tile(3044, 4810), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_LEECH_41, new Tile(3027, 4811), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_LEECH_41, new Tile(3017, 4821), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_LEECH_41, new Tile(3016, 4835), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_LEECH_41, new Tile(3020, 4847), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_GUARDIAN_59, new Tile(3025, 4851), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_GUARDIAN_59, new Tile(3039, 4855), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_GUARDIAN_59, new Tile(3049, 4852), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_GUARDIAN_59, new Tile(3059, 4846), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_GUARDIAN_59, new Tile(3062, 4832), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_GUARDIAN_59, new Tile(3060, 4819), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_GUARDIAN_59, new Tile(3051, 4812), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_GUARDIAN_59, new Tile(3036, 4810), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_GUARDIAN_59, new Tile(3021, 4814), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_GUARDIAN_59, new Tile(3015, 4829), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_GUARDIAN_59, new Tile(3017, 4840), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3016, 4824), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3017, 4810), 5));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3039, 4807), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3057, 4811), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3059, 4824), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3064, 4840), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3053, 4855), 5));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3042, 4855), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3035, 4855), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_LEECH_41, new Tile(3059, 4850), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_LEECH_41, new Tile(3060, 4836), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_LEECH_41, new Tile(3064, 4822), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_LEECH_41, new Tile(3054, 4814), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_LEECH_41, new Tile(3031, 4808), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_LEECH_41, new Tile(3017, 4816), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_LEECH_41, new Tile(3013, 4838), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_LEECH_41, new Tile(3015, 4848), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_LEECH_41, new Tile(3018, 4843), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_LEECH_41, new Tile(3029, 4854), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_GUARDIAN_59, new Tile(3047, 4809), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_GUARDIAN_59, new Tile(3059, 4817), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_GUARDIAN_59, new Tile(3062, 4828), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_GUARDIAN_59, new Tile(3060, 4840), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_GUARDIAN_59, new Tile(3053, 4850), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_GUARDIAN_59, new Tile(3027, 4851), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_GUARDIAN_59, new Tile(3020, 4843), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_GUARDIAN_59, new Tile(3019, 4819), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_GUARDIAN_59, new Tile(3026, 4807), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3033, 4810), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3046, 4811), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3054, 4810), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3061, 4810), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3061, 4823), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3062, 4836), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3056, 4848), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3049, 4855), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3030, 4856), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3022, 4850), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3016, 4838), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3016, 4832), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3017, 4813), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3024, 4812), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3030, 4810), 4));
    spawns.add(new NpcSpawn(NpcId.ABYSSAL_WALKER_81, new Tile(3041, 4810), 4));
    spawns.add(new NpcSpawn(NpcId.MAGE_OF_ZAMORAK_7425, new Tile(3039, 4835), 4));

    return spawns;
  }
}
