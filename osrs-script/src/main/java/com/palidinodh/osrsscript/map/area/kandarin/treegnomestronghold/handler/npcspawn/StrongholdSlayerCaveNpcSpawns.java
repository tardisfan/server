package com.palidinodh.osrsscript.map.area.kandarin.treegnomestronghold.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class StrongholdSlayerCaveNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.HELLHOUND_122, new Tile(2408, 9801), 4));
    spawns.add(new NpcSpawn(NpcId.HELLHOUND_122, new Tile(2410, 9795), 4));
    spawns.add(new NpcSpawn(NpcId.HELLHOUND_122, new Tile(2417, 9796), 4));
    spawns.add(new NpcSpawn(NpcId.HELLHOUND_122, new Tile(2417, 9803), 4));
    spawns.add(new NpcSpawn(NpcId.HELLHOUND_122, new Tile(2413, 9802), 4));
    spawns.add(new NpcSpawn(NpcId.HELLHOUND_122, new Tile(2414, 9797), 4));
    spawns.add(new NpcSpawn(NpcId.HELLHOUND_122, new Tile(2409, 9786), 4));
    spawns.add(new NpcSpawn(NpcId.HELLHOUND_122, new Tile(2413, 9784), 4));
    spawns.add(new NpcSpawn(NpcId.HELLHOUND_122, new Tile(2415, 9788), 4));
    spawns.add(new NpcSpawn(NpcId.HELLHOUND_122, new Tile(2425, 9782), 4));
    spawns.add(new NpcSpawn(NpcId.HELLHOUND_122, new Tile(2429, 9786), 4));
    spawns.add(new NpcSpawn(NpcId.HELLHOUND_122, new Tile(2433, 9784), 4));
    spawns.add(new NpcSpawn(NpcId.HELLHOUND_122, new Tile(2430, 9776), 4));
    spawns.add(new NpcSpawn(NpcId.HELLHOUND_122, new Tile(2426, 9773), 4));
    spawns.add(new NpcSpawn(NpcId.HELLHOUND_122, new Tile(2429, 9770), 4));
    spawns.add(new NpcSpawn(NpcId.HELLHOUND_122, new Tile(2433, 9772), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_GIANT_86, new Tile(2402, 9784), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_GIANT_86, new Tile(2401, 9780), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_GIANT_86, new Tile(2399, 9777), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_GIANT_86, new Tile(2394, 9786), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_GIANT_86, new Tile(2391, 9783), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_GIANT_86, new Tile(2393, 9779), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_GIANT_86, new Tile(2395, 9771), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_GIANT_86, new Tile(2400, 9770), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_GIANT_86, new Tile(2405, 9770), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_GIANT_86, new Tile(2410, 9777), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_GIANT_86, new Tile(2412, 9774), 4));
    spawns.add(new NpcSpawn(NpcId.FIRE_GIANT_86, new Tile(2415, 9771), 4));
    spawns.add(new NpcSpawn(NpcId.ABERRANT_SPECTRE_96, new Tile(2444, 9785), 4));
    spawns.add(new NpcSpawn(NpcId.ABERRANT_SPECTRE_96, new Tile(2443, 9781), 4));
    spawns.add(new NpcSpawn(NpcId.ABERRANT_SPECTRE_96, new Tile(2445, 9777), 4));
    spawns.add(new NpcSpawn(NpcId.ABERRANT_SPECTRE_96, new Tile(2441, 9776), 4));
    spawns.add(new NpcSpawn(NpcId.ABERRANT_SPECTRE_96, new Tile(2457, 9775), 4));
    spawns.add(new NpcSpawn(NpcId.ABERRANT_SPECTRE_96, new Tile(2461, 9777), 4));
    spawns.add(new NpcSpawn(NpcId.ABERRANT_SPECTRE_96, new Tile(2457, 9780), 4));
    spawns.add(new NpcSpawn(NpcId.ABERRANT_SPECTRE_96, new Tile(2452, 9794), 4));
    spawns.add(new NpcSpawn(NpcId.ABERRANT_SPECTRE_96, new Tile(2460, 9792), 4));
    spawns.add(new NpcSpawn(NpcId.ABERRANT_SPECTRE_96, new Tile(2458, 9789), 4));
    spawns.add(new NpcSpawn(NpcId.ABERRANT_SPECTRE_96, new Tile(2454, 9790), 4));
    spawns.add(new NpcSpawn(NpcId.ABERRANT_SPECTRE_96, new Tile(2471, 9776), 4));
    spawns.add(new NpcSpawn(NpcId.ABERRANT_SPECTRE_96, new Tile(2469, 9779), 4));
    spawns.add(new NpcSpawn(NpcId.ABERRANT_SPECTRE_96, new Tile(2470, 9783), 4));
    spawns.add(new NpcSpawn(NpcId.ANKOU_75, new Tile(2472, 9796), 4));
    spawns.add(new NpcSpawn(NpcId.ANKOU_75, new Tile(2475, 9798), 4));
    spawns.add(new NpcSpawn(NpcId.ANKOU_75, new Tile(2479, 9798), 4));
    spawns.add(new NpcSpawn(NpcId.ANKOU_75, new Tile(2482, 9800), 4));
    spawns.add(new NpcSpawn(NpcId.ANKOU_75, new Tile(2477, 9804), 4));
    spawns.add(new NpcSpawn(NpcId.ANKOU_75, new Tile(2475, 9807), 4));
    spawns.add(new NpcSpawn(NpcId.ANKOU_75, new Tile(2472, 9805), 4));
    spawns.add(new NpcSpawn(NpcId.ANKOU_75, new Tile(2470, 9802), 4));
    spawns.add(new NpcSpawn(NpcId.ANKOU_75, new Tile(2470, 9798), 4));
    spawns.add(new NpcSpawn(NpcId.ANKOU_75, new Tile(2477, 9801), 4));
    spawns.add(new NpcSpawn(NpcId.BLOODVELD_76, new Tile(2489, 9817), 4));
    spawns.add(new NpcSpawn(NpcId.BLOODVELD_76, new Tile(2484, 9819), 4));
    spawns.add(new NpcSpawn(NpcId.BLOODVELD_76, new Tile(2485, 9825), 4));
    spawns.add(new NpcSpawn(NpcId.BLOODVELD_76, new Tile(2492, 9825), 4));
    spawns.add(new NpcSpawn(NpcId.BLOODVELD_76, new Tile(2489, 9829), 4));
    spawns.add(new NpcSpawn(NpcId.BLOODVELD_76, new Tile(2473, 9829), 4));
    spawns.add(new NpcSpawn(NpcId.BLOODVELD_76, new Tile(2470, 9834), 4));
    spawns.add(new NpcSpawn(NpcId.BLOODVELD_76, new Tile(2466, 9829), 4));
    spawns.add(new NpcSpawn(NpcId.BLOODVELD_76, new Tile(2463, 9825), 4));
    spawns.add(new NpcSpawn(NpcId.BLOODVELD_76, new Tile(2464, 9834), 4));
    spawns.add(new NpcSpawn(NpcId.BLOODVELD_76, new Tile(2452, 9814), 4));
    spawns.add(new NpcSpawn(NpcId.BLOODVELD_76, new Tile(2454, 9819), 4));
    spawns.add(new NpcSpawn(NpcId.BLOODVELD_76, new Tile(2446, 9819), 4));
    spawns.add(new NpcSpawn(NpcId.BLOODVELD_76, new Tile(2449, 9823), 4));
    spawns.add(new NpcSpawn(NpcId.BLOODVELD_76, new Tile(2439, 9818), 4));
    spawns.add(new NpcSpawn(NpcId.BLOODVELD_76, new Tile(2434, 9815), 4));
    spawns.add(new NpcSpawn(NpcId.BLOODVELD_76, new Tile(2435, 9822), 4));
    spawns.add(new NpcSpawn(NpcId.BLOODVELD_76, new Tile(2440, 9823), 4));
    spawns.add(new NpcSpawn(NpcId.BLOODVELD_76, new Tile(2439, 9831), 4));
    spawns.add(new NpcSpawn(NpcId.BLOODVELD_76, new Tile(2444, 9832), 4));

    return spawns;
  }
}
