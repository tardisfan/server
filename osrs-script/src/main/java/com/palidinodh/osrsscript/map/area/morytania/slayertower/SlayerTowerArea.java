package com.palidinodh.osrsscript.map.area.morytania.slayertower;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 13623, 13624, 6727, 13723 })
public class SlayerTowerArea extends Area {
}
