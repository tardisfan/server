package com.palidinodh.osrsscript.map.area.dragonkinislands.wyverncave;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 14495, 14496 })
public class WyvernCaveArea extends Area {
}
