package com.palidinodh.osrsscript.map.area.asgarnia.rimmington.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class RimmingtonNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.HETTY, new Tile(2968, 3205), 4));
    spawns.add(new NpcSpawn(NpcId.BRIAN_1309, new Tile(2955, 3203), 4));
    spawns.add(new NpcSpawn(NpcId.ROMMIK, new Tile(2949, 3205), 2));
    spawns.add(new NpcSpawn(NpcId.PHIALS, new Tile(2949, 3212), 2));
    spawns.add(new NpcSpawn(NpcId.SHOP_KEEPER_516, new Tile(2947, 3217)));
    spawns.add(new NpcSpawn(NpcId.SHOP_ASSISTANT, new Tile(2948, 3217), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.IMP_7, new Tile(2947, 3221), 8));
    spawns.add(new NpcSpawn(NpcId.TOOL_LEPRECHAUN, new Tile(2940, 3225)));
    spawns.add(new NpcSpawn(NpcId.TARIA, new Tile(2943, 3224), 2));
    spawns.add(new NpcSpawn(NpcId.IMP_7, new Tile(2937, 3234), 8));
    spawns.add(new NpcSpawn(NpcId.CHANCY, new Tile(2929, 3222)));
    spawns.add(new NpcSpawn(NpcId.HOPS, new Tile(2931, 3220), 2));
    spawns.add(new NpcSpawn(NpcId.DA_VINCI, new Tile(2928, 3218), 2));
    spawns.add(new NpcSpawn(NpcId.CHEMIST, new Tile(2932, 3209), 4));

    return spawns;
  }
}
