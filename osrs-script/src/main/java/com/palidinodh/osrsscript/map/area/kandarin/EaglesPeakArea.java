package com.palidinodh.osrsscript.map.area.kandarin;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 9014, 9270 })
public class EaglesPeakArea extends Area {
}
