package com.palidinodh.osrsscript.map.area.fremennikprovince.svenscave;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(10910)
public class SvensCaveArea extends Area {
}
