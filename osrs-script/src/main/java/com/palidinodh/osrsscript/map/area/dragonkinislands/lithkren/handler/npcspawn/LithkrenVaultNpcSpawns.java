package com.palidinodh.osrsscript.map.area.dragonkinislands.lithkren.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class LithkrenVaultNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.ADAMANT_DRAGON_338, new Tile(1544, 5065), 4));
    spawns.add(new NpcSpawn(NpcId.ADAMANT_DRAGON_338, new Tile(1552, 5068), 4));
    spawns.add(new NpcSpawn(NpcId.ADAMANT_DRAGON_338, new Tile(1545, 5074), 4));
    spawns.add(new NpcSpawn(NpcId.ADAMANT_DRAGON_338, new Tile(1541, 5081), 4));
    spawns.add(new NpcSpawn(NpcId.ADAMANT_DRAGON_338, new Tile(1551, 5080), 4));
    spawns.add(new NpcSpawn(NpcId.RUNE_DRAGON_380_8031, new Tile(1583, 5066), 4));
    spawns.add(new NpcSpawn(NpcId.RUNE_DRAGON_380_8031, new Tile(1591, 5068), 4));
    spawns.add(new NpcSpawn(NpcId.RUNE_DRAGON_380_8031, new Tile(1578, 5072), 4));
    spawns.add(new NpcSpawn(NpcId.RUNE_DRAGON_380_8031, new Tile(1590, 5079), 4));
    spawns.add(new NpcSpawn(NpcId.RUNE_DRAGON_380_8031, new Tile(1582, 5081), 4));
    spawns.add(new NpcSpawn(NpcId.ADAMANT_DRAGON_338, new Tile(1544, 5065, 4), 4));
    spawns.add(new NpcSpawn(NpcId.ADAMANT_DRAGON_338, new Tile(1552, 5068, 4), 4));
    spawns.add(new NpcSpawn(NpcId.ADAMANT_DRAGON_338, new Tile(1545, 5074, 4), 4));
    spawns.add(new NpcSpawn(NpcId.ADAMANT_DRAGON_338, new Tile(1541, 5081, 4), 4));
    spawns.add(new NpcSpawn(NpcId.ADAMANT_DRAGON_338, new Tile(1551, 5080, 4), 4));
    spawns.add(new NpcSpawn(NpcId.RUNE_DRAGON_380_8031, new Tile(1583, 5066, 4), 4));
    spawns.add(new NpcSpawn(NpcId.RUNE_DRAGON_380_8031, new Tile(1591, 5068, 4), 4));
    spawns.add(new NpcSpawn(NpcId.RUNE_DRAGON_380_8031, new Tile(1578, 5072, 4), 4));
    spawns.add(new NpcSpawn(NpcId.RUNE_DRAGON_380_8031, new Tile(1590, 5079, 4), 4));
    spawns.add(new NpcSpawn(NpcId.RUNE_DRAGON_380_8031, new Tile(1582, 5081, 4), 4));

    return spawns;
  }
}
