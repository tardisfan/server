package com.palidinodh.osrsscript.map.area.asgarnia.molelair.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class MoleLairNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.GIANT_MOLE_230, new Tile(1759, 5184), 64));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1749, 5222), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1754, 5224), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1759, 5218), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1766, 5217), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1770, 5222), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1772, 5230), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1773, 5239), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1781, 5234), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1782, 5224), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1780, 5215), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1783, 5204), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1774, 5205), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1773, 5195), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1784, 5194), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1778, 5187), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1780, 5180), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1775, 5172), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1780, 5164), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1779, 5155), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1781, 5150), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1774, 5149), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1767, 5151), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1757, 5152), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1751, 5149), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1743, 5151), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1737, 5154), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1738, 5161), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1744, 5165), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1750, 5173), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1741, 5174), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1741, 5181), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1741, 5189), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1739, 5196), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1735, 5203), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1736, 5212), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1742, 5207), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1751, 5207), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1755, 5200), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1749, 5195), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1758, 5210), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1742, 5221), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1735, 5228), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1764, 5198), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1766, 5176), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1756, 5162), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1761, 5166), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1763, 5162), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1770, 5164), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1756, 5178), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1755, 5184), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1761, 5190), 8));
    spawns.add(new NpcSpawn(NpcId.BABY_MOLE, new Tile(1766, 5184), 8));

    return spawns;
  }
}
