package com.palidinodh.osrsscript.map.area.kharidiandesert.uzer.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class UzerHunterNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.ORANGE_SALAMANDER, new Tile(3415, 3076), 4));
    spawns.add(new NpcSpawn(NpcId.ORANGE_SALAMANDER, new Tile(3417, 3079), 4));
    spawns.add(new NpcSpawn(NpcId.ORANGE_SALAMANDER, new Tile(3418, 3074), 4));
    spawns.add(new NpcSpawn(NpcId.ORANGE_SALAMANDER, new Tile(3411, 3072), 4));
    spawns.add(new NpcSpawn(NpcId.ORANGE_SALAMANDER, new Tile(3409, 3077), 4));
    spawns.add(new NpcSpawn(NpcId.ORANGE_SALAMANDER, new Tile(3407, 3081), 4));
    spawns.add(new NpcSpawn(NpcId.ORANGE_SALAMANDER, new Tile(3413, 3083), 4));
    spawns.add(new NpcSpawn(NpcId.ORANGE_SALAMANDER, new Tile(3416, 3083), 4));
    spawns.add(new NpcSpawn(NpcId.ORANGE_SALAMANDER, new Tile(3414, 3070), 4));
    spawns.add(new NpcSpawn(NpcId.ORANGE_SALAMANDER, new Tile(3410, 3088), 4));
    spawns.add(new NpcSpawn(NpcId.ORANGE_SALAMANDER, new Tile(3403, 3087), 4));
    spawns.add(new NpcSpawn(NpcId.ORANGE_SALAMANDER, new Tile(3401, 3093), 4));
    spawns.add(new NpcSpawn(NpcId.ORANGE_SALAMANDER, new Tile(3407, 3090), 4));
    spawns.add(new NpcSpawn(NpcId.ORANGE_SALAMANDER, new Tile(3409, 3095), 4));
    spawns.add(new NpcSpawn(NpcId.ORANGE_SALAMANDER, new Tile(3407, 3096), 4));
    spawns.add(new NpcSpawn(NpcId.ORANGE_SALAMANDER, new Tile(3400, 3099), 4));
    spawns.add(new NpcSpawn(NpcId.ORANGE_SALAMANDER, new Tile(3406, 3099), 4));
    spawns.add(new NpcSpawn(NpcId.ORANGE_SALAMANDER, new Tile(3405, 3132), 4));
    spawns.add(new NpcSpawn(NpcId.ORANGE_SALAMANDER, new Tile(3405, 3136), 4));
    spawns.add(new NpcSpawn(NpcId.ORANGE_SALAMANDER, new Tile(3401, 3134), 4));
    spawns.add(new NpcSpawn(NpcId.ORANGE_SALAMANDER, new Tile(3409, 3134), 4));
    spawns.add(new NpcSpawn(NpcId.GOLDEN_WARBLER, new Tile(3413, 3073), 8));
    spawns.add(new NpcSpawn(NpcId.GOLDEN_WARBLER, new Tile(3413, 3077), 8));
    spawns.add(new NpcSpawn(NpcId.GOLDEN_WARBLER, new Tile(3414, 3081), 8));
    spawns.add(new NpcSpawn(NpcId.GOLDEN_WARBLER, new Tile(3409, 3079), 8));
    spawns.add(new NpcSpawn(NpcId.GOLDEN_WARBLER, new Tile(3412, 3085), 8));
    spawns.add(new NpcSpawn(NpcId.GOLDEN_WARBLER, new Tile(3406, 3086), 8));
    spawns.add(new NpcSpawn(NpcId.GOLDEN_WARBLER, new Tile(3406, 3092), 8));
    spawns.add(new NpcSpawn(NpcId.GOLDEN_WARBLER, new Tile(3402, 3094), 8));
    spawns.add(new NpcSpawn(NpcId.GOLDEN_WARBLER, new Tile(3405, 3101), 8));
    spawns.add(new NpcSpawn(NpcId.GOLDEN_WARBLER, new Tile(3406, 3129), 8));
    spawns.add(new NpcSpawn(NpcId.GOLDEN_WARBLER, new Tile(3409, 3119), 8));
    spawns.add(new NpcSpawn(NpcId.GOLDEN_WARBLER, new Tile(3404, 3114), 8));
    spawns.add(new NpcSpawn(NpcId.GOLDEN_WARBLER, new Tile(3402, 3107), 8));
    spawns.add(new NpcSpawn(NpcId.GOLDEN_WARBLER, new Tile(3405, 3105), 8));
    spawns.add(new NpcSpawn(NpcId.GOLDEN_WARBLER, new Tile(3401, 3104), 8));
    spawns.add(new NpcSpawn(NpcId.GOLDEN_WARBLER, new Tile(3399, 3116), 8));
    spawns.add(new NpcSpawn(NpcId.GOLDEN_WARBLER, new Tile(3400, 3120), 8));
    spawns.add(new NpcSpawn(NpcId.GOLDEN_WARBLER, new Tile(3401, 3126), 8));
    spawns.add(new NpcSpawn(NpcId.GOLDEN_WARBLER, new Tile(3409, 3124), 8));
    spawns.add(new NpcSpawn(NpcId.GOLDEN_WARBLER, new Tile(3409, 3110), 8));
    spawns.add(new NpcSpawn(NpcId.GOLDEN_WARBLER, new Tile(3410, 3090), 8));
    spawns.add(new NpcSpawn(NpcId.GOLDEN_WARBLER, new Tile(3417, 3076), 8));

    return spawns;
  }
}
