package com.palidinodh.osrsscript.map.area.misthalin.draynor.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class DraynorVillageNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.MARTIN_THE_MASTER_GARDENER, new Tile(3079, 3256), 2));
    spawns.add(new NpcSpawn(NpcId.PIG_2812, new Tile(3075, 3259), 2));
    spawns.add(new NpcSpawn(NpcId.PIG, new Tile(3079, 3259), 2));
    spawns.add(new NpcSpawn(NpcId.PIGLET, new Tile(3078, 3261), 2));
    spawns.add(new NpcSpawn(NpcId.PIGLET_2814, new Tile(3076, 3261), 2));
    spawns.add(new NpcSpawn(NpcId.PIGLET_2815, new Tile(3077, 3260), 2));
    spawns.add(new NpcSpawn(NpcId.PIG, new Tile(3077, 3263), 2));
    spawns.add(new NpcSpawn(NpcId.PIG, new Tile(3072, 3260), 2));
    spawns.add(new NpcSpawn(NpcId.PIG, new Tile(3077, 3256), 2));
    spawns.add(new NpcSpawn(NpcId.TOWN_CRIER_277, new Tile(3078, 3250), 4));
    spawns.add(new NpcSpawn(NpcId.OLIVIA, new Tile(3077, 3252), 4));
    spawns.add(new NpcSpawn(NpcId.DIANGO, new Tile(3077, 3246), 4));
    spawns.add(new NpcSpawn(NpcId.FORTUNATO, new Tile(3085, 3250), 4));
    spawns.add(new NpcSpawn(NpcId.MASTER_FARMER, new Tile(3086, 3245), 4));
    spawns.add(new NpcSpawn(NpcId.MARKET_GUARD_20, new Tile(3082, 3249), 4));
    spawns.add(new NpcSpawn(NpcId.BANK_GUARD, new Tile(3088, 3248), 2));
    spawns.add(new NpcSpawn(NpcId.AGGIE, new Tile(3086, 3258), 2));
    spawns.add(new NpcSpawn(NpcId.MARKET_GUARD_20, new Tile(3079, 3251), 4));
    spawns.add(new NpcSpawn(NpcId.WISE_OLD_MAN, new Tile(3088, 3255)));
    spawns.add(new NpcSpawn(NpcId.MISS_SCHISM, new Tile(3095, 3252)));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(3090, 3245), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(3090, 3243), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(3090, 3242), Tile.Direction.EAST));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(3088, 3242)));
    spawns.add(new NpcSpawn(NpcId.NED, new Tile(3099, 3257), 2));
    spawns.add(new NpcSpawn(NpcId.TWIGGY_OKORN, new Tile(3096, 3227), 2));
    spawns.add(new NpcSpawn(NpcId.BLACK_KNIGHT_33, new Tile(3097, 3219), 4));
    spawns.add(new NpcSpawn(NpcId.SQUIRREL_1418, new Tile(3099, 3226), 8));
    spawns.add(new NpcSpawn(NpcId.DARK_WIZARD_7, new Tile(3090, 3232), 4));
    spawns.add(new NpcSpawn(NpcId.DARK_WIZARD_7, new Tile(3084, 3235), 4));

    return spawns;
  }
}
