package com.palidinodh.osrsscript.map.area.feldiphills;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ 9774, 10030, 10286, 10542 })
public class FeldipHillsArea extends Area {
}
