package com.palidinodh.osrsscript.map.area.kandarin.smokedevildungeon.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class SmokeDevilDungeonNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2382, 9430), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2387, 9433), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2383, 9437), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2384, 9443), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2388, 9446), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2383, 9449), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2383, 9454), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2381, 9461), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2382, 9466), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2387, 9451), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2388, 9457), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2391, 9462), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2396, 9462), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2399, 9458), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2400, 9453), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2394, 9451), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2393, 9446), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2393, 9440), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2393, 9432), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2390, 9427), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2391, 9422), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2397, 9429), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2398, 9435), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2399, 9440), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2399, 9447), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2404, 9448), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2406, 9453), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2405, 9459), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2405, 9439), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2404, 9433), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2402, 9427), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2400, 9422), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2410, 9422), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2408, 9427), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2409, 9433), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2418, 9420), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2423, 9423), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2420, 9426), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2420, 9431), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2422, 9436), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2424, 9443), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2412, 9440), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2411, 9446), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2418, 9447), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2422, 9451), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2423, 9457), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2420, 9460), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2415, 9462), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2416, 9456), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2410, 9460), 4));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2412, 9451), 4));
    spawns.add(new NpcSpawn(NpcId.THERMONUCLEAR_SMOKE_DEVIL_301, new Tile(2363, 9449), 8));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2371, 9452), 8));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2357, 9454), 8));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2356, 9445), 8));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2363, 9443), 8));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2370, 9444), 8));
    spawns.add(new NpcSpawn(NpcId.SMOKE_DEVIL_160, new Tile(2366, 9455), 8));

    return spawns;
  }
}
