package com.palidinodh.osrsscript.map.area.wilderness.handler.mapobject;

import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrsscript.map.area.wilderness.WildernessLarransChest;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.LARRANS_BIG_CHEST_34832)
class LarransChestMapObject implements MapObjectHandler {
  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    WildernessLarransChest.open(player, mapObject);
  }
}
