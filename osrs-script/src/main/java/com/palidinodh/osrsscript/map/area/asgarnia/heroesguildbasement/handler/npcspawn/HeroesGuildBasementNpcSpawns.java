package com.palidinodh.osrsscript.map.area.asgarnia.heroesguildbasement.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class HeroesGuildBasementNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.BLUE_DRAGON_111, new Tile(2908, 9904), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_BAT_27, new Tile(2894, 9911), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_BAT_27, new Tile(2889, 9907), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_BAT_27, new Tile(2936, 9896), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_BAT_27, new Tile(2936, 9890), 4));
    spawns.add(new NpcSpawn(NpcId.GIANT_BAT_27, new Tile(2931, 9887), 4));
    spawns.add(new NpcSpawn(NpcId.BANKER, new Tile(2940, 9894), Tile.Direction.WEST));
    spawns.add(new NpcSpawn(NpcId.BANKER_395, new Tile(2924, 9917)));

    return spawns;
  }
}
