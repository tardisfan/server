package com.palidinodh.osrsscript.map.area.kandarin.crashsite.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class CrashSiteCavernNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2153, 5668), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2154, 5677), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2146, 5678), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2133, 5677), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2128, 5672), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2125, 5679), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2102, 5673), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2098, 5679), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2095, 5672), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2073, 5680), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2080, 5675), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2074, 5672), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2073, 5646), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2079, 5645), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2076, 5652), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2119, 5659), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2111, 5661), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2104, 5658), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2111, 5651), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2104, 5643), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2096, 5645), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2095, 5658), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2094, 5651), 4));
    spawns.add(new NpcSpawn(NpcId.DEMONIC_GORILLA_275, new Tile(2102, 5651), 4));
    spawns.add(new NpcSpawn(NpcId.TORTURED_GORILLA_141, new Tile(2136, 5645), 4));
    spawns.add(new NpcSpawn(NpcId.TORTURED_GORILLA_141, new Tile(2143, 5649), 4));
    spawns.add(new NpcSpawn(NpcId.TORTURED_GORILLA_141, new Tile(2144, 5644), 4));
    spawns.add(new NpcSpawn(NpcId.TORTURED_GORILLA_141, new Tile(2150, 5648), 4));
    spawns.add(new NpcSpawn(NpcId.TORTURED_GORILLA_141, new Tile(2152, 5653), 4));
    spawns.add(new NpcSpawn(NpcId.TORTURED_GORILLA_141, new Tile(2158, 5655), 4));
    spawns.add(new NpcSpawn(NpcId.TORTURED_GORILLA_141, new Tile(2156, 5660), 4));
    spawns.add(new NpcSpawn(NpcId.TORTURED_GORILLA_141, new Tile(2140, 5661), 4));
    spawns.add(new NpcSpawn(NpcId.TORTURED_GORILLA_141, new Tile(2133, 5660), 4));

    return spawns;
  }
}
