package com.palidinodh.osrsscript.map.area.kharidiandesert.kalphitelair.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class KalphiteLairNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3498, 9515, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3502, 9519, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3507, 9518, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3510, 9523, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3503, 9525, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3498, 9523, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3492, 9525, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3479, 9525, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3473, 9516, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3464, 9509, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3467, 9501, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3482, 9501, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3494, 9500, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3494, 9491, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3479, 9489, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3464, 9488, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3469, 9487, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3465, 9483, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3470, 9481, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3467, 9477, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3461, 9480, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_SOLDIER_85_957, new Tile(3479, 9477, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28, new Tile(3499, 9476, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_GUARDIAN_141, new Tile(3509, 9491, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_GUARDIAN_141, new Tile(3507, 9500, 2), 4));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_QUEEN_333, new Tile(3476, 9492), 16));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_GUARDIAN_141_960, new Tile(3496, 9500), 8));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_GUARDIAN_141_960, new Tile(3494, 9489), 8));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28_956, new Tile(3475, 9488), 8));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28_956, new Tile(3483, 9486), 8));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28_956, new Tile(3488, 9492), 8));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28_956, new Tile(3495, 9496), 8));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28_956, new Tile(3494, 9508), 8));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28_956, new Tile(3485, 9513), 8));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28_956, new Tile(3477, 9506), 8));
    spawns.add(new NpcSpawn(NpcId.KALPHITE_WORKER_28_956, new Tile(3471, 9498), 8));

    return spawns;
  }
}
