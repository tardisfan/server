package com.palidinodh.osrsscript.map.area.entrana.handler.npcspawn;

import java.util.ArrayList;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.var;

class EntranaDungeonNpcSpawns implements NpcSpawnHandler {
  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(2840, 9774), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(2842, 9771), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(2841, 9767), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(2843, 9764), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(2847, 9763), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(2842, 9759), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(2847, 9759), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(2845, 9756), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(2843, 9752), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(2845, 9748), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(2849, 9748), 4));
    spawns.add(new NpcSpawn(NpcId.ZOMBIE_24, new Tile(2847, 9753), 4));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(2859, 9751), 4));
    spawns.add(new NpcSpawn(NpcId.GREATER_DEMON_92, new Tile(2863, 9749), 4));

    return spawns;
  }
}
