package com.palidinodh.osrsscript.incomingpacket.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Magic;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.GADRIN)
class GadrinNpc implements NpcHandler {
  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (npc.getX() == 2446 && npc.getY() == 3426) {
      player.getMovement().animatedTeleport(new Tile(2467, 9905),
          Magic.NORMAL_MAGIC_ANIMATION_START, Magic.NORMAL_MAGIC_ANIMATION_END,
          Magic.NORMAL_MAGIC_GRAPHIC, null, 2);
    } else if (npc.getX() == 3272 && npc.getY() == 3164) {
      player.getMovement().animatedTeleport(new Tile(3294, 3282),
          Magic.NORMAL_MAGIC_ANIMATION_START, Magic.NORMAL_MAGIC_ANIMATION_END,
          Magic.NORMAL_MAGIC_GRAPHIC, null, 2);
    } else {
      player.openShop("skilling");
    }
  }
}
