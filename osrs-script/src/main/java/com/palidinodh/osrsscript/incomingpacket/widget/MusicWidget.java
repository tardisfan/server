package com.palidinodh.osrsscript.incomingpacket.widget;

import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.MUSIC)
class MusicWidget implements WidgetHandler {
  @Override
  public void widgetOption(Player player, int option, int widgetId, int childId, int slot,
      int itemId) {}
}
