package com.palidinodh.osrsscript.incomingpacket.npc;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ NpcId.KALPHITE_PRINCESS_6653, NpcId.KALPHITE_PRINCESS_6654, NpcId.CORPOREAL_CRITTER,
    NpcId.DARK_CORE_388, NpcId.SNAKELING_2127, NpcId.SNAKELING_2128, NpcId.SNAKELING_2129,
    NpcId.IKKLE_HYDRA_8517, NpcId.IKKLE_HYDRA_8518, NpcId.IKKLE_HYDRA_8519, NpcId.IKKLE_HYDRA_8520,
    NpcId.JAL_NIB_REK, NpcId.TZREK_ZUK, NpcId.MIDNIGHT, NpcId.NOON, NpcId.VETION_JR,
    NpcId.VETION_JR_5537 })
class BossPetsNpc implements NpcHandler {
  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (npc != player.getFamiliar().getFamiliar()) {
      return;
    }
    switch (npc.getId()) {
      case NpcId.KALPHITE_PRINCESS_6653:
        player.getFamiliar().transform(NpcId.KALPHITE_PRINCESS_6654);
        break;
      case NpcId.KALPHITE_PRINCESS_6654:
        player.getFamiliar().transform(NpcId.KALPHITE_PRINCESS_6653);
        break;
      case NpcId.CORPOREAL_CRITTER:
        player.getFamiliar().transform(NpcId.DARK_CORE_388);
        break;
      case NpcId.DARK_CORE_388:
        player.getFamiliar().transform(NpcId.CORPOREAL_CRITTER);
        break;
      case NpcId.SNAKELING_2127:
        switch (PRandom.randomI(1)) {
          case 0:
            player.getFamiliar().transform(NpcId.SNAKELING_2128);
            break;
          case 1:
            player.getFamiliar().transform(NpcId.SNAKELING_2129);
            break;
        }
        break;
      case NpcId.SNAKELING_2128:
        switch (PRandom.randomI(1)) {
          case 0:
            player.getFamiliar().transform(NpcId.SNAKELING_2127);
            break;
          case 1:
            player.getFamiliar().transform(NpcId.SNAKELING_2129);
            break;
        }
        break;
      case NpcId.SNAKELING_2129:
        switch (PRandom.randomI(1)) {
          case 0:
            player.getFamiliar().transform(NpcId.SNAKELING_2128);
            break;
          case 1:
            player.getFamiliar().transform(NpcId.SNAKELING_2127);
            break;
        }
        break;
      case NpcId.IKKLE_HYDRA_8517:
        switch (PRandom.randomE(2)) {
          case 0:
            player.getFamiliar().transform(NpcId.IKKLE_HYDRA_8518);
            break;
          case 1:
            player.getFamiliar().transform(NpcId.IKKLE_HYDRA_8519);
            break;
          case 2:
            player.getFamiliar().transform(NpcId.IKKLE_HYDRA_8520);
            break;
        }
        break;
      case NpcId.IKKLE_HYDRA_8518:
        switch (PRandom.randomI(2)) {
          case 0:
            player.getFamiliar().transform(NpcId.IKKLE_HYDRA_8517);
            break;
          case 1:
            player.getFamiliar().transform(NpcId.IKKLE_HYDRA_8519);
            break;
          case 2:
            player.getFamiliar().transform(NpcId.IKKLE_HYDRA_8520);
            break;
        }
        break;
      case NpcId.IKKLE_HYDRA_8519:
        switch (PRandom.randomI(2)) {
          case 0:
            player.getFamiliar().transform(NpcId.IKKLE_HYDRA_8518);
            break;
          case 1:
            player.getFamiliar().transform(NpcId.IKKLE_HYDRA_8517);
            break;
          case 2:
            player.getFamiliar().transform(NpcId.IKKLE_HYDRA_8520);
            break;
        }
        break;
      case NpcId.IKKLE_HYDRA_8520:
        switch (PRandom.randomI(2)) {
          case 0:
            player.getFamiliar().transform(NpcId.IKKLE_HYDRA_8518);
            break;
          case 1:
            player.getFamiliar().transform(NpcId.IKKLE_HYDRA_8519);
            break;
          case 2:
            player.getFamiliar().transform(NpcId.IKKLE_HYDRA_8517);
            break;
        }
        break;
      case NpcId.TZREK_ZUK:
        player.getFamiliar().transform(NpcId.JAL_NIB_REK);
        break;
      case NpcId.JAL_NIB_REK:
        player.getFamiliar().transform(NpcId.TZREK_ZUK);
        break;
      case NpcId.MIDNIGHT:
        player.getFamiliar().transform(NpcId.NOON);
        break;
      case NpcId.NOON:
        player.getFamiliar().transform(NpcId.MIDNIGHT);
        break;
      case NpcId.VETION_JR:
        player.getFamiliar().transform(NpcId.VETION_JR_5537);
        break;
      case NpcId.VETION_JR_5537:
        player.getFamiliar().transform(NpcId.VETION_JR);
        break;
    }

  }
}
