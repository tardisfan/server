package com.palidinodh.osrsscript.incomingpacket;

import java.util.HashMap;
import java.util.Map;
import com.palidinodh.io.Readers;
import com.palidinodh.io.Stream;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.InStreamKey;
import com.palidinodh.osrscore.io.incomingpacket.IncomingPacketDecoder;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.AchievementDiary;
import com.palidinodh.osrscore.model.entity.player.Equipment;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.util.RequestManager;
import com.palidinodh.osrsscript.player.misc.DropItem;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PLogger;
import lombok.var;

class WidgetDecoder extends IncomingPacketDecoder {
  private static final Map<Integer, WidgetHandler> WIDGET_HANDLERS = new HashMap<>();

  @Override
  public boolean execute(Player player, Stream stream) {
    var option = getInt(InStreamKey.PACKET_OPTION);
    var widgetHash = getInt(InStreamKey.WIDGET_HASH);
    var widgetId = WidgetHandler.getWidgetId(widgetHash);
    var widgetChildId = WidgetHandler.getWidgetChildId(widgetHash);
    var widgetSlot = getInt(InStreamKey.WIDGET_SLOT);
    var itemId = getInt(InStreamKey.ITEM_ID);
    player.clearIdleTime();
    var message = "[Widget(" + option + ")] widgetId=" + widgetId + "/" + WidgetId.valueOf(widgetId)
        + "; widgetChildId=" + widgetChildId + "; widgetSlot=" + widgetSlot + "; itemId=" + itemId
        + "/" + ItemId.valueOf(itemId);
    player.log(PlayerLogType.DECODER_DEBUG, message);
    if (Settings.getInstance().isLocal()) {
      PLogger.println(message);
    }
    if (player.getOptions().isDebug()) {
      player.getGameEncoder().sendMessage(message);
    }
    RequestManager.addUserPacketLog(player, message);
    if (!player.getWidgetManager().hasWidget(widgetId)) {
      return false;
    }
    var isEquipment = widgetId == WidgetId.EQUIPMENT || widgetId == WidgetId.EQUIPMENT_BONUSES;
    Equipment.Slot equipmentSlot = null;
    if (isEquipment) {
      equipmentSlot = Equipment.Slot.getFromWidget(widgetId, widgetChildId);
      if (equipmentSlot != null) {
        widgetSlot = equipmentSlot.ordinal();
        itemId = player.getEquipment().getId(equipmentSlot);
      }
    }
    if (widgetId == WidgetId.INVENTORY || widgetId == WidgetId.EQUIPMENT
        || widgetId == WidgetId.EQUIPMENT_BONUSES) {
      if (player.isLocked()) {
        return false;
      }
      if (player.getMovement().isViewing()) {
        return false;
      }
      if (widgetId == WidgetId.INVENTORY && itemId == -1) {
        return false;
      }
      if (widgetId == WidgetId.INVENTORY && itemId != player.getInventory().getId(widgetSlot)) {
        return false;
      }
      if ((widgetId == WidgetId.EQUIPMENT || widgetId == WidgetId.EQUIPMENT_BONUSES)
          && equipmentSlot != null && itemId == -1) {
        return false;
      }
      if ((widgetId == WidgetId.EQUIPMENT || widgetId == WidgetId.EQUIPMENT_BONUSES)
          && equipmentSlot != null && itemId != player.getEquipment().getId(equipmentSlot)) {
        return false;
      }
    }
    AchievementDiary.widgetUpdate(player, option, widgetId, widgetChildId, widgetSlot, itemId);
    if (player.getController().isItemStorageDisabled()
        && WidgetHandler.isItemStorageWidget(widgetId)) {
      player.getWidgetManager().removeInteractiveWidgets();
      return false;
    }
    if (player.getController().widgetHook(option, widgetId, widgetChildId, widgetSlot, itemId)) {
      return false;
    }
    var finalWidgetSlot = widgetSlot;
    var finalItemId = itemId;
    if (player.getPlugins().containsIf(
        p -> p.widgetHook(option, widgetId, widgetChildId, finalWidgetSlot, finalItemId))) {
      return true;
    }
    if (player.getRandomEvent().widgetHook(option, widgetId, widgetChildId, widgetSlot, itemId)) {
      return false;
    }
    if (SkillContainer.widgetHooks(player, option, widgetId, widgetChildId, widgetSlot, itemId)) {
      return false;
    }
    if (widgetId == WidgetId.INVENTORY || equipmentSlot != null
        && (widgetId == WidgetId.EQUIPMENT || widgetId == WidgetId.EQUIPMENT_BONUSES)) {
      var itemHandler = PlayerPlugin.getInventoryItemHandler(itemId);
      if (widgetId == WidgetId.INVENTORY) {
        var item = player.getInventory().getItem(widgetSlot);
        var optionText = item.getDef().getOption(option).toLowerCase();
        if (item.getDef().getEquipSlot() != null
            && (optionText.equals("wear") || optionText.equals("wield")
                || optionText.equals("equip") || optionText.equals("ride"))) {
          player.getEquipment().equip(itemId, widgetSlot);
          return false;
        }
        if (optionText.equals("drop") || optionText.equals("destroy")
            || optionText.equals("release")) {
          DropItem.drop(player, widgetSlot);
          return false;
        }
      }
      if (isEquipment) {
        var item = player.getEquipment().getItem(widgetSlot);
        if (item != null && option == 0) {
          player.getEquipment().unequip(widgetSlot);
          return false;
        }
      }
      if (itemHandler != null) {
        itemHandler.itemOption(player, option, widgetSlot, itemId, isEquipment);
        return false;
      }
    }
    var widgetHandler = PlayerPlugin.getWidgetHandler(widgetId);
    if (widgetHandler == null) {
      widgetHandler = WIDGET_HANDLERS.get(widgetId);
    }
    if (widgetHandler != null) {
      if (!widgetHandler.isLockedUsable()
          && (player.isLocked() || player.getMovement().isViewing())) {
        return false;
      }
      widgetHandler.widgetOption(player, option, widgetId, widgetChildId, widgetSlot, itemId);
    }
    return false;
  }

  public static void load(boolean force) {
    if (!force && !WIDGET_HANDLERS.isEmpty()) {
      return;
    }
    try {
      WIDGET_HANDLERS.clear();
      var classes = Readers.getScriptClasses(WidgetHandler.class, "incomingpacket.widget");
      for (var clazz : classes) {
        var handler = Readers.newInstance(clazz);
        var ids = WidgetHandler.getIds(handler);
        if (ids == null) {
          continue;
        }
        for (var id : ids) {
          if (WIDGET_HANDLERS.containsKey(id)) {
            throw new RuntimeException(clazz.getName() + " - " + id + ": widget id already used.");
          }
          WIDGET_HANDLERS.put(id, handler);
        }
      }
    } catch (Exception e) {
      PLogger.error(e);
    }
  }

  static {
    load(true);
  }
}
