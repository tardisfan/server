package com.palidinodh.osrsscript.npc.combat;

import java.util.Arrays;
import java.util.List;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import lombok.var;

class SkeletalMysticCombat extends NpcCombat {
  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder().additionalPlayers(255);
    var dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BUCHU_SEED, 8, 12)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GOLPAR_SEED, 8, 12)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NOXIFER_SEED, 8, 12)));
    drop.table(dropTable.build());


    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.SKELETAL_MYSTIC).id(NpcId.SKELETAL_MYSTIC_7605).id(NpcId.SKELETAL_MYSTIC_7606);
    combat.hitpoints(NpcCombatHitpoints.total(150));
    combat.stats(NpcCombatStats.builder().attackLevel(140).magicLevel(140).defenceLevel(187)
        .bonus(BonusType.MELEE_ATTACK, 85).bonus(BonusType.ATTACK_MAGIC, 40)
        .bonus(BonusType.DEFENCE_STAB, 155).bonus(BonusType.DEFENCE_SLASH, 155)
        .bonus(BonusType.DEFENCE_CRUSH, 115).bonus(BonusType.DEFENCE_MAGIC, 140)
        .bonus(BonusType.DEFENCE_RANGED, 115).build());
    combat.aggression(NpcCombatAggression.builder().range(12).always(true).build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.type(NpcCombatType.UNDEAD).deathAnimation(5491).blockAnimation(5489);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.builder().maximum(16).prayerEffectiveness(0.5).build());
    style.animation(5485).attackSpeed(4);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(
        NpcCombatDamage.builder().maximum(16).prayerEffectiveness(0.5).splashOnMiss(true).build());
    style.animation(5523).attackSpeed(4);
    style.targetGraphic(new Graphic(131, 124));
    style.projectile(NpcCombatProjectile.id(130));
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(
        NpcCombatDamage.builder().maximum(16).prayerEffectiveness(0.5).splashOnMiss(true).build());
    style.animation(5523).attackSpeed(4);
    style.targetGraphic(new Graphic(110, 124));
    style.projectile(NpcCombatProjectile.id(109));
    style.effect(NpcCombatEffect.builder().statDrain(Skills.DEFENCE, 1).build());
    combat.style(style.build());


    return Arrays.asList(combat.build());
  }
}
