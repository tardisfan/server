package com.palidinodh.osrsscript.npc.combat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import lombok.var;

class SkotizoCombat extends NpcCombat {
  private static final Tile[] ALTAR_TILES =
      { new Tile(1693, 9904), new Tile(1714, 9889), new Tile(1697, 9871), new Tile(1678, 9887) };
  private static final int[] WIDGET_CHILDREN = { 7, 5, 6, 4 };

  @Inject
  private Npc npc;
  private Npc[] altars;
  private int altarSpawnTime;
  private List<Npc> minions = new ArrayList<>();

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder().clue(NpcCombatDrop.ClueScroll.ELITE,
        NpcCombatDropTable.CHANCE_1_IN_5);
    var dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_1_IN_2500)
        .broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.JAR_OF_DARKNESS)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_1_IN_1000);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_ONYX)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_1_IN_65)
        .broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SKOTOS)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_1_IN_25).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DARK_CLAW)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DARK_TOTEM_BASE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DARK_TOTEM_MIDDLE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DARK_TOTEM)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 500)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SOUL_RUNE, 450)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 450)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_PLATEBODY_NOTED, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_KITESHIELD_NOTED, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_PLATELEGS_NOTED, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_PLATESKIRT_NOTED, 3)));
    dropTable
        .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_RANARR_WEED_NOTED, 40)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_SNAPDRAGON_NOTED, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TORSTOL_NOTED, 20)));
    dropTable
        .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_DRAGONSTONE_NOTED, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BATTLESTAFF_NOTED, 25)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ONYX_BOLT_TIPS, 40)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANTITE_ORE_NOTED, 75)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_BAR_NOTED, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RAW_ANGLERFISH_NOTED, 60)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAHOGANY_PLANK_NOTED, 150)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SHIELD_LEFT_HALF)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().chance(NpcCombatDropTable.CHANCE_ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ASHES)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_SHARD, 1, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CLUE_SCROLL_HARD)));
    drop.table(dropTable.build());


    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.SKOTIZO_321);
    combat.hitpoints(NpcCombatHitpoints.total(450));
    combat.stats(NpcCombatStats.builder().attackLevel(240).magicLevel(280).defenceLevel(200)
        .bonus(BonusType.MELEE_ATTACK, 160).bonus(BonusType.MELEE_DEFENCE, 80)
        .bonus(BonusType.DEFENCE_MAGIC, 130).bonus(BonusType.DEFENCE_RANGED, 130).build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat.type(NpcCombatType.DEMON).deathAnimation(4677).blockAnimation(4676);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(38));
    style.animation(4680).attackSpeed(6);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(38));
    style.animation(69).attackSpeed(6);
    style.targetGraphic(new Graphic(1243));
    style.projectile(NpcCombatProjectile.id(1242));
    combat.style(style.build());


    return Arrays.asList(combat.build());
  }

  @Override
  public void spawnHook() {
    altars = new Npc[4];
    altarSpawnTime = 1 + PRandom.randomE(100);
  }

  @Override
  public void despawnHook() {
    npc.getWorld().removeNpcs(altars);
    npc.getWorld().removeNpcs(minions);
    npc.getWorld().removeNpcs(minions);
    if (npc.getCombat().getTarget() instanceof Player) {
      var player = (Player) npc.getCombat().getTarget();
      if (player.getWidgetManager().getOverlay() == WidgetId.SKOTIZO_OVERLAY) {
        player.getWidgetManager().removeOverlay();
      }
    }
  }

  @Override
  public void tickStartHook() {
    if (npc.getCombat().isDead() && altars != null) {
      npc.getWorld().removeNpcs(altars);
      altars = null;
    }
    if (npc.isVisible() && !npc.getCombat().isDead() && --altarSpawnTime <= 0) {
      altarSpawnTime = 1 + PRandom.randomE(100);
      for (var i = 0; i < altars.length; i++) {
        var index = PRandom.randomE(altars.length);
        var altar = altars[index];
        if (altar != null && (altar.isVisible() || !altar.getCombat().isDead())) {
          continue;
        }
        var tile = new Tile(ALTAR_TILES[index]).setHeight(npc.getHeight());
        altar =
            altars[index] = npc.getController().addNpc(new NpcSpawn(NpcId.AWAKENED_ALTAR, tile));
        altar.getSpawn().respawnable(npc.getSpawn().isRespawnable());
        break;
      }
    }
    if (npc.isVisible() && !npc.getCombat().isDead() && altars != null
        && npc.getCombat().getTarget() instanceof Player) {
      var p = (Player) npc.getCombat().getTarget();
      if (p.getWidgetManager().getOverlay() != WidgetId.SKOTIZO_OVERLAY) {
        p.getWidgetManager().sendOverlay(WidgetId.SKOTIZO_OVERLAY);
        for (var i2 = 4; i2 <= 7; i2++) {
          p.getGameEncoder().sendHideWidget(WidgetId.SKOTIZO_OVERLAY, 4, true);
        }
      }
      for (var i = 0; i < altars.length; i++) {
        p.getGameEncoder().sendHideWidget(WidgetId.SKOTIZO_OVERLAY, WIDGET_CHILDREN[i],
            altars[i] == null || !altars[i].isVisible());
      }
    }
    if (minions.isEmpty() && npc.getCombat().getTarget() != null
        && npc.getCombat().getHitpoints() < npc.getCombat().getMaxHitpoints() / 2) {
      if (PRandom.randomI(1) == 0) {
        minions.add(npc.getController().addNpc(new NpcSpawn(NpcId.REANIMATED_DEMON_SPAWN_87,
            new Tile(npc.getX() + 1, npc.getY(), npc.getHeight()))));
        minions.add(npc.getController().addNpc(new NpcSpawn(NpcId.REANIMATED_DEMON_SPAWN_87,
            new Tile(npc.getX() + 2, npc.getY() + 1, npc.getHeight()))));
        minions.add(npc.getController().addNpc(new NpcSpawn(NpcId.REANIMATED_DEMON_SPAWN_87,
            new Tile(npc.getX() + 3, npc.getY(), npc.getHeight()))));
      } else {
        minions.add(npc.getController().addNpc(new NpcSpawn(NpcId.DARK_ANKOU_95,
            new Tile(npc.getX() + 2, npc.getY() + 1, npc.getHeight()))));
      }
      for (var minion : minions) {
        minion.getCombat().setTarget(npc.getCombat().getTarget());
      }
    }
  }

  @Override
  public double defenceHook(HitStyleType hitStyleType, BonusType meleeAttackStyle, double defence) {
    var boost = 1.0;
    for (var altar : altars) {
      if (altar == null || altar.isLocked()) {
        continue;
      }
      boost += 2;
    }
    return defence * boost;
  }
}
