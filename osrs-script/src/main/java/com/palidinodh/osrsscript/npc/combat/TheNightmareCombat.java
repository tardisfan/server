package com.palidinodh.osrsscript.npc.combat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import com.google.inject.Inject;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.io.cache.id.VarbitId;
import com.palidinodh.osrscore.io.cache.id.VarpId;
import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitMarkType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatMulti;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatQuadrants;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatSummonNpc;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatTargetTile;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrsscript.map.area.morytania.sisterhoodsanctuary.SisterhoodSanctuaryArea;
import com.palidinodh.osrsscript.player.plugin.boss.BossPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PArrayList;
import com.palidinodh.util.PCollection;
import com.palidinodh.util.PEvent;
import com.palidinodh.util.PEventTasks;
import com.palidinodh.util.PTime;
import lombok.var;

class TheNightmareCombat extends NpcCombat {
  private enum SpecialAttack {
    HUSKS, FLOWER_POWER, GRASPING_CLAWS, SLEEPWALKERS, CURSE
  }

  private static final List<Integer> SHIELD_COMBAT_IDS = PCollection.toImmutableList(
      NpcId.THE_NIGHTMARE_814, NpcId.THE_NIGHTMARE_814_9427, NpcId.THE_NIGHTMARE_814_9429);
  private static final List<Integer> TOTEM_COMBAT_IDS = PCollection.toImmutableList(
      NpcId.THE_NIGHTMARE_814_9426, NpcId.THE_NIGHTMARE_814_9428, NpcId.THE_NIGHTMARE_814_9430);
  private static final Tile CENTER_TILE = new Tile(3872, 9951, 3);
  private static final Tile ARENA_SIZE_TILE = new Tile(9, 10);
  private static final Map<Integer, List<SpecialAttack>> PHASE_SPECIAL_ATTACKS =
      PCollection.toImmutableMap(SHIELD_COMBAT_IDS.get(0),
          PCollection.toImmutableList(
              SpecialAttack.HUSKS, SpecialAttack.FLOWER_POWER, SpecialAttack.GRASPING_CLAWS),
          TOTEM_COMBAT_IDS.get(0),
          PCollection.toImmutableList(SpecialAttack.HUSKS, SpecialAttack.FLOWER_POWER,
              SpecialAttack.GRASPING_CLAWS),
          SHIELD_COMBAT_IDS.get(1),
          PCollection.toImmutableList(SpecialAttack.CURSE, SpecialAttack.GRASPING_CLAWS),
          TOTEM_COMBAT_IDS.get(1),
          PCollection.toImmutableList(SpecialAttack.CURSE, SpecialAttack.GRASPING_CLAWS),
          SHIELD_COMBAT_IDS.get(2), PCollection.toImmutableList(SpecialAttack.GRASPING_CLAWS),
          TOTEM_COMBAT_IDS.get(2), PCollection.toImmutableList(SpecialAttack.GRASPING_CLAWS));
  private static final List<NpcSpawn> TOTEM_SPAWNS =
      PCollection.toImmutableList(new NpcSpawn(NpcId.TOTEM, new Tile(CENTER_TILE).moveTile(-9, -9)),
          new NpcSpawn(NpcId.TOTEM_9437, new Tile(CENTER_TILE).moveTile(7, -9)),
          new NpcSpawn(NpcId.TOTEM_9440, new Tile(CENTER_TILE).moveTile(-9, 7)),
          new NpcSpawn(NpcId.TOTEM_9443, new Tile(CENTER_TILE).moveTile(7, 7)));

  @Inject
  private Npc npc;
  private int realHitpoints;
  private int maxShield;
  private NpcCombatStyle meleeAttack;
  private NpcCombatStyle rangedAttack;
  private NpcCombatStyle magicAttack;
  private NpcCombatStyle husksAttack;
  private NpcCombatStyle flowerPowerAttack;
  private PEventTasks flowerPowerEvent;
  private NpcCombatStyle graspingClawsAttack;
  private NpcCombatStyle curseAttack;
  private int normalAttacks;
  private SpecialAttack specialAttack;
  private SpecialAttack lastSpecialAttack;
  private List<Npc> totems = new ArrayList<>();
  private PArrayList<Npc> sleepwalkers = new PArrayList<>();
  private List<Player> players = new ArrayList<>();

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var shieldHitpoints = NpcCombatHitpoints.builder().total(2400)
        .barType(HitpointsBarType.CYAN_DARKER_140).hitMarkType(HitMarkType.CYAN);
    var stats = NpcCombatStats.builder().attackLevel(150).magicLevel(150).rangedLevel(150)
        .defenceLevel(150).bonus(BonusType.MELEE_ATTACK, 140).bonus(BonusType.ATTACK_MAGIC, 140)
        .bonus(BonusType.ATTACK_RANGED, 140).bonus(BonusType.DEFENCE_STAB, 120)
        .bonus(BonusType.DEFENCE_SLASH, 180).bonus(BonusType.DEFENCE_CRUSH, 40)
        .bonus(BonusType.DEFENCE_MAGIC, 600).bonus(BonusType.DEFENCE_RANGED, 600);
    var aggression =
        NpcCombatAggression.builder().range(16).always(true).highestDefenceType(HitStyleType.MELEE);
    var immunity = NpcCombatImmunity.builder().poison(true).venom(true);
    var focus = NpcCombatFocus.builder().keepWithinDistance(1);
    var meleeStyle = NpcCombatStyle.builder();
    meleeStyle.type(NpcCombatStyleType.MELEE_SLASH);
    var rangedStyle = NpcCombatStyle.builder();
    rangedStyle.type(NpcCombatStyleType.RANGED);
    var magicStyle = NpcCombatStyle.builder();
    magicStyle.type(NpcCombatStyleType.MAGIC);


    var idleCombat = NpcCombatDefinition.builder();
    idleCombat.id(NpcId.THE_NIGHTMARE_814_9431).id(NpcId.THE_NIGHTMARE_814_9432)
        .id(NpcId.THE_NIGHTMARE_814_9433);
    idleCombat.immunity(NpcCombatImmunity.builder().playerAttack(true).npcAttack(true).build());
    idleCombat.aggression(NpcCombatAggression.NONE);


    var shield1Combat = NpcCombatDefinition.builder();
    shield1Combat.id(SHIELD_COMBAT_IDS.get(0));
    shield1Combat
        .spawn(NpcCombatSpawn.builder().animation(8611).respawnId(TOTEM_COMBAT_IDS.get(0)).build());
    shield1Combat.hitpoints(shieldHitpoints.build());
    shield1Combat.stats(stats.build());
    shield1Combat.aggression(aggression.build());
    shield1Combat.immunity(immunity.build());
    shield1Combat.focus(focus.build());
    shield1Combat.style(meleeStyle.build());
    shield1Combat.style(rangedStyle.build());
    shield1Combat.style(magicStyle.build());


    var shield2Combat = NpcCombatDefinition.builder();
    shield2Combat.id(SHIELD_COMBAT_IDS.get(1));
    shield2Combat.spawn(NpcCombatSpawn.builder().respawnId(TOTEM_COMBAT_IDS.get(1)).build());
    shield2Combat.hitpoints(shieldHitpoints.build());
    shield2Combat.stats(stats.build());
    shield2Combat.aggression(aggression.build());
    shield2Combat.immunity(immunity.build());
    shield2Combat.focus(focus.build());
    shield2Combat.style(meleeStyle.build());
    shield2Combat.style(rangedStyle.build());
    shield2Combat.style(magicStyle.build());

    var shield3Combat = NpcCombatDefinition.builder();
    shield3Combat.id(SHIELD_COMBAT_IDS.get(2));
    shield3Combat.spawn(NpcCombatSpawn.builder().respawnId(TOTEM_COMBAT_IDS.get(2)).build());
    shield3Combat.hitpoints(shieldHitpoints.build());
    shield3Combat.stats(stats.build());
    shield3Combat.aggression(aggression.build());
    shield3Combat.immunity(immunity.build());
    shield3Combat.focus(focus.build());
    shield3Combat.style(meleeStyle.build());
    shield3Combat.style(rangedStyle.build());
    shield3Combat.style(magicStyle.build());


    var totemCombat = NpcCombatDefinition.builder();
    TOTEM_COMBAT_IDS.forEach(i -> {
      if (i == TOTEM_COMBAT_IDS.get(TOTEM_COMBAT_IDS.size() - 1)) {
        return;
      }
      totemCombat.id(i);
    });
    totemCombat.hitpoints(
        NpcCombatHitpoints.builder().total(2400).barType(HitpointsBarType.GREEN_RED_140).build());
    totemCombat.stats(stats.build());
    totemCombat.aggression(aggression.build());
    totemCombat.immunity(immunity.build());
    totemCombat.focus(focus.build());
    totemCombat.style(meleeStyle.build());
    totemCombat.style(rangedStyle.build());
    totemCombat.style(magicStyle.build());


    var lastTotemCombat = NpcCombatDefinition.builder();
    lastTotemCombat.id(TOTEM_COMBAT_IDS.get(TOTEM_COMBAT_IDS.size() - 1));
    lastTotemCombat.hitpoints(
        NpcCombatHitpoints.builder().total(2400).barType(HitpointsBarType.GREEN_RED_140).build());
    lastTotemCombat.stats(stats.build());
    lastTotemCombat.aggression(aggression.build());
    lastTotemCombat.immunity(immunity.build());
    lastTotemCombat.focus(focus.build());
    lastTotemCombat.deathAnimation(8612);
    lastTotemCombat.style(meleeStyle.build());
    lastTotemCombat.style(rangedStyle.build());
    lastTotemCombat.style(magicStyle.build());


    return Arrays.asList(idleCombat.build(), shield1Combat.build(), shield2Combat.build(),
        shield3Combat.build(), totemCombat.build(), lastTotemCombat.build());
  }

  @Override
  public void addToWorld() {
    TOTEM_SPAWNS.forEach(s -> totems.add(npc.getController().addNpc(s)));
  }

  @Override
  public void removeFromWorld() {
    totems.forEach(n -> npc.getController().removeNpc(n));
  }

  @Override
  public void spawnHook() {
    npc.lock();
    setEnterNpc(NpcId.THE_NIGHTMARE_814_9461, 8573);
    var tasks = new PEventTasks();
    tasks.execute(PTime.secToTick(4), t -> npc.getController()
        .sendRegionMessage("The Nightmare will awaken in 30 seconds!", 15256, npc.getRegionId()));
    tasks.execute(PTime.secToTick(1), t -> npc.getController()
        .sendRegionMessage("The Nightmare will awaken in 20 seconds!", 15256, npc.getRegionId()));
    tasks.execute(PTime.secToTick(1), t -> npc.getController()
        .sendRegionMessage("The Nightmare will awaken in 10 seconds!", 15256, npc.getRegionId()));
    tasks.execute(PTime.secToTick(1), t -> {
      npc.setId(SHIELD_COMBAT_IDS.get(0));
      configureStats();
      setEnterNpc(NpcId.THE_NIGHTMARE_814_9462, 8575);
      npc.getController().sendRegionMessage("<col=ff0000>The Nightmare has awoken!</col>", 15256,
          npc.getRegionId());
    });
    tasks.execute(PTime.secToTick(6), t -> npc.unlock());
    addEvent(tasks);
  }

  @Override
  public void despawnHook() {
    setEnterNpc(NpcId.THE_NIGHTMARE_814_9460, 8580);
    players.clear();
  }

  @Override
  public void idChangedHook(int oldId, int newId) {
    if (TOTEM_COMBAT_IDS.contains(newId)) {
      addEvent(PEvent.singleEvent(1, e -> {
        setHitpoints(realHitpoints);
        updateHealth();
        players.forEach(p -> {
          p.getGameEncoder().setVarp(VarpId.HEALTH_OVERLAY_TYPE, 9432);
          p.getGameEncoder().sendWidgetColor(WidgetId.HEALTH_OVERLAY, 13, 0xcc0000);
          p.getGameEncoder().sendWidgetColor(WidgetId.HEALTH_OVERLAY, 14, 0x00cc00);
        });
      }));
      npc.getController().sendRegionMessage(
          "<col=ff0000>As the Nightmare's shield fails, the totems in the area are activated.</col>");
      totems.forEach(n -> n.setId(n.getId() + 1));
    }
  }

  @Override
  public void tickStartHook() {
    players = npc.getController().getPlayers(npc.getRegionId());
    if (npc.isLocked()) {
      return;
    }
    if (getHitDelay() > 0) {
      return;
    }
    if (players.isEmpty()) {
      npc.getController().removeNpc(npc);
      return;
    }
    checkTotems();
    checkAttacks();
  }

  @Override
  public void tickEndHook() {
    if (npc.isLocked()) {
      return;
    }
    if (specialAttack != null) {
      lastSpecialAttack = specialAttack;
      specialAttack = null;
    }
  }

  @Override
  public NpcCombatStyle attackTickCombatStyleHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (specialAttack == SpecialAttack.HUSKS) {
      return husksAttack;
    }
    if (specialAttack == SpecialAttack.FLOWER_POWER) {
      return flowerPowerAttack;
    }
    if (specialAttack == SpecialAttack.GRASPING_CLAWS) {
      return graspingClawsAttack;
    }
    if (specialAttack == SpecialAttack.CURSE) {
      return curseAttack;
    }
    if (combatStyle.getType().getHitStyleType() == HitStyleType.MELEE
        && combatStyle.getAnimation() == -1) {
      return meleeAttack;
    }
    if (combatStyle.getType().getHitStyleType() == HitStyleType.RANGED
        && combatStyle.getAnimation() == -1) {
      return rangedAttack;
    }
    if (combatStyle.getType().getHitStyleType() == HitStyleType.MAGIC
        && combatStyle.getAnimation() == -1) {
      return magicAttack;
    }
    return combatStyle;
  }

  @Override
  public void attackTickEndHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (specialAttack != null) {
      npc.getController().setMagicBind(combatStyle.getAttackSpeed());
    } else {
      npc.getController().setMagicBind(3);
    }
  }

  @Override
  public void applyAttackEndHook(NpcCombatStyle combatStyle, Entity opponent,
      int applyAttackLoopCount, HitEvent hitEvent) {
    if (combatStyle != curseAttack) {
      return;
    }
    if (!opponent.isPlayer()) {
      return;
    }
    opponent.asPlayer().getPlugin(BossPlugin.class).setTheNightmareCurse(30);
  }

  @Override
  public void quadrantsTasksHook(NpcCombatStyle combatStyle, Entity opponent, PEventTasks tasks) {
    flowerPowerEvent = tasks;
  }

  @Override
  public void npcApplyHitEndHook(Hit hit) {
    updateHealth();
  }

  @Override
  public double damageReceivedHook(Entity opponent, double damage, HitStyleType hitStyleType,
      HitStyleType defenceType) {
    if (TOTEM_COMBAT_IDS.contains(npc.getId()) && opponent.isPlayer()) {
      damage = 0;
    }
    return damage;
  }

  private void updateHealth() {
    players.forEach(p -> {
      p.getGameEncoder().setVarbit(VarbitId.HEALTH_OVERLAY_CURRENT, npc.getCombat().getHitpoints());
      p.getGameEncoder().setVarbit(VarbitId.HEALTH_OVERLAY_TOTAL,
          Math.max(1, npc.getCombat().getMaxHitpoints()));
    });
  }

  private void checkTotems() {
    if (npc.isLocked()) {
      return;
    }
    if (areTotemsCharged()) {
      npc.getController().sendRegionMessage("<col=ff0000>All four totems are fully charged.</col>");
      totems.forEach(n -> n.setId(n.getId() - 2));
      var projectile =
          Graphic.Projectile.builder().id(1768).entity(npc).speed(getProjectileSpeed(10));
      npc.setGraphic(new Graphic(1769, 0, projectile.build().getContactDelay()));
      totems.forEach(n -> sendMapProjectile(projectile.startTile(n).build()));
      var tasks = new PEventTasks();
      tasks.execute(projectile.build().getEventDelay(), e -> applyHit(new Hit(800)));
      tasks.execute(e -> {
        if (npc.getId() == TOTEM_COMBAT_IDS.get(TOTEM_COMBAT_IDS.size() - 1)) {
          handleDeath();
          return;
        }
        realHitpoints = getHitpoints();
        npc.setId(npc.getId() + 1);
        setHitpoints(maxShield);
        setMaxHitpoints(maxShield);
        specialAttack = SpecialAttack.SLEEPWALKERS;
        npc.setLock(2);
        npc.getCombat().setHitDelay(0);
      });
      addEvent(tasks);
    }
  }

  private boolean areTotemsInactive() {
    return getTotemMatches(NpcId.TOTEM, NpcId.TOTEM_9437, NpcId.TOTEM_9440, NpcId.TOTEM_9443) > 0;
  }

  private boolean areTotemsCharging() {
    return getTotemMatches(NpcId.TOTEM_9435, NpcId.TOTEM_9438, NpcId.TOTEM_9441,
        NpcId.TOTEM_9443) > 0;
  }

  private boolean areTotemsCharged() {
    return getTotemMatches(NpcId.TOTEM_9436, NpcId.TOTEM_9439, NpcId.TOTEM_9442,
        NpcId.TOTEM_9445) == totems.size();
  }

  private int getTotemMatches(int... totemIds) {
    var count = 0;
    for (var totem : totems) {
      for (var totemId : totemIds) {
        if (totem.getId() != totemId) {
          continue;
        }
        count++;
        break;
      }
    }
    return count;
  }

  private void checkAttacks() {
    if (npc.isLocked()) {
      return;
    }
    if (specialAttack == null && ++normalAttacks > 2 && PRandom.randomE(4) == 0
        && PHASE_SPECIAL_ATTACKS.containsKey(npc.getId())) {
      normalAttacks = 0;
      var specialAttacks = new ArrayList<SpecialAttack>(PHASE_SPECIAL_ATTACKS.get(npc.getId()));
      if (specialAttacks != null) {
        specialAttacks.remove(lastSpecialAttack);
        if (flowerPowerEvent != null && flowerPowerEvent.isRunning()) {
          specialAttacks.remove(SpecialAttack.FLOWER_POWER);
        }
        specialAttack = PRandom.listRandom(specialAttacks);
      }
    }
    if ((specialAttack == SpecialAttack.FLOWER_POWER || specialAttack == SpecialAttack.SLEEPWALKERS)
        && !npc.matchesTile(SisterhoodSanctuaryArea.NIGHTMARE_CENTER_TILE)) {
      centerTeleport();
      return;
    }
    if (specialAttack == SpecialAttack.SLEEPWALKERS) {
      sleepwalkersAttack();
    }
  }

  private void sleepwalkersAttack() {
    if (players.isEmpty()) {
      return;
    }
    var currentId = npc.getId();
    npc.setId(NpcId.THE_NIGHTMARE_814_9431);
    var sleepwalkerCount = Math.min(Math.max(1, players.size() - 1), 24);
    if (players.size() == 2) {
      sleepwalkerCount = 2;
    }
    for (var i = 0; i < sleepwalkerCount; i++) {
      var randomX = PRandom.randomE(2) == 0;
      var reversed = PRandom.randomE(2) == 0;
      var x = randomX ? -6 + PRandom.randomI(12) : reversed ? -9 : 9;
      var y = randomX ? reversed ? -9 : 9 : -6 + PRandom.randomI(12);
      var tile = new Tile(CENTER_TILE).moveTile(x, y);
      sleepwalkers.add(addNpc(new NpcSpawn(NpcId.SLEEPWALKER_3_9450 + PRandom.randomI(1), tile)));
      sleepwalkers.forEach(n -> n.getCombat().setTarget(npc));
      // NpcId.SLEEPWALKER_3 + PRandom.randomI(5)
      // Diary boots got remade and their old model ids were replaced with the other sleepwalkers
      // TODO: Certain cache files will need updating to use all the sleepwalkers
    }
    npc.getController().sendRegionMessage(
        "<col=ff0000>The Nightmare begins to charge up a devastating attack.</col>");
    var tasks = new PEventTasks();
    tasks.condition(4, t -> sleepwalkers.meetsIf(n -> !n.isVisible()));
    tasks.execute(t -> {
      var total = sleepwalkers.size();
      var remaining = sleepwalkers.countMeetsIf(n -> !n.getCombat().isDead());
      var percent = remaining / (double) total;
      sleepwalkers.removeEach(n -> removeNpc(n));
      npc.setAnimation(8604);
      for (var player : players) {
        if (!npc.withinDistance(player, 32)) {
          continue;
        }
        var damage =
            Math.max(player.getCombat().getHitpoints(), player.getCombat().getMaxHitpoints())
                * percent;
        damage = Math.min(damage, 200); // god mod don't ko me pls
        player.getCombat().addHitEvent(new HitEvent(2, player, new Hit((int) Math.max(5, damage))));
        player.setGraphic(new Graphic(1782));
      }
    });
    tasks.execute(9, t -> {
      npc.setId(currentId);
      npc.getController().sendRegionMessage("<col=ff0000>The Nightmare restores her shield.</col>");
      updateHealth();
      players.forEach(p -> {
        p.getGameEncoder().setVarp(VarpId.HEALTH_OVERLAY_TYPE, 9425);
        p.getGameEncoder().sendWidgetColor(WidgetId.HEALTH_OVERLAY, 13, 0x002020);
        p.getGameEncoder().sendWidgetColor(WidgetId.HEALTH_OVERLAY, 14, 0x00d0a8);
      });
    });
    addEvent(tasks);
  }

  private void setEnterNpc(int npcId, int animation) {
    var enterNpc = npc.getController().getNpc(NpcId.THE_NIGHTMARE_814_9460,
        NpcId.THE_NIGHTMARE_814_9461, NpcId.THE_NIGHTMARE_814_9462, NpcId.THE_NIGHTMARE_814_9463,
        NpcId.THE_NIGHTMARE_814_9464);
    if (enterNpc == null) {
      return;
    }
    enterNpc.setId(npcId);
    enterNpc.setAnimation(animation);
  }

  private void centerTeleport() {
    npc.lock();
    npc.setAnimation(8607);
    var tasks = new PEventTasks();
    tasks.execute(2, t -> {
      npc.getMovement().teleport(SisterhoodSanctuaryArea.NIGHTMARE_CENTER_TILE);
      npc.setAnimation(8609);
    });
    tasks.execute(3, t -> npc.unlock());
    addEvent(tasks);
  }

  private void handleDeath() {
    //
  }

  private void configureStats() {
    players = npc.getController().getPlayers(npc.getRegionId());

    realHitpoints = npc.getCombatDef().getHitpoints().getTotal();
    maxShield = 400 * Math.min(Math.max(5, players.size()), 50);

    setHitpoints(maxShield);
    setMaxHitpoints(maxShield);
    updateHealth();
    addEvent(PEvent.singleEvent(1, e -> {
      players.forEach(p -> {
        p.getWidgetManager().sendOverlay(WidgetId.HEALTH_OVERLAY);
        p.getGameEncoder().setVarp(VarpId.HEALTH_OVERLAY_TYPE, 9425);
        p.getGameEncoder().sendWidgetColor(WidgetId.HEALTH_OVERLAY, 13, 0x002020);
        p.getGameEncoder().sendWidgetColor(WidgetId.HEALTH_OVERLAY, 14, 0x00d0a8);
      });
    }));

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.builder().hitStyleType(HitStyleType.MELEE)
        .meleeAttackStyle(BonusType.ATTACK_SLASH).directional(true).build());
    style.damage(NpcCombatDamage.builder().maximum(25).ignoreDefence(true).prayerEffectiveness(0.2)
        .wrongPrayerEffectiveness(1.2).build());
    style.animation(8594).attackSpeed(6).applyAttackDelay(3);
    style.multiCombat(NpcCombatMulti.WITHIN_RANGE);
    meleeAttack = style.build();

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.builder().maximum(25).prayerEffectiveness(0.2)
        .wrongPrayerEffectiveness(1.2).build());
    style.animation(8596).attackSpeed(6).applyAttackDelay(3);
    style.projectile(NpcCombatProjectile.builder().id(1766).startHeight(80).delay(0).build());
    style.multiCombat(NpcCombatMulti.WITHIN_RANGE);
    rangedAttack = style.build();

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(25).prayerEffectiveness(0.2)
        .wrongPrayerEffectiveness(1.2).build());
    style.animation(8595).attackSpeed(6).applyAttackDelay(3).targetGraphic(new Graphic(1765, 124));
    style.projectile(NpcCombatProjectile.builder().id(1764).startHeight(80).delay(0).build());
    style.multiCombat(NpcCombatMulti.WITHIN_RANGE);
    magicAttack = style.build();

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.animation(8599).attackSpeed(6).applyAttackDelay(3);
    style.projectile(NpcCombatProjectile.builder().id(1781).startHeight(80).delay(0).build());
    style.multiCombat(NpcCombatMulti.builder().chance(50).build());
    var summonNpc = NpcCombatSummonNpc.builder();
    summonNpc.type(NpcCombatSummonNpc.Type.OPPONENT).target(true);
    summonNpc.spawn(new NpcSpawn(NpcId.HUSK_48)).spawn(new NpcSpawn(NpcId.HUSK_48_9455));
    style.specialAttack(summonNpc.build());
    husksAttack = style.build();

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().minimum(4).maximum(6).ignoreDefence(true)
        .ignorePrayer(true).build());
    style.animation(8601).attackSpeed(6)
        .nearbyMessage("<col=7f007f>The Nightmare splits the area into segments!</col>");
    var quadrants = NpcCombatQuadrants.builder();
    quadrants.centerTile(CENTER_TILE).size(ARENA_SIZE_TILE);
    var phase = NpcCombatQuadrants.Phase.builder();
    phase.safeMapObjects(NpcCombatQuadrants.MapObjectPhase.builder()
        .mapObject(new MapObject(ObjectId.NIGHTMARE_BLOSSOM, 10)).build());
    phase.unsafeMapObjects(NpcCombatQuadrants.MapObjectPhase.builder()
        .mapObject(new MapObject(ObjectId.NIGHTMARE_BERRIES, 10)).build());
    quadrants.phase(phase.build());
    phase = NpcCombatQuadrants.Phase.builder();
    phase.safeMapObjects(NpcCombatQuadrants.MapObjectPhase.builder()
        .mapObject(new MapObject(ObjectId.NIGHTMARE_BLOSSOM_37744, 10)).animation(8617).build());
    phase.unsafeMapObjects(NpcCombatQuadrants.MapObjectPhase.builder()
        .mapObject(new MapObject(ObjectId.NIGHTMARE_BERRIES_37741, 10)).animation(8623).build());
    quadrants.phase(phase.build());
    phase = NpcCombatQuadrants.Phase.builder();
    phase.delay(4).startDamage(true);
    phase.safeMapObjects(NpcCombatQuadrants.MapObjectPhase.builder()
        .mapObject(new MapObject(ObjectId.NIGHTMARE_BLOSSOM_37745, 10)).animation(8619).build());
    phase.unsafeMapObjects(NpcCombatQuadrants.MapObjectPhase.builder()
        .mapObject(new MapObject(ObjectId.NIGHTMARE_BERRIES_37742, 10)).animation(8625).build());
    quadrants.phase(phase.build());
    phase = NpcCombatQuadrants.Phase.builder();
    phase.delay(20);
    phase.safeMapObjects(NpcCombatQuadrants.MapObjectPhase.animation(8621));
    phase.unsafeMapObjects(NpcCombatQuadrants.MapObjectPhase.animation(8627));
    quadrants.phase(phase.build());
    style.specialAttack(quadrants.build());
    flowerPowerAttack = style.build();

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.animation(8598).attackSpeed(8).targetTileGraphic(new Graphic(1767));
    style.damage(NpcCombatDamage.maximum(50));
    style.projectile(NpcCombatProjectile.builder().speedMaximumDistance(1).build());
    var targetTile = NpcCombatTargetTile.builder();
    targetTile.eventDelay(3);
    targetTile.breakOff(NpcCombatTargetTile.BreakOff.builder().count(48).distance(20).build());
    style.specialAttack(targetTile.build());
    graspingClawsAttack = style.build();

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.animation(8599).attackSpeed(6)
        .message("T<col=7f007f>he Nightmare has cursed you, shuffling your prayers!</col>");
    style.multiCombat(NpcCombatMulti.WITHIN_RANGE);
    curseAttack = style.build();
  }
}
